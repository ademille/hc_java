package com.ademille.hc.device;

public interface Device {

  int read(byte[] buf);

  int write(byte[] buf, int len);

}
