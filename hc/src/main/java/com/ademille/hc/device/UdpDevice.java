package com.ademille.hc.device;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class UdpDevice implements Device {
  
  private DatagramSocket socket = null;
  private InetAddress destAddr = null;
  private int destPort = 0;
  
  public UdpDevice(){
    this(0);
  }
  
  public UdpDevice(int port){
    try {
      if (0 == port)
        socket = new DatagramSocket();
      else
        socket = new DatagramSocket(port);
      
    } catch (SocketException e) {
      e.printStackTrace();
    }
  }

  @Override
  public int read(byte[] buf) {
    DatagramPacket packet = new DatagramPacket(buf, buf.length);
    try {
      socket.receive(packet);
    } catch (IOException e) {
      return -1;
    }
    return packet.getLength();
  }

  @Override
  public int write(byte[] buf, int len) {
    if (null == destAddr || 0 == destPort){
      return -1;
    } else {
      DatagramPacket packet = new DatagramPacket(buf, len, destAddr, destPort);
      try {
        socket.send(packet);
      } catch (IOException e) {
        e.printStackTrace();
        return -1;
      }
      return packet.getLength();
    }
  }
  
  public InetAddress getDestAddr(){
    return destAddr;
  }
  
  public int getDestPort() {
    return destPort;
  }
  
  public void setDestination(InetAddress addr, int port){
    this.destAddr = addr;
    this.destPort = port;
    
  }
  
  public void getMembership(){
    
  }
  
  public void setMembership(){
    
  }

}
