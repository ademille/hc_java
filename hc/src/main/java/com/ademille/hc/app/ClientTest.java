package com.ademille.hc.app;

import java.net.InetAddress;
import java.net.UnknownHostException;

import com.ademille.hc.core.HcClient;
import com.ademille.hc.core.HcContainer;
import com.ademille.hc.device.UdpDevice;
import com.ademille.hc.exception.HcException;
import com.ademille.hc.param.HcParam;

public class ClientTest {

  static HcParam rcs8Stub = null;

  /**
   * @param args
   */
  public static void main(String[] args) {

    //TODO: Find a better place for these
    final int PID_NAME = 0;
    final int PID_VERSION = 1;
    final int PID_INFOFILECRC = 2;
    final int PID_INFOFILE = 3;

    UdpDevice udpDev = new UdpDevice();
    try {
      udpDev.setDestination(InetAddress.getLocalHost(), 1500);
    } catch (UnknownHostException e) {
      e.printStackTrace();
    }
    /* Create top container */
    System.out.println("Creating top container");
    HcContainer topCont = new HcContainer("");
    HcClient cli = new HcClient(udpDev);
    //cli.registerDebugParams(topCont, "rccli");
    //cli.setDebug(true);
    try {
      //cli.ping();
      String name = cli.getString(PID_NAME);
      System.out.println("Server name: " + name);
      String version = cli.getString(PID_VERSION);
      System.out.println("Server version: " + version);
      long infoFileCrc = cli.getU32(PID_INFOFILECRC);
      System.out.println("Server crc: " + infoFileCrc);

      System.out.println("Downloading server information file");
      cli.downloadSif(PID_INFOFILE, "/tmp/hctree.xml");

      /* Get a list of elements on the server */

    }
    catch (HcException e) {
      System.out.println("Exception connecting to server");
      System.out.println(e);
      e.printStackTrace();
      System.exit(-1);
      return;
    }

    /* Get parameter info from server */
    System.out.println("Getting parameter info from server\n");

    //HcContainer cont = cli.discover();
    //printContainer(cont);

    System.exit(0);

  }

  public static void printContainer(HcContainer cont){
    //System.out.println("+++++++++++++++++");
    //System.out.println(cont.getName());
    for (HcParam param: cont.getParamList()){
      System.out.println("*****************");
      System.out.print("Path:");
      System.out.println(param.getPath());
      System.out.println("\nValue:");
      param.printValue();
      System.out.println("");
      System.out.print("Info:");
      System.out.println(param.getInfo());
      System.out.println("");
    }

    for (HcContainer c: cont.getContList()){
      printContainer(c);
    }
  }

}
