package com.ademille.hc.param;

import com.ademille.hc.core.HcCell;
import com.ademille.hc.exception.AccessException;
import com.ademille.hc.exception.DeserializeException;
import com.ademille.hc.exception.HcException;
import com.ademille.hc.exception.InvalidException;
import com.ademille.hc.exception.TypeException;
import com.ademille.hc.exception.UnspecifiedException;

public class HcF64 extends HcParam {
  
  private double scale;
  private double offset;
  private double llim;
  private double ulim;
  private double step;
  private final String typeStr = "F64";

  public HcF64(){
    this("", Mode.READ_WRITE);
  }
  
  public HcF64(String name, Mode mode){
    this(name, mode, 1.0, 0.0, -1000000000000.0, 1000000000000.0, 0.000000000001);
  }
  
  public HcF64(String name, Mode mode, double scale, double offset, double llim, double ulim, double step){
    super(name, mode, HCPARAM_TYPE_F64);
    this.scale = scale;
    this.offset = offset;
    this.llim = llim;
    this.ulim = ulim;
    this.step = step;
    
  }
  
  @Override
	public void serialize(HcCell msg) throws HcException {
		int attrlen;

		/* Write access flags */
		byte access = (byte) ((byte) ((mode == Mode.READ_ONLY || mode == Mode.READ_WRITE) ? HCPARAM_ACCESS_READ
		    : 0) | (byte) ((mode == Mode.WRITE_ONLY || mode == Mode.READ_WRITE) ? HCPARAM_ACCESS_WRITE
		    : 0));
		msg.writeS8(access);

		/* Check for limits or step not default */
		if ((llim != -1000000000000.0) || (ulim != 1000000000000.0)
		    || (step != 0.000000000001)) {
			/* Write the attribute code */
			msg.writeU8(HCPARAM_ATTR_VALRANGE);

			/* Initialize attribute length */
			attrlen = 8 * 3; //llim, ulim, step

			/* Write attribute length */
			msg.writeU16(attrlen);

			/* Write the lower limit */
			msg.writeF64(llim);

			/* Write the upper limit */
			msg.writeF64(ulim);

			/* Write the step */
			msg.writeF64(step);

		}

		/* Check for scale of offset not default */
		if ((scale != 1.0) || (offset != 0.0)) {
			/* Write the attribute code */
			msg.writeS8(HCPARAM_ATTR_SCALE);

			/* Initialize attribute length */
			attrlen = 8 * 2; //scale + offset
			
			/* Write attribute length */
			msg.writeU16(attrlen);

			/* Write the scale */
			msg.writeF64(scale);

			/* Write the offset */
			msg.writeF64(offset);

		}

		/* Write the end of attributes code */
		msg.writeS8(HCPARAM_ATTR_END);
	}
  
  @Override
	public void deserialize(HcCell msg) throws HcException {
		byte accflags;
		byte attrcode;
		int attrlen;
		
		/* Reset qualification flags */
		resetQualFlags();

		/* Read the access flags */
		accflags = msg.readS8();

		/* Adjust mode depending on read access */
		if ((accflags & HCPARAM_ACCESS_READ) != 0
		    && (accflags & HCPARAM_ACCESS_WRITE) != 0)
			mode = Mode.READ_WRITE;
		else if ((accflags & HCPARAM_ACCESS_READ) != 0)
			mode = Mode.READ_ONLY;
		else if ((accflags & HCPARAM_ACCESS_WRITE) != 0)
			mode = Mode.WRITE_ONLY;
		else
			throw new UnspecifiedException("Invalid Mode");

		/* Process all attributes */
		while (true) {
			/* Read the attribute code */
			attrcode = msg.readS8();

			/* Check for end of attributes */
			if (attrcode == HCPARAM_ATTR_END)
				break;

			/* Read the attribute length */
			attrlen = msg.readU16();

			/* Read attribute value */
			switch (attrcode) {
			case HCPARAM_ATTR_VALRANGE:
				/* Check for invalid state */
				if (qualValRange)
					throw new DeserializeException("Invalid Attributes");
				
				/* Read the lower limit */
				llim = msg.readF64();

				/* Read the upper limit */
				ulim = msg.readF64();

				/* Read the step */
				step = msg.readF64();

				/* Update qualifying flags */
				qualValRange = true;

				break;
			case HCPARAM_ATTR_SCALE:
				/* Check for invalid state */
				if (qualValXlate)
					throw new DeserializeException("Invalid Attributes");
				
				/* Read the scale */
				scale = msg.readF64();

				/* Read the off */
				offset = msg.readF64();

				/* Update qualifying flags */
				qualValXlate = true;
				break;
			case HCPARAM_ATTR_TIDRANGE:
				/* Delegate to base class */
				deserializeTableRange(msg);
				break;

			case HCPARAM_ATTR_TIDENUMS:
				/* Delegate to base class */
				deserializeTableEnums(msg);
				break;
			default:
				/* Unknown parameter attribute, so skip */
				System.err.println("[HCF64]Unknown Attribute:" + attrcode);
				for (int i = 0; i < attrlen; i++)
					msg.readU8();
				break;
			}
		}
	}
  
  
  @Override
  public String getValueString() throws HcException {
    String str;
    double natval;
    natval = getValue();

    /* Print primary value */
    str = String.format("%s", natval);
    return str;
  }

  @Override
  public String iGetValueString(int tid) throws HcException {
    String str;
    double natval;
    natval = iGetValue(tid);

    /* Print primary value */
    str = String.format("%s", natval);
    return str;
  }
  
  
  @Override
  public void printValue() {
    double natval = 0;
    int spacecnt;
    String str;
    HcException hcException = null;

    /* Print name */
    System.out.print(name);
    spacecnt = HCPARAM_NAME_LEN_MAX - this.name.length();

    /* Space to next position */
    for (int i = 0; i < spacecnt; i++)
      System.out.print(" ");

    /* Print delimiter */
    System.out.print(" = ");

    /* Call the native get function */
    try {
      natval = getValue();
    } catch (HcException e) {
      hcException = e;
    }

    /* Print primary value */
    str = String.format("%s", natval);
    spacecnt = HCPARAM_PRIVAL_LEN_MAX - str.length();
    System.out.print(str);

    /* Space to next position */
    for (int i = 0; i < spacecnt; i++)
      System.out.print(" ");

    /* Print delimiter */
    System.out.print(" # ");

    /* Print alternate value */
    str = String.format("%s", natval);
    System.out.print(str);

    spacecnt = HCPARAM_ALTVAL_LEN_MAX - str.length();

    /* Space to next position */
    for (int i = 0; i < spacecnt; i++)
      System.out.print(" ");

    /* Print delimiter and error code */
    System.out.print(" ! ");
    if (hcException == null) {
      System.out.println("None");
    } else {
      System.out.println(hcException.getType());
    }

  }

  @Override
  public String getInfo() {

    StringBuffer str = new StringBuffer();
    
    /* Print type and name */
    str.append("Type:\t" + typeStr + "\n");
    str.append("Name:\t");
    str.append(getPath());
    str.append("\n");

    /* Print access */
    str.append(String.format("Access:\t%s%s\n", (mode == Mode.READ_ONLY || mode == Mode.READ_WRITE) ? "R" : "",
      (mode == Mode.WRITE_ONLY || mode == Mode.READ_WRITE) ? "W" : ""));

    /* Print range and step */
    str.append(String.format("Scale:\t%s\n", scale));
    str.append(String.format("Offset:\t%s\n", offset));
    str.append(String.format("Range:\t%s to %s\n", llim, ulim));
    str.append(String.format("Step:\t%s\n", step));
    
    return str.toString();

  }
  
  @Override
  public void setNatString(String val) throws HcException {
  	//Delegate to table method using invalid tid
  	setTableNatString(-1, val);
  }

  @Override
  public void setTableNatString(int tid, String val) throws HcException {
    double nval;

    /* Check for invalid parameters */
    if(val == null) 
      throw new UnspecifiedException();

    /* Check for no set function */
    if (mode == Mode.READ_ONLY)
      throw new AccessException();

    /* Convert the string to numerical value */
    try {
      nval = Double.parseDouble(val);
    } catch (NumberFormatException nfe){
      throw new InvalidException();
    }

    /* Call the native set function */
    if (tid == -1)
      setValue(nval);
    else
      setTableValue(tid, nval);
	  
  }
 

  @Override
  public void getMsg(HcCell omsg) throws HcException {
    
    double natval = 0;
    byte err = 0;

    /* Call native get function */
    try {
      natval = getValue();
    } catch (HcException rce){
      /* Convert the exception into an error ID */
      err = rce.getErrId();
    }
    
    /* Write type to outbound message */
    omsg.writeU8(HCPARAM_TYPE_F64);

    /* Write error code to outbound message */
    omsg.writeS8(err);

    /* Write value to outbound message */
    omsg.writeF64(natval);

  }
  
  @Override
  public void setMsg(HcCell imsg, HcCell omsg) throws HcException {
    double natval = 0;
    int type;
    byte err = 0;
    
    /* Get type from inbound message */
    type = imsg.readU8();
    
    /* Check for wrong type */
    if (type != this.type){
      omsg.writeS8(TypeException.ERR_ID);
      return;
    }
    
    /* Get value from inbound message */
    natval = imsg.readF64();
    
    /* Call native set function */
    try {
      setValue(natval);
    } catch (HcException rce) {
      err = rce.getErrId();
    }
    
    /* Write error code to outbound message */
    omsg.writeS8(err);

  }

  //RC methods that should be overridden by derived classes.
  public void setValue(double val) throws HcException{
  }
  
  public double getValue() throws HcException{
    return 0;
  }

  public double iGetValue(int tid) throws HcException{
    return 0;
  }

  public void setTableValue(int tid, double val) throws HcException{
  }

  @Override
  public String getEnumString() throws HcException {
    //Not Used for floats
    return "";
  }

  @Override
  public void setEnumString(String val) throws HcException {
    //Not Used for floats
  }

	@Override
  public String iGetEnumString(int tid) throws HcException {
    //Not Used for floats
    return "";
  }

	@Override
  public void setTableEnumString(int tid, String val) throws HcException {
    //Not Used for floats
  }

}
