package com.ademille.hc.param;

import java.util.ArrayList;
import java.util.List;

import com.ademille.hc.core.HcContainer;
import com.ademille.hc.core.HcEnum;
import com.ademille.hc.core.HcCell;
import com.ademille.hc.exception.DeserializeException;
import com.ademille.hc.exception.HcException;
import com.ademille.hc.exception.UnspecifiedException;

public abstract class HcParam {

  // Param Info methods such as R/W flags, Enumeration Values, range and step.

  /* Maximum string lengths */
  public static final int HCPARAM_NAME_LEN_MAX = 20;
  public static final int HCPARAM_ENUM_STR_LEN_MAX = 26;
  public static final int HCPARAM_PRIVAL_LEN_MAX = 26;
  public static final int HCPARAM_ALTVAL_LEN_MAX = 18;

  /* Cardinal Types */
  public static final byte HCPARAM_TYPE_CALL  = 0x00;
  public static final byte HCPARAM_TYPE_S8    = 0x10;
  public static final byte HCPARAM_TYPE_S16   = 0x11;
  public static final byte HCPARAM_TYPE_S32   = 0x12;
  public static final byte HCPARAM_TYPE_S64   = 0x13;
  public static final byte HCPARAM_TYPE_U8    = 0x20;
  public static final byte HCPARAM_TYPE_U16   = 0x21;
  public static final byte HCPARAM_TYPE_U32   = 0x22;
  public static final byte HCPARAM_TYPE_U64   = 0x23;
  public static final byte HCPARAM_TYPE_F32   = 0x32;
  public static final byte HCPARAM_TYPE_F64   = 0x33;
  public static final byte HCPARAM_TYPE_BOOL  = 0x40;
  public static final byte HCPARAM_TYPE_STR   = 0x50;
  public static final byte HCPARAM_TYPE_ARRAY = 0x60;
  
  /* Type masks */
  public static final byte HCPARAM_TYPE_CARD  = 0x7F;
  public static final byte HCPARAM_TYPE_TABLE  = (byte)0x80;
  

  /* Access flags */
  public static final byte HCPARAM_ACCESS_READ  = 0x01;
  public static final byte HCPARAM_ACCESS_WRITE = 0x02;

  /* Attribute codes */
  public static final byte HCPARAM_ATTR_END = 0x00;
  public static final byte HCPARAM_ATTR_VALRANGE = 0x01;
  public static final byte HCPARAM_ATTR_VALENUMS = 0x02;
  public static final byte HCPARAM_ATTR_SCALE = 0x03;
  public static final byte HCPARAM_ATTR_TIDRANGE = 0x04;
  public static final byte HCPARAM_ATTR_TIDENUMS = 0x05;
  
  public enum Mode { READ_ONLY, WRITE_ONLY, READ_WRITE};
  
  protected String name;
  protected Mode mode;

  protected byte type; 
  protected HcContainer parent;

  /* Qualification flags */
  protected boolean qualValRange;
  
	protected boolean qualValEnums;
  protected boolean qualValXlate;
  protected boolean qualStub;
  protected boolean qualTidRange;
  protected boolean qualTidEnums;
  
  /* Table Enumerations */
	protected List<HcEnum> tableEnums;
	
	/* Table Count */
	protected int tidCnt;
 
	public boolean hasQualValRange() {
  	return qualValRange;
  }

	public boolean hasQualValEnums() {
  	return qualValEnums;
  }

	public boolean hasQualValXlate() {
  	return qualValXlate;
  }

	public boolean isQualStub() {
  	return qualStub;
  }

	public boolean hasQualTidRange() {
  	return qualTidRange;
  }

	public boolean hasQualTidEnums() {
  	return qualTidEnums;
  }

	public int getTidCnt() {
  	return tidCnt;
  }

	protected void resetQualFlags() {
		qualValRange = false;
		qualValEnums = false;
		qualValXlate = false;
		qualStub = false;
		qualTidRange = false;
		qualTidEnums = false;
	}
  
  public Mode getMode() {
    return mode;
  }
  
  public String getModeString(){
    String modeStr = "";
      switch (getMode()){
      case READ_ONLY:
        modeStr = "RO";
        break;
      case WRITE_ONLY:
        modeStr = "WO";
        break;
      case READ_WRITE:
        modeStr = "RW";
        break;
      }
      return modeStr;
  }

  public HcParam(String name, Mode mode, byte type){
    this.name = name;
    this.mode = mode;
    this.type = type;
  }

  public List<HcEnum> iGetEnums() {
		return tableEnums;
	}
  
  public String getName(){
    return name;
  }
  
	public String getPath() {

		StringBuffer str = new StringBuffer();
		/* Check for parent */
		if (parent != null) {
			/* Print parent path */
			str.append(parent.getPath());
		}

		/* Print this parameter name */
		str.append(name);
		return str.toString();
	}
  
  public void setParent(HcContainer parent){
    this.parent = parent;
  }
  
  public int serializeName(byte[] serbuf) throws HcException{
    
    /* Get length of the name */
    int nameLen = name.length();
    
    /* Check for invalid parameters */
    if (serbuf == null){
      throw new UnspecifiedException();
    }
    
    /* Check for overflow */
    if (nameLen + 1 > serbuf.length){
      throw new UnspecifiedException();
    }
    
    /* Write parameter name to buffer */
    System.arraycopy(name.getBytes(), 0, serbuf, 0, nameLen);
    
    /* Null terminate */
    serbuf[nameLen] = '\0';
    
    /* Add one for null character at end */
    return nameLen + 1;
    
  }
  
  public byte getType() {
    return type;
  }
  
  public String getTypeString(){
   String retVal = "Unknown"; 
    switch(type){
    case HCPARAM_TYPE_S8:
      retVal = "S8";
      break;
    case HCPARAM_TYPE_S16:
      retVal = "S16";
      break;
    case HCPARAM_TYPE_S32:
      retVal = "S32";
      break;
    case HCPARAM_TYPE_S64:
      retVal = "S64";
      break;
    case HCPARAM_TYPE_U8:
      retVal = "U8";
      break;
    case HCPARAM_TYPE_U16:
      retVal = "U16";
      break;
    case HCPARAM_TYPE_U32:
      retVal = "U32";
      break;
    case HCPARAM_TYPE_U64:
      retVal = "U64";
      break;
    case HCPARAM_TYPE_F32:
      retVal = "F32";
      break;
    case HCPARAM_TYPE_F64:
      retVal = "F64";
      break;
    case HCPARAM_TYPE_BOOL:
      retVal = "BOOL";
      break;
    case HCPARAM_TYPE_STR:
      retVal = "STR";
      break;
    case HCPARAM_TYPE_ARRAY:
      retVal = "ARRAY";
      break;
    case HCPARAM_TYPE_CALL:
      retVal = "CALL";
      break;
    }
    
    return retVal;
    
  }
  
  //Because the table range deserializes the same for all parameters
  //put the method in the base class
  protected void deserializeTableRange(HcCell msg) throws HcException {
  	/* Check for invalid state */
		if (qualTidRange || qualTidEnums)
			throw new DeserializeException("Invalid Table Attributes");
		tidCnt = msg.readU16();
		qualTidRange = true;
  }
  
  protected void deserializeTableEnums(HcCell msg) throws HcException {
  	/* Check for invalid state */
		if (qualTidRange || qualTidEnums)
			throw new DeserializeException("Invalid Table Attributes");
		
		/* Read TID enum count */
		tidCnt = msg.readU16();
		
		/* Create TID enum list */
		tableEnums = new ArrayList<HcEnum>();

		/* Deserialize the TID enums */
		for (int i = 0; i < tidCnt; i++) {
			
			/* Read TID enum value */
			int val = msg.readU16();

			/* Read the enum string */
			String str = msg.readStr();
			tableEnums.add(new HcEnum(val, str));
		}
		
		qualTidEnums = true;
  }

  public abstract void serialize(HcCell msg) throws HcException;

  public abstract void deserialize(HcCell msg) throws HcException;

  public abstract void printValue();
  
  public abstract String getValueString() throws HcException;

  public abstract String iGetValueString(int tid) throws HcException;

  public abstract String getInfo();

  public abstract String getEnumString() throws HcException;


  public abstract void setEnumString(String val) throws HcException;

  public abstract String iGetEnumString(int tid) throws HcException;
  public abstract void setTableEnumString(int tid, String val) throws HcException;

  public abstract void setNatString(String val) throws HcException;

  public abstract void setTableNatString(int tid, String val) throws HcException;

  public abstract void getMsg(HcCell omsg) throws HcException;
  //public abstract void iGetMsg(int tind, HcMessage omsg) throws HcException;

  public abstract void setMsg(HcCell imsg, HcCell omsg) throws HcException;
  //public abstract void setTableMsg(int tind, HcMessage imsg, HcMessage omsg) throws HcException;
  //public abstract int tidValToInd(int tind) throws HcException;
  //public abstract int tidStringToInd(String tid) throws HcException;
  
}
