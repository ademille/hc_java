package com.ademille.hc.param;

import java.util.List;

import com.ademille.hc.core.HcEnum;
import com.ademille.hc.exception.HcException;

public abstract class HcIntegerParam extends HcParam {
	
	//Description of type
	String typeStr = "Unknown";
	
	//Validity Flags 
	protected byte flags = 0;
	
	//Range variables
	protected long llim=0;
	protected long ulim=0;
	
	//Step variables
	protected long step=1;
	
	//Enums
	protected List<HcEnum> enums;

	public HcIntegerParam(String name, Mode mode, byte type, String typeStr, long llim,
	    long ulim, long step, List<HcEnum> enums) {
		super(name, mode, type);
		this.typeStr = typeStr;
		this.llim = llim;
		this.ulim = ulim;
		this.step = step;
		this.enums = enums;
	}
	
	public long getLlim(){
		return llim;
	}
	
	public long getUlim(){
		return ulim;
	}

	public long getStep(){
		return step;
	}

	public boolean isStepValid(){
    return (step != 1);
	}
	
	public List<HcEnum> getEnums() {
		return enums;
	}
	
	@Override
	public String getInfo() {

		StringBuffer str = new StringBuffer();
		/* Print type and name */
		str.append("Type:\t" + typeStr + "\n");
		str.append("Name:\t");
		str.append(getPath());
		str.append("\n");

		/* Print access */
		str.append(String.format("Access:\t%s%s\n",
		    (mode == Mode.READ_ONLY || mode == Mode.READ_WRITE) ? "R" : "",
		    (mode == Mode.WRITE_ONLY || mode == Mode.READ_WRITE) ? "W" : ""));

		/* Check for enums */
		if (enums == null) {
			/* Print range and step */
			str.append(String.format("Range:\t%d to %d\n", llim, ulim));
			str.append(String.format("Step:\t%d\n", step));
		} else {
			/* Print enums */
			str.append("Enums:\n");

			for (HcEnum en : enums)
				str.append(String.format("  %d = \"%s\"\n", en.getVal(), en.getStr()));
		}

		return str.toString();
	}

	public abstract long getLongValue() throws HcException;
	public abstract long iGetLongValue(int tid) throws HcException;
}
