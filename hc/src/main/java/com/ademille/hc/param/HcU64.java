package com.ademille.hc.param;

import java.util.List;

import com.ademille.hc.core.HcEnum;
import com.ademille.hc.core.HcCell;
import com.ademille.hc.exception.HcException;

public class HcU64 extends HcParam {
  
  public HcU64(String name, int llim, int ulim, int step, List<HcEnum> enums){
    super(name, Mode.READ_WRITE, HCPARAM_TYPE_U64);
  }

  @Override
  public void deserialize(HcCell msg) throws HcException {
  }

  @Override
  public String getEnumString() throws HcException {
    return null;
  }

  @Override
  public void getMsg(HcCell omsg) throws HcException {
    
  }

  @Override
  public String getInfo() {
    return "";
    
  }

  @Override
  public String getValueString() throws HcException {
    return "Not Implemented";
 }
  @Override
  public void printValue() {
    
  }

  @Override
  public void serialize(HcCell msg) throws HcException {
  }

  @Override
  public void setEnumString(String val) throws HcException {
    
  }

  @Override
  public void setMsg(HcCell imsg, HcCell omsg) throws HcException {
    
  }

  @Override
  public void setNatString(String val) throws HcException {
    
  }

  @Override
  public void setTableNatString(int tid, String val) throws HcException {
	  
  }
  

	@Override
  public String iGetValueString(int tid) throws HcException {
	  return null;
  }

	@Override
  public String iGetEnumString(int tid) throws HcException {
	  return null;
  }

	@Override
  public void setTableEnumString(int tid, String val) throws HcException {
  }

}
