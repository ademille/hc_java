package com.ademille.hc.param;

import java.util.List;

import com.ademille.hc.core.HcEnum;
import com.ademille.hc.core.HcCell;
import com.ademille.hc.exception.HcException;
import com.ademille.hc.exception.TypeException;
import com.ademille.hc.exception.UnspecifiedException;

public class HcCall extends HcParam {
  
  private List<HcEnum> enums;

  public HcCall(){
    this("");
  }
  
  public HcCall(String name){
    super(name, Mode.WRITE_ONLY, HCPARAM_TYPE_CALL);
  }
  
  @Override
	public void serialize(HcCell msg) throws HcException {
		/* Write access flags */
		msg.writeU8(HCPARAM_ACCESS_WRITE);

		/* Write the end of attributes code */
		msg.writeU8(HCPARAM_ATTR_END);
	}
  
  @Override
	public void deserialize(HcCell msg) throws HcException {
		byte accflags;
		byte attrcode;
		int attrlen;

		/* Read the access flags */
		accflags = msg.readS8();

		/* Adjust mode depending on read access */
		if ((accflags & HCPARAM_ACCESS_WRITE) != 0)
			mode = Mode.WRITE_ONLY;
		else
			throw new UnspecifiedException("Invalid Mode");

		/* Process all attributes */
		while (true) {
			/* Read the attribute code */
			attrcode = msg.readS8();

			/* Check for end of attributes */
			if (attrcode == HCPARAM_ATTR_END)
				break;

			/* Read the attribute length */
			attrlen = msg.readU16();

			/* Read attribute value */
			switch (attrcode) {
			case HCPARAM_ATTR_TIDRANGE:
				/* Delegate to base class */
				deserializeTableRange(msg);
				break;

			case HCPARAM_ATTR_TIDENUMS:
				/* Delegate to base class */
				deserializeTableEnums(msg);
				break;
			default:
				/* Unknown parameter attribute, so skip */
				System.err.println("[HCCall]Unknown Attribute:" + attrcode);
				for (int i = 0; i < attrlen; i++)
					msg.readU8();
				break;
			}
		}
	}
  
  @Override
  public String getValueString() {
    return "Access Error";
  }

  @Override
  public String iGetValueString(int tid) {
    return "Access Error";
  }
  
  @Override
  public void printValue() {
    int spacecnt;
    
    /* Print name */
    System.out.print(name);
    spacecnt = HCPARAM_NAME_LEN_MAX - this.name.length();
    
    /* Space to next position */
    for(int i=0; i<spacecnt; i++)
      System.out.print(" ");

    /* Print delimiter */
    System.out.print(" = ");

    
    /* Space to next position */
    spacecnt = HCPARAM_PRIVAL_LEN_MAX;
    for(int i=0; i<spacecnt; i++)
      System.out.print(" ");

    /* Print delimiter */
    System.out.print(" # ");

    /* Print alternate value */
    

    /* Space to next position */
    spacecnt = HCPARAM_ALTVAL_LEN_MAX;
    for(int i=0; i<spacecnt; i++)
      System.out.print(" ");

    /* Print delimiter and error code */
    System.out.print(" ! ");
    //Any read attempt on a Call is an Access error.
    System.out.println("ACCESS");
  }

  @Override
  public String getInfo() {

    StringBuffer str = new StringBuffer();
    
    /* Print type and name */
    str.append("Type:\tCALL\n");
    str.append("Name:\t");
    str.append(getPath());
    str.append("\n");

    /* Print access */
    str.append(String.format("Access:\t%s\n", (mode == Mode.WRITE_ONLY) ? "W" : ""));

    /* Check for enums */
    if (enums != null){
      /* Print enums */
      str.append("Enums:\n");

      for(HcEnum en:enums)
        str.append(String.format("  %d = \"%s\"\n", en.getVal(), en.getStr()));
    }
    return str.toString();
  }
  
  
  
  @Override
  public void setMsg(HcCell imsg, HcCell omsg) throws HcException {
    int type;
    byte err = 0;
    
    /* Get type from inbound message */
    type = imsg.readU8();
    
    /* Check for wrong type */
    if (type != this.type){
      omsg.writeS8(TypeException.ERR_ID);
      return;
    }
    
    /* Call native set function */
    try {
      call();
    } catch (HcException rce) {
      err = rce.getErrId();
    }
    
    /* Write error code to outbound message */
    omsg.writeS8(err);

  }

  //Methods that should be overridden by derived classes.
  public void call() throws HcException{
  }

  //Methods that should be overridden by derived classes.
  public void callTable(int tid) throws HcException{
  }

 
  @Override
  public void getMsg(HcCell omsg) throws HcException {
   //Not used for Calls
    
  }

  @Override
  public String getEnumString() throws HcException {
    //Not used for Calls
    return "";
  }

  @Override
  public void setEnumString(String val) throws HcException {
    //Not used for Calls
    
  }

  @Override
  public void setNatString(String val) throws HcException {
    //It doesn't matter what the string is. Just send down a call.
  	setTableNatString(-1, val);
  }

  @Override
  public void setTableNatString(int tid, String val) throws HcException {
    //Ignore value, just send down command.
  	if (tid == -1)
  		call();
  	else
  		callTable(tid);
  }

	@Override
  public String iGetEnumString(int tid) throws HcException {
    //Not used for Calls
	  return null;
  }

	@Override
  public void setTableEnumString(int tid, String val) throws HcException {
    //Not used for Calls
	  
  }

 
}
