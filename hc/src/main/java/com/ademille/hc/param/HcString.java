package com.ademille.hc.param;


import com.ademille.hc.core.HcCell;
import com.ademille.hc.exception.HcException;
import com.ademille.hc.exception.TypeException;
import com.ademille.hc.exception.UnspecifiedException;

public class HcString extends HcParam {
  
  public HcString(){
    this("", Mode.READ_WRITE);
  }
  
  public HcString(String name, Mode mode){
    super(name, mode, HCPARAM_TYPE_STR);
  }
  
  @Override
  public void serialize(HcCell msg) throws HcException {
		/* Write access flags */
		byte access = (byte) ((byte) ((mode == Mode.READ_ONLY || mode == Mode.READ_WRITE) ? HCPARAM_ACCESS_READ
		    : 0) | (byte) ((mode == Mode.WRITE_ONLY || mode == Mode.READ_WRITE) ? HCPARAM_ACCESS_WRITE
		    : 0));
		msg.writeU8(access);

		/* Write the end of attributes code */
		msg.writeU8(HCPARAM_ATTR_END);
  }
  
  @Override
	public void deserialize(HcCell msg) throws HcException {
		byte accflags;
		byte attrcode;
		int attrlen;

		/* Read the access flags */
		accflags = msg.readS8();

		/* Adjust mode depending on read access */
		if ((accflags & HCPARAM_ACCESS_READ) != 0
		    && (accflags & HCPARAM_ACCESS_WRITE) != 0)
			mode = Mode.READ_WRITE;
		else if ((accflags & HCPARAM_ACCESS_READ) != 0)
			mode = Mode.READ_ONLY;
		else if ((accflags & HCPARAM_ACCESS_WRITE) != 0)
			mode = Mode.WRITE_ONLY;
		else
			throw new UnspecifiedException("Invalid Mode");

		/* Process all attributes */
		while (true) {
			/* Read the attribute code */
			attrcode = msg.readS8();

			/* Check for end of attributes */
			if (attrcode == HCPARAM_ATTR_END)
				break;

			/* Read the attribute length */
			attrlen = msg.readU16();

			/* Read attribute value */
			switch (attrcode) {
			case HCPARAM_ATTR_TIDRANGE:
				/* Delegate to base class */
				deserializeTableRange(msg);
				break;

			case HCPARAM_ATTR_TIDENUMS:
				/* Delegate to base class */
				deserializeTableEnums(msg);
				break;
			default:
				/* Unknown parameter attribute, so skip */
				System.err.println("[HCString]Unknown Attribute:" + attrcode);
				for (int i = 0; i < attrlen; i++)
					msg.readU8();
				break;
			}
		}
	}
  
  @Override
  public String getValueString() throws HcException {
    
    /* Call the native get function */
    return getValue();
  }
  
  @Override
  public String iGetValueString(int tid) throws HcException {
    
    /* Call the native get function */
    return iGetValue(tid);
  }
  
  
  @Override
  public void printValue() {
    String natval = "";
    int spacecnt;
    String str;
    HcException hcException = null;
    
    /* Print name */
    System.out.print(name);
    spacecnt = HCPARAM_NAME_LEN_MAX - this.name.length();
    
    /* Space to next position */
    for(int i=0; i<spacecnt; i++)
      System.out.print(" ");

    /* Print delimiter */
    System.out.print(" = ");

    /* Call the native get function */
    try {
      natval = getValue();
    } catch (HcException e){
      hcException = e;
    }

    /* Print primary value */
      str = natval;
      spacecnt = HCPARAM_PRIVAL_LEN_MAX - str.length();
      System.out.print(str);

      /* Space to next position */
    for(int i=0; i<spacecnt; i++)
      System.out.print(" ");

    /* Print delimiter */
    System.out.print(" # ");

    /* Print alternate value */
    str = natval;
    spacecnt = HCPARAM_PRIVAL_LEN_MAX - str.length();
    System.out.print(str);

    /* Space to next position */
    for(int i=0; i<spacecnt; i++)
      System.out.print(" ");

    /* Print delimiter and error code */
    System.out.print(" ! ");
    if (hcException == null){
    	System.out.println("None");
    } else {
    	System.out.println(hcException.getType());
    }

  }

  @Override
  public String getInfo() {

    StringBuffer str = new StringBuffer();
    /* Print type and name */
    str.append("Type:\tSTR\n");
    str.append("Name:\t");
    str.append(getPath());
    str.append("\n");

    /* Print access */
    str.append(String.format("Access:\t%s%s\n", (mode == Mode.READ_ONLY || mode == Mode.READ_WRITE) ? "R" : "",
      (mode == Mode.WRITE_ONLY || mode == Mode.READ_WRITE) ? "W" : ""));
    
    return str.toString();
  }
  
  @Override
  public String getEnumString() throws HcException {
   //Not used for String
    return "";
  }

  @Override
  public void setEnumString(String val) throws HcException {
    //Not used for String
   }

  @Override
  public String iGetEnumString(int tid) throws HcException {
   //Not used for String
    return "";
  }

	@Override
  public void setTableEnumString(int tid, String val) throws HcException {
   //Not used for String
  }
  

  @Override
  public void setNatString(String val) throws HcException {
    /* Call the native set function */
    setValue(val);
  }

  @Override
  public void setTableNatString(int tid, String val) throws HcException {
    /* Call the native set function */
    setTableValue(tid, val);
  }


  @Override
  public void getMsg(HcCell omsg) throws HcException {
    
    String natval = "";
    byte err = 0;

    /* Call native get function */
    try {
      natval = getValue();
    } catch (HcException rce){
      /* Convert the exception into an error ID */
      err = rce.getErrId();
    }

    /* Write type to outbound message */
    omsg.writeU8(HCPARAM_TYPE_STR);

    /* Write value to outbound message */
    omsg.writeStr(natval);
    
    /* Write error code to outbound message */
    omsg.writeS8(err);
  }
  
  @Override
  public void setMsg(HcCell imsg, HcCell omsg) throws HcException {
    String natval = "";
    int type;
    byte err = 0;
    
    /* Get type from inbound message */
    type = imsg.readU8();
    
    /* Check for wrong type */
    if (type != this.type){
      omsg.writeS8(TypeException.ERR_ID);
      return;
    }
    
    /* Get value from inbound message */
    natval = imsg.readStr();
    
    /* Call native set function */
    try {
      setValue(natval);
    } catch (HcException rce) {
      err = rce.getErrId();
    }
    
    /* Write error code to outbound message */
    omsg.writeS8(err);

  }

  //Methods that should be overridden by derived classes.
  public void setValue(String val) throws HcException{
  }
  
  public String getValue() throws HcException{
    return "";
  }

  public String iGetValue(int tid) throws HcException{
    return "";
  }

  public void setTableValue(int tid, String val) throws HcException{
  }

}
