package com.ademille.hc.param;

import java.util.ArrayList;
import java.util.List;

import com.ademille.hc.core.HcEnum;
import com.ademille.hc.core.HcCell;
import com.ademille.hc.exception.AccessException;
import com.ademille.hc.exception.DeserializeException;
import com.ademille.hc.exception.HcException;
import com.ademille.hc.exception.InvalidException;
import com.ademille.hc.exception.RangeException;
import com.ademille.hc.exception.SerializeException;
import com.ademille.hc.exception.TypeException;
import com.ademille.hc.exception.UnspecifiedException;

public class HcBool extends HcParam {
  
  private List<HcEnum> enums;

  public HcBool(){
    this("", Mode.READ_WRITE);
  }
  
  public HcBool(String name, Mode mode){
    this(name, mode, null);
  }
  
  public HcBool(String name, Mode mode, List<HcEnum> enums){
    super(name, mode, HCPARAM_TYPE_BOOL);
    this.enums = enums;
  }
  
  public List<HcEnum> getEnumList(){
  	return enums;
  }

  @Override
  public void serialize(HcCell msg) throws HcException
 {
		int attrlen;

		/* Write access flags */
		byte access = (byte) ((byte) ((mode == Mode.READ_ONLY || mode == Mode.READ_WRITE) ? HCPARAM_ACCESS_READ
		    : 0) | (byte) ((mode == Mode.WRITE_ONLY || mode == Mode.READ_WRITE) ? HCPARAM_ACCESS_WRITE
		    : 0));

		msg.writeS8(access);

		/* Check for enums */
		if (enums != null) {
			/* There must only be two enums for a boolean type */
			if (enums.size() != 2)
				throw new SerializeException();

			/* Write the attribute code */
			msg.writeU8(HCPARAM_ATTR_VALENUMS);

			/* Initialize attribute length */
			attrlen = 0;

			/* Count bytes needed for attributes */
			for (HcEnum hcenum: enums) {
				attrlen += hcenum.getStr().length() + 1;
			}

			/* Write attribute length */
			msg.writeU16(attrlen);

			/* Serialize enums */
			for (HcEnum hcenum : enums) {
				/* Serialize the enum string and check for error */
				msg.writeStr(hcenum.getStr());
			}
			
			/* Write the end of attributes code */
			msg.writeU8(HCPARAM_ATTR_END);
		}
	}
  
  @Override
  public void deserialize(HcCell msg) throws HcException
 {
		byte accflags;
		byte attrcode;
		int attrlen;
		int i;
		
		/* Reset qualification flags */
		resetQualFlags();

		/* Read the access flags */
		accflags = msg.readS8();

		/* Adjust mode depending on read access */
		if ((accflags & HCPARAM_ACCESS_READ) != 0
		    && (accflags & HCPARAM_ACCESS_WRITE) != 0)
			mode = Mode.READ_WRITE;
		else if ((accflags & HCPARAM_ACCESS_READ) != 0)
			mode = Mode.READ_ONLY;
		else if ((accflags & HCPARAM_ACCESS_WRITE) != 0)
			mode = Mode.WRITE_ONLY;
		else
			throw new UnspecifiedException("Invalid Mode");

		/* Process all attributes */
		while (true) {
			/* Read the attribute code */
			attrcode = msg.readS8();

			/* Check for end of attributes */
			if (attrcode == HCPARAM_ATTR_END)
				break;

			/* Read the attribute length */
			attrlen = msg.readU16();

			/* Read attribute value */
			switch (attrcode) {
			case HCPARAM_ATTR_VALENUMS:
				/* Check for invalid state */
				if (qualValEnums)
					throw new DeserializeException("Invalid Attributes");
				
				/* Create enum array */
				enums = new ArrayList<HcEnum>();
				for (i = 0; i < 2; i++) {
					/* Deserialize the enum string */
					String str = msg.readStr();

					/* Add enum string to list */
					enums.add(new HcEnum(i, str));
				}

				/* Update qualifying flags */
				qualValEnums = true;
				break;
			case HCPARAM_ATTR_TIDRANGE:
				/* Delegate to base class */
				deserializeTableRange(msg);
				break;
				
			case HCPARAM_ATTR_TIDENUMS:
				/* Delegate to base class */
				deserializeTableEnums(msg);
				break;	
			default:
				/* Unknown parameter attribute, so skip */
				System.err.println("[HCBool]Unknown Attribute:" + attrcode);
				for (i = 0; i < attrlen; i++)
					msg.readU8();
				break;
			}
		}
	}
  
  @Override
  public String getValueString() throws HcException {
    boolean natval = false;
    String str;

    /* Call the native get function */
    natval = getValue();

    /* Print primary value */
    if (enums == null) {
      str = String.format("%b", natval);
    } else {
      /* Convert native value to enum string */
      str = "";
      if (natval) {
        str = enums.get(1).getStr();
      } else {
        str = enums.get(0).getStr();
      }
    }

    return str;
 }
  
  @Override
  public String iGetValueString(int tid) throws HcException {
    boolean natval = false;
    String str;

    /* Call the native get function */
    natval = iGetValue(tid);

    /* Print primary value */
    if (enums == null) {
      str = String.format("%b", natval);
    } else {
      /* Convert native value to enum string */
      str = "";
      if (natval) {
        str = enums.get(1).getStr();
      } else {
        str = enums.get(0).getStr();
      }
    }

    return str;
 }
  
  public void printValue() {
    boolean natval = false;
    int spacecnt;
    String str;
    HcException hcException = null;
    
    /* Print name */
    System.out.print(name);
    spacecnt = HCPARAM_NAME_LEN_MAX - this.name.length();
    
    /* Space to next position */
    for(int i=0; i<spacecnt; i++)
      System.out.print(" ");

    /* Print delimiter */
    System.out.print(" = ");

    /* Call the native get function */
    try {
      natval = getValue();
    } catch (HcException e){
      hcException = e;
    }

    /* Print primary value */
    if(enums == null){
      str = String.format("%b", natval);
      spacecnt = HCPARAM_PRIVAL_LEN_MAX - str.length();
      System.out.print(str);
    }
    else{
      /* Convert native value to enum string */
      str = "";
      if (natval){
        str = String.format("\"%s\"", enums.get(1).getStr());
        spacecnt = HCPARAM_PRIVAL_LEN_MAX - str.length();
        System.out.print(str);
      } else {
        str = String.format("\"%s\"", enums.get(0).getStr());
        spacecnt = HCPARAM_PRIVAL_LEN_MAX - str.length();
        System.out.print(str);
      }
    }

    /* Space to next position */
    for(int i=0; i<spacecnt; i++)
      System.out.print(" ");

    /* Print delimiter */
    System.out.print(" # ");

    /* Print alternate value */
    str = natval?"true":"false";
    System.out.print(str);
    spacecnt = HCPARAM_ALTVAL_LEN_MAX - str.length();

    /* Space to next position */
    for(int i=0; i<spacecnt; i++)
      System.out.print(" ");

    /* Print delimiter and error code */
    System.out.print(" ! ");
    if (hcException == null){
    	System.out.println("None");
    } else {
    	System.out.println(hcException.getType());
    }

  }

  @Override
  public String getInfo() {

    StringBuffer str = new StringBuffer();
    /* Print type and name */
    str.append("Type:\tBOOL\n");
    str.append("Name:\t\n");
    str.append(getPath());
    str.append("\n");

    /* Print access */
    str.append(String.format("Access:\t%s%s\n", (mode == Mode.READ_ONLY || mode == Mode.READ_WRITE) ? "R" : "",
      (mode == Mode.WRITE_ONLY || mode == Mode.READ_WRITE) ? "W" : ""));

    /* Check for enums */
    if (enums != null){
      /* Print enums */
      str.append("Enums:\n");

      for(HcEnum en:enums)
        str.append(String.format("  %d = \"%s\"\n", en.getVal(), en.getStr()));
    }
    
    return str.toString();
  }
  
  @Override
  public String getEnumString() throws HcException {
  	return iGetEnumString(-1);
  }
  
  @Override
  public String iGetEnumString(int tid) throws HcException {
    boolean natval;
    int index;

    /* Check for no enums */
    if(enums == null)
      throw new TypeException();

    /* Check for no get function */
    if (mode == Mode.WRITE_ONLY){
      throw new AccessException();
    }

    //Handle non-table type
    if (tid == -1){
			/* Call the native get function */
			natval = getValue();
    } 
    else {
			/* Call the native get function */
			natval = iGetValue(tid);
    }
    
    /* Convert native value to string */
    if(natval)
      index = 1;
    else
      index = 0;
    
    /* Convert native value to string */
    
    /* Return enum string */
    return enums.get(index).getStr();

  }

  @Override
  public void setEnumString(String val) throws HcException {
  	//Delegate to table method since only one call is different
   setTableEnumString(-1, val);
  }

  @Override
  public void setTableEnumString(int tid, String val) throws HcException {
    int i;

    /* Check for no enums */
    if(enums == null)
      throw new TypeException();

    /* Check for no set function */
    if(mode == Mode.READ_ONLY) 
      throw new AccessException();

    /* Convert string to native value */
    for(i=0; i<2; i++) {
      if(val == enums.get(i).getStr()) {
        /* Call the native set function */
      	if (tid == -1)
          setValue(enums.get(i).getVal()==1?true:false);
      	else
      		setTableValue(tid, enums.get(i).getVal()==1?true:false);
        return;
      }
    }

    /* No matching enum */
    throw new RangeException();
  }


  @Override
  public void setNatString(String val) throws HcException {
  	//Delegate to table method
  	setTableNatString(-1, val);
  }

  @Override
  public void setTableNatString(int tid, String val) throws HcException {
    long nval;

    /* Check for invalid parameters */
    if(val == null) 
      throw new UnspecifiedException();

    /* Check for no set function */
    if (mode == Mode.READ_ONLY)
      throw new AccessException();

    /* Convert the string to numerical value */
    try {
      if ("false".equals(val.toLowerCase()))
        nval = 0;
      else if ("true".equals(val.toLowerCase()))
        nval = 1;
      else
        nval = Long.parseLong(val);
    } catch (NumberFormatException nfe){
      throw new InvalidException();
    }

    /* Check for value beyond range of native type */
    if(nval > 1 || nval < 0 ) {
      throw new RangeException();
    }

    /* Call the native set function */
    if (tid == -1)
			setValue(nval == 1 ? true : false);
		else
			setTableValue(tid, nval == 1 ? true : false);
  }


  @Override
  public void getMsg(HcCell omsg) throws HcException {
    
    boolean natval = false;
    byte err = 0;

    /* Call native get function */
    try {
    natval = getValue();
    } catch (HcException rce){
      /* Convert the exception into an error ID */
      err = rce.getErrId();
    }
    
    /* Write type to outbound message */
    omsg.writeU8(HCPARAM_TYPE_BOOL);

    /* Write value to outbound message */
    omsg.writeBool(natval);

    /* Write error code to outbound message */
    omsg.writeS8(err);


  }
  
  @Override
  public void setMsg(HcCell imsg, HcCell omsg) throws HcException {
  	short type = 0;
    boolean natval = false;
    byte err = 0;
    
    /* Get type from inbound message */
    type = imsg.readU8();
    
    /* Get value from inbound message */
    natval = imsg.readBool();
    
    /* Check for wrong type */
		if (type == this.type) {
			/* Call native set function */
			try {
				setValue(natval);
			} catch (HcException rce) {
				err = rce.getErrId();
			}
		}
		else {
			err = TypeException.ERR_ID;
		}
    
    /* Write error code to outbound message */
    omsg.writeS8(err);

  }

  //Methods that should be overridden by derived classes.
  public void setValue(boolean val) throws HcException{
  }
  
  public boolean getValue() throws HcException{
    return false;
  }
  
  public boolean iGetValue(int tid) throws HcException {
  	return false;
  }

  public void setTableValue(int tid, boolean val) throws HcException {
  }

}
