package com.ademille.hc.param;

import java.util.ArrayList;
import java.util.List;

import com.ademille.hc.core.HcEnum;
import com.ademille.hc.core.HcCell;
import com.ademille.hc.exception.AccessException;
import com.ademille.hc.exception.DeserializeException;
import com.ademille.hc.exception.HcException;
import com.ademille.hc.exception.InvalidException;
import com.ademille.hc.exception.RangeException;
import com.ademille.hc.exception.TypeException;
import com.ademille.hc.exception.UnspecifiedException;

public class HcS16 extends HcIntegerParam {

	private final static short LLIM_MIN = Short.MIN_VALUE;
	private final static short ULIM_MAX = Short.MAX_VALUE;
	private final static short DEFAULT_STEP = 1;
	private final static String typeStr = "S16";

	public HcS16() {
		this("", Mode.READ_WRITE);
	}

	public HcS16(String name, Mode mode) {
		this(name, mode, LLIM_MIN, ULIM_MAX, DEFAULT_STEP, null);
	}

	public HcS16(String name, Mode mode, int llim, int ulim, int step,
	    List<HcEnum> enums) {
		super(name, mode, HCPARAM_TYPE_S16, typeStr, llim, ulim, step, enums);

	}

	@Override
	public void serialize(HcCell msg) throws HcException {
		int attrlen;

		/* Write access flags */
		byte access = (byte) ((byte) ((mode == Mode.READ_ONLY || mode == Mode.READ_WRITE) ? HCPARAM_ACCESS_READ
		    : 0) | (byte) ((mode == Mode.WRITE_ONLY || mode == Mode.READ_WRITE) ? HCPARAM_ACCESS_WRITE
		    : 0));
		msg.writeS8(access);

		/* Check for limits or step not default */
		if ((llim != LLIM_MIN) || (ulim != ULIM_MAX) || (step != DEFAULT_STEP)) {
			/* Write the attribute code */
			msg.writeS8(HCPARAM_ATTR_VALRANGE);

			/* Initialize attribute length */
			attrlen = 2 * 3; // 2 bytes * llim, ulim, step

			/* Write attribute length */
			msg.writeU16(attrlen);

			/* Write the lower limit */
			msg.writeS16((short) llim);

			/* Write the upper limit */
			msg.writeS16((short) ulim);

			/* Write the step */
			msg.writeS16((short) step);
		}

		/* Check for enums */
		if (enums != null) {
			/* Write the attribute code */
			msg.writeS8(HCPARAM_ATTR_VALENUMS);

			/* Initialize attribute length */
			attrlen = 2; // 2-bytes for number of attributes

			/* Calculate attribute length */
			for (HcEnum hcenum : enums) {
				attrlen += 2; // 2 bytes for each value
				attrlen += hcenum.getStr().length() + 1; // value string length
			}

			/* Write attribute length */
			msg.writeU16(attrlen);

			/* Write the enum count */
			msg.writeU16(enums.size());

			/* Serialize enums */
			for (HcEnum hcenum : enums) {
				{
					/* Write the enum value */
					msg.writeS16((short) hcenum.getVal());

					/* Serialize the enum string and check for error */
					msg.writeStr(hcenum.getStr());
				}
			}
		}

		/* Write the end of attributes code */
		msg.writeS8(HCPARAM_ATTR_END);
	}

	@Override
	public void deserialize(HcCell msg) throws HcException {
		byte accflags;
		byte attrcode;
		int attrlen;
		int i;
		int enumcnt;
		int val;
		
		/* Reset qualification flags */
		resetQualFlags();

		/* Read the access flags */
		accflags = msg.readS8();

		/* Adjust mode depending on read access */
		if ((accflags & HCPARAM_ACCESS_READ) != 0
		    && (accflags & HCPARAM_ACCESS_WRITE) != 0)
			mode = Mode.READ_WRITE;
		else if ((accflags & HCPARAM_ACCESS_READ) != 0)
			mode = Mode.READ_ONLY;
		else if ((accflags & HCPARAM_ACCESS_WRITE) != 0)
			mode = Mode.WRITE_ONLY;
		else
			throw new UnspecifiedException("Invalid Mode");

		/* Process all attributes */
		while (true) {
			/* Read the attribute code */
			attrcode = msg.readS8();

			/* Check for end of attributes */
			if (attrcode == HCPARAM_ATTR_END)
				break;

			/* Read the attribute length */
			attrlen = msg.readU16();

			/* Read attribute value */
			switch (attrcode) {
			case HCPARAM_ATTR_VALRANGE:
				/* Check for invalid state */
				if (qualValRange)
					throw new DeserializeException("Invalid Attributes");
				/* Read the lower limit */
				llim = msg.readS16();

				/* Read the upper limit */
				ulim = msg.readS16();

				/* Read the step */
				step = msg.readS16();

				/* Update qualifying flags */
				qualValRange = true;
				break;
			case HCPARAM_ATTR_VALENUMS:
				/* Check for invalid state */
				if (qualValEnums)
					throw new DeserializeException("Invalid Attributes");
				
				/* Deserialize the enum count */
				enumcnt = msg.readU16();

				/* Create enum array */
				enums = new ArrayList<HcEnum>();
				for (i = 0; i < enumcnt; i++) {
					/* Deserialize the enum value */
					val = msg.readS16();

					/* Deserialize the enum string */

					String str = msg.readStr();
					enums.add(new HcEnum(val, str));
				}
				
				/* Update qualifying flags */
				qualValEnums = true;
				break;
			case HCPARAM_ATTR_TIDRANGE:
				/* Delegate to base class */
				deserializeTableRange(msg);
				break;

			case HCPARAM_ATTR_TIDENUMS:
				/* Delegate to base class */
				deserializeTableEnums(msg);
				break;
			default:
				/* Unknown parameter attribute, so skip */
				System.err.println("[HCS16]Unknown Attribute:" + attrcode);
				for (i = 0; i < attrlen; i++)
					msg.readU8();
				break;
			}
		}
	}

	@Override
	public String getValueString() throws HcException {
		String str;
		int natval = 0;

		/* Call the native get function */
		natval = getValue();

		/* Print primary value */
		if (enums == null) {
			str = String.format("%d", natval);
		} else {
			/* Convert native value to enum string */
			str = "";
			for (HcEnum en : enums) {
				if (en.getVal() == natval) {
					str = en.getStr();
					break;
				}
			}
		}

		/* Print alternate value */
		// str = String.format("0x%02x", natval);
		return str;
	}
	
	@Override
	public String iGetValueString(int tid) throws HcException {
		String str;
		int natval = 0;

		/* Call the native get function */
		natval = iGetValue(tid);

		/* Print primary value */
		if (enums == null) {
			str = String.format("%d", natval);
		} else {
			/* Convert native value to enum string */
			str = "";
			for (HcEnum en : enums) {
				if (en.getVal() == natval) {
					str = en.getStr();
					break;
				}
			}
		}

		/* Print alternate value */
		// str = String.format("0x%02x", natval);
		return str;
	}


	@Override
	public void printValue() {
		int natval = 0;
		int spacecnt;
		String str;
		HcException hcException = null;

		/* Print name */
		System.out.print(name);
		spacecnt = HCPARAM_NAME_LEN_MAX - this.name.length();

		/* Space to next position */
		for (int i = 0; i < spacecnt; i++)
			System.out.print(" ");

		/* Print delimiter */
		System.out.print(" = ");

		/* Call the native get function */
		try {
			natval = getValue();
		} catch (HcException e) {
			hcException = e;
		}

		/* Print primary value */
		if (enums == null) {
			str = String.format("%d", natval);
			spacecnt = HCPARAM_PRIVAL_LEN_MAX - str.length();
			System.out.print(str);
		} else {
			/* Convert native value to enum string */
			str = "";
			for (HcEnum en : enums) {
				if (en.getVal() == natval) {
					str = en.getStr();
					break;
				}
			}

			System.out.print("\"" + str + "\"");
			spacecnt = HCPARAM_PRIVAL_LEN_MAX - str.length();
		}

		/* Space to next position */
		for (int i = 0; i < spacecnt; i++)
			System.out.print(" ");

		/* Print delimiter */
		System.out.print(" # ");

		/* Print alternate value */
		/* Print natural value in hex */
		str = String.format("0x%02x", natval);
		System.out.print(str);

		spacecnt = HCPARAM_ALTVAL_LEN_MAX - str.length();

		/* Space to next position */
		for (int i = 0; i < spacecnt; i++)
			System.out.print(" ");

		/* Print delimiter and error code */
		System.out.print(" ! ");
		if (hcException == null) {
			System.out.println("None");
		} else {
			System.out.println(hcException.getType());
		}

	}

	

	@Override
	public String getEnumString() throws HcException {
		//Delegate to table version with invalid tid
		return iGetEnumString(-1);
	}


	@Override
	public void setEnumString(String val) throws HcException {
		setTableEnumString(-1, val);
	}	
	
	@Override
  public String iGetEnumString(int tid) throws HcException {
		int natval;
		int i;

		/* Check for no enums */
		if (enums == null)
			throw new TypeException();

		/* Check for no get function */
		if (mode == Mode.WRITE_ONLY) {
			throw new AccessException();
		}

		/* Call the native get function */
		if (tid == -1)
		  natval = getValue();
		else
		  natval = iGetValue(tid);

		/* Convert native value to string */
		for (i = 0; i < enums.size(); i++)
			if (enums.get(i).getVal() == natval)
				break;

		/* Check for string not found */
		if (i >= enums.size()) {
			throw new RangeException();
		}

		/* Return enum string */
		return enums.get(i).getStr();

  }

	@Override
  public void setTableEnumString(int tid, String val) throws HcException {
		int i;

		/* Check for no enums */
		if (enums == null)
			throw new TypeException();

		/* Check for no set function */
		if (mode == Mode.READ_ONLY)
			throw new AccessException();

		/* Convert string to native value */
		for (i = 0; i < enums.size(); i++) {
			if (val == enums.get(i).getStr()) {
				/* Call the native set function */
				if (tid == -1)
				  setValue((short) enums.get(i).getVal());
				else
				  setTableValue(tid, (short) enums.get(i).getVal());
				return;
			}
		}

		/* No matching enum */
		throw new RangeException();
	}


	@Override
	public void setNatString(String val) throws HcException {
		//Delegate to table method using invalid tid
		setTableNatString(-1, val);
	}

	@Override
  public void setTableNatString(int tid, String val) throws HcException {
		long nval;

		/* Check for invalid parameters */
		if (val == null)
			throw new UnspecifiedException();

		/* Check for no set function */
		if (mode == Mode.READ_ONLY)
			throw new AccessException();

		/* Convert the string to numerical value */
		try {
			nval = Long.parseLong(val);
		} catch (NumberFormatException nfe) {
			throw new InvalidException();
		}

		/* Check for value beyond range of native type */
		if ((nval < LLIM_MIN) || (nval > ULIM_MAX)) {
			throw new RangeException();
		}

		/* Call the native set function */
		if (tid == -1)
		  setValue((short) nval);
		else
		  setTableValue(tid, (short) nval);

	  
  }


	@Override
	public void getMsg(HcCell omsg) throws HcException {

		long natval = 0;
		byte err = 0;

		/* Call native get function */
		try {
			natval = getValue();
		} catch (HcException rce) {
			/* Convert the exception into an error ID */
			err = rce.getErrId();
		}
		/* Write type to outbound message */
		omsg.writeU8(HCPARAM_TYPE_S16);

		/* Write value to outbound message */
		omsg.writeS16((short) natval);

		/* Write error code to outbound message */
		omsg.writeS8(err);


	}

	@Override
	public void setMsg(HcCell imsg, HcCell omsg) throws HcException {
		long natval = 0;
		int type;
		byte err = 0;

		/* Get type from inbound message */
		type = imsg.readU8();

		/* Check for wrong type */
		if (type != this.type) {
			omsg.writeS8(TypeException.ERR_ID);
			return;
		}

		/* Get value from inbound message */
		natval = imsg.readS16();

		/* Call native set function */
		try {
			setValue((short) natval);
		} catch (HcException rce) {
			err = rce.getErrId();
		}

		/* Write error code to outbound message */
		omsg.writeS8(err);

	}

	// RC S8 methods that should be overridden by derived classes.
	public void setValue(short val) throws HcException {
	}

	public short getValue() throws HcException {
		return 0;
	}

	public short iGetValue(int tid) throws HcException {
		return 0;
	}

	public void setTableValue(int tid, short val) throws HcException {
	}

	public long getLongValue() throws HcException {
		return getValue();
	}

	@Override
  public long iGetLongValue(int tid) throws HcException {
		return iGetValue(tid);
  }

}
