package com.ademille.hc.core;

public class HcEnum {
  private long val;
  private String str;
  
  public HcEnum(long val, String str){
    this.setVal(val);
    this.setStr(str);
  }

  public void setVal(long val) {
    this.val = val;
  }

  public long getVal() {
    return val;
  }

  public void setStr(String str) {
    this.str = str;
  }

  public String getStr() {
    return str;
  }

}
