package com.ademille.hc.core;


import com.ademille.hc.exception.DeserializeException;
import com.ademille.hc.exception.SerializeException;
import com.ademille.hc.util.StringUtil;

public class HcCell {

  /* Cell Overhead */
  static final int OHD_LEN = 3;

  /* Maximum payload size (see message payload max) */
  static final int PLD_LEN_MAX = 1397;
  
  /* Opcodes */
  
  static final byte OPCODE_CALL_CMD    = (byte)0x00;
  static final byte OPCODE_CALL_STS    = (byte)0x01;
  static final byte OPCODE_GET_CMD     = (byte)0x02;
  static final byte OPCODE_GET_STS     = (byte)0x03;
  static final byte OPCODE_SET_CMD     = (byte)0x04;
  static final byte OPCODE_SET_STS     = (byte)0x05;
  static final byte OPCODE_ICALL_CMD   = (byte)0x06;
  static final byte OPCODE_ICALL_STS   = (byte)0x07;
  static final byte OPCODE_IGET_CMD    = (byte)0x08;
  static final byte OPCODE_IGET_STS    = (byte)0x09;
  static final byte OPCODE_ISET_CMD    = (byte)0x0A;
  static final byte OPCODE_ISET_STS    = (byte)0x0B;
  static final byte OPCODE_ADD_CMD     = (byte)0x0C;
  static final byte OPCODE_ADD_STS     = (byte)0x0D;
  static final byte OPCODE_SUB_CMD     = (byte)0x0E;
  static final byte OPCODE_SUB_STS     = (byte)0x0F;
  static final byte OPCODE_READ_CMD    = (byte)0x10;
  static final byte OPCODE_READ_STS    = (byte)0x11;
  static final byte OPCODE_WRITE_CMD   = (byte)0x12;
  static final byte OPCODE_WRITE_STS   = (byte)0x13;
  
  static final byte OPCODE_NEG_ACK = (byte)0xFF;

  private byte opcode;
  private int payloadLength;
  private int dataIndex;
  
  private byte payload[];

  public HcCell() {
    opcode = 0;
    payloadLength = 0;
    payload = new byte[PLD_LEN_MAX];
    dataIndex = 0;
  }

  public void reset(byte opcode) {
    this.opcode = opcode;
    this.payloadLength = 0;
    this.dataIndex = 0;
  }

  public int serialize(byte serbuf[], int startPos) {
    int maxlen = serbuf.length - startPos;
      
    if (maxlen < payloadLength + OHD_LEN)
      return 0;

    /* Reset index */
    int i = startPos;

    /* Serialize the opcode */
    serbuf[i++] = opcode;
    
    /* Serialize the payload length */
    serbuf[i++] = (byte)(payloadLength >> 8);
    serbuf[i++] = (byte)(payloadLength);

    /* Serialize the payload */
    for (int j = 0; j < payloadLength; j++)
      serbuf[i++] = payload[j];

    /* Return the number of bytes serialized */
    return i - startPos;

  }

  public int deserialize(byte serbuf[], int startPos, int maxLen) {
    int i;


    /* Check for invalid arguments */
    if ((startPos < 0) || (maxLen < OHD_LEN) || (maxLen > (OHD_LEN + PLD_LEN_MAX)) )
    //if ((startPos < 0) || (serbuf.length - startPos < OHD_LEN))
      return 0;

    /* Initialize index */
    i = startPos;

    /* Deserialize the opcode */
    opcode = serbuf[i++];

    /* Deserialize the payload length */
    payloadLength =  (int) ((serbuf[i++] & 0xFF) << 8 & 0xFFFF);
    payloadLength |= (int) (serbuf[i++] & 0xFF);
    
    /* Check for underflow */
    if ((startPos + maxLen) < (payloadLength + i))
    //if (serbuf.length < (payloadLength + i))
      return 0;

    /* Deserialize the payload */
    for (int j = 0; j < payloadLength; j++)
      payload[j] = serbuf[i++];
    
    /* Reset the payload offset position */
    dataIndex = 0;
    
    return i - startPos;
  }

  public byte readS8() throws DeserializeException {
    return (byte)readU8();
  }

  public void writeS8(byte val) throws SerializeException {
    writeU8(val);
  }

  public short readS16() throws DeserializeException {
    return (short)readU16();
  }

  public void writeS16(short val) throws SerializeException {
    writeU16(val);
  }

  public int readS32() throws DeserializeException {
    return (int)readU32();
  }

  public void writeS32(int val) throws SerializeException {
    writeU32(val);
  }

  public long readS64() throws DeserializeException {
    long retVal = 0;
    /* Check for underflow */
    if ((payloadLength - dataIndex) < 4){
      throw new DeserializeException();
    }
    
    retVal =
        ((long) (payload[dataIndex++] & 0xFF) << 56 & 0xFF00000000000000L)     | 
        ((long) (payload[dataIndex++] & 0xFF) << 48 & 0x00FF000000000000L)  |
        ((long) (payload[dataIndex++] & 0xFF) << 40 & 0x0000FF0000000000L) |
        ((long) (payload[dataIndex++] & 0xFF) << 32 & 0x000000FF00000000L) |
        ((long) (payload[dataIndex++] & 0xFF) << 24 & 0x00000000FF000000L) | 
        ((long) (payload[dataIndex++] & 0xFF) << 16 & 0x0000000000FF0000L) |
        ((long) (payload[dataIndex++] & 0xFF) << 8 & 0x0000000000000FF00L)  |
        ((long) (payload[dataIndex++] & 0xFF) &      0x000000000000000FFL);

    return retVal;
  }

  public void writeS64(long val) throws SerializeException {
    if ((PLD_LEN_MAX - dataIndex) < 8){
      throw new SerializeException();
    }
    
    /* Serialize val into payload */
    payload[dataIndex++] = (byte)(val >>> 56);
    payload[dataIndex++] = (byte)(val >>> 48);
    payload[dataIndex++] = (byte)(val >>> 40);
    payload[dataIndex++] = (byte)(val >>> 32);
    payload[dataIndex++] = (byte)(val >>> 24);
    payload[dataIndex++] = (byte)(val >>> 16);
    payload[dataIndex++] = (byte)(val >>> 8);
    payload[dataIndex++] = (byte)(val);
    
    /* Update payload length if necessary */
    if (dataIndex > payloadLength){
      payloadLength = dataIndex;
    }
  }

  public short readU8() throws DeserializeException {

    short retVal = 0;
    /* Check for underflow */
    if ((payloadLength - dataIndex) < 1){
      throw new DeserializeException();
    }

    /* Deserialize val from payload */
    retVal = (short)(payload[dataIndex++] & 0xFF);
    
    return retVal;
  }

  public void writeU8(short val) throws SerializeException {
    if ((PLD_LEN_MAX - dataIndex) < 1){
      throw new SerializeException();
    }
    
    /* Serialize val into payload */
    payload[dataIndex++] = (byte)(val);
    
    /* Update payload length if necessary */
    if (dataIndex > payloadLength){
      payloadLength = dataIndex;
    }
  }

  public int readU16() throws DeserializeException {
    int retVal = 0;
    /* Check for underflow */
    if ((payloadLength - dataIndex) < 2){
      throw new DeserializeException();
    }
    
    retVal =
        (int) (payload[dataIndex++] & 0xFF) << 8 & 0xFF00 | 
        (int) (payload[dataIndex++] & 0xFF);

    return retVal;
            
  }

  public void writeU16(int val) throws SerializeException {
    if ((PLD_LEN_MAX - dataIndex) < 2){
      throw new SerializeException();
    }
    
    /* Serialize val into payload */
    payload[dataIndex++] = (byte)(val >>> 8);
    payload[dataIndex++] = (byte)(val);
    
    /* Update payload length if necessary */
    if (dataIndex > payloadLength){
      payloadLength = dataIndex;
    }
  }

  public long readU32() throws DeserializeException {
    long retVal = 0;
    /* Check for underflow */
    if ((payloadLength - dataIndex) < 4){
      throw new DeserializeException();
    }
    
    retVal = (long) (payload[dataIndex++] & 0xFF) << 24 & 0xFF000000 |
             (long) (payload[dataIndex++] & 0xFF) << 16 & 0x00FF0000 |
             (long) (payload[dataIndex++] & 0xFF) << 8  & 0x0000FF00 |
             (long) (payload[dataIndex++] & 0xFF) << 0  & 0x000000FF;
    
    return retVal;
  }

  public void writeU32(long val) throws SerializeException {
    if ((PLD_LEN_MAX - dataIndex) < 4){
      throw new SerializeException();
    }
    
    /* Serialize val into payload */
    payload[dataIndex++] = (byte)(val >>> 24);
    payload[dataIndex++] = (byte)(val >>> 16);
    payload[dataIndex++] = (byte)(val >>> 8);
    payload[dataIndex++] = (byte)(val);
    
    /* Update payload length if necessary */
    if (dataIndex > payloadLength){
      payloadLength = dataIndex;
    }
  }

  public long readU64() throws DeserializeException{
    //TODO: This will require BigInteger
    throw new DeserializeException("readU64 not currently supported.");
    //return 0L;
  }

  public void writeU64(long val) throws SerializeException {
    //TODO: This will require BigInteger
    throw new SerializeException("writeU64 not currently supported.");
  }

  public float readF32() throws DeserializeException {
    return Float.intBitsToFloat((int)readU32());
  }

  public void writeF32(float val) throws SerializeException {
    writeU32(Float.floatToIntBits(val));
  }

  public double readF64() throws DeserializeException {
    return Double.longBitsToDouble(readS64());
  }

  public void writeF64(double val) throws SerializeException {
    writeS64(Double.doubleToLongBits(val));
  }

  public boolean readBool() throws DeserializeException {
    boolean retVal;
    /* Check for underflow */
    if ((payloadLength - dataIndex) < 1){
      throw new DeserializeException();
    }
    
    //Assume zero == false any other value is true.
    retVal = payload[dataIndex++] != 0; 

    return retVal;
  }

  public void writeBool(boolean val) throws SerializeException {
    if ((PLD_LEN_MAX - dataIndex) < 1){
      throw new SerializeException();
    }
    
    /* Serialize val into payload */
    payload[dataIndex++] = (byte)(val?1:0);
    
    /* Update payload length if necessary */
    if (dataIndex > payloadLength){
      payloadLength = dataIndex;
    }
  }

  public String readStr() throws DeserializeException {
    String retVal="";
    
    if ((payloadLength - dataIndex) < 0){
      throw new DeserializeException();
    }
    
    /* Deserialize val from payload */
    retVal = StringUtil.strncpy(payload, dataIndex, payloadLength - dataIndex);
    
    /* Update the offset based on number of bytes read + 1 for null char*/
    dataIndex += retVal.length() + 1;
    
    return retVal;
  }

  public void writeStr(String val) throws SerializeException {
    int len;
    
    /* Calculate length of string */
    len = val.length();

    /* Check for overflow. +1 for null termination. */
    if((dataIndex + len + 1) > HcCell.PLD_LEN_MAX)
      throw new SerializeException();

    /* Serialize val into payload */
    System.arraycopy(val.getBytes(), 0, payload, dataIndex, len);
    
    /* Update offset based on string length */
    dataIndex += len;

    /* Null terminate */
    payload[dataIndex++] = '\0';
    

    /* Update payload length if necessary */
    if (dataIndex > payloadLength){
      payloadLength = dataIndex;
    }
  }

  public HcCell readMsg(int offset) {
    return null;
  }

  public int writeMsg(int offset, HcCell val) {
    return 0;
  }

  public int readArray(int offset, byte val[]) throws DeserializeException  {

    /* Read length and check for error */
	int len = readU16();
	
	/* Check for buffer underflow */
	if ((dataIndex + len) > payloadLength) {
	  throw new DeserializeException();
	}
	
	/* Check for insufficient room in receiving buffer */
	if ((offset + len) > val.length) {
	  throw new DeserializeException();
	}
	
	/* Copy bytes */
	for (int i=0; i<len; i++) {
	  val[i+offset] = payload[dataIndex++];
	}
	
	/* Return how many bytes were read */
	return len;
  }

  public int writeArray(int offset, byte[] data, int len) {
    return 0;
  }

  public void print() {
      int i;

      System.out.printf("RCMsg\n");
      System.out.printf("  OpCode...............: %d = ", opcode);
      switch(opcode)
      {
      case OPCODE_CALL_CMD:
        System.out.println("Call Cmd");
        break;
      case OPCODE_CALL_STS:
        System.out.println("Call Sts");
        break;
      case OPCODE_GET_CMD:
        System.out.println("Get Cmd");
        break;
      case OPCODE_GET_STS:
        System.out.println("Get Sts");
        break;
      case OPCODE_SET_CMD:
        System.out.println("Set Cmd");
        break;
      case OPCODE_SET_STS:
        System.out.println("Set Sts");
        break;
      case OPCODE_ICALL_CMD:
        System.out.println("ICall Cmd");
        break;
      case OPCODE_ICALL_STS:
        System.out.println("ICall Sts");
        break;
      case OPCODE_IGET_CMD:
        System.out.println("IGet Cmd");
        break;
      case OPCODE_IGET_STS:
        System.out.println("IGet Sts");
        break;
      case OPCODE_ISET_CMD:
        System.out.println("ISet Cmd");
        break;
      case OPCODE_ISET_STS:
        System.out.println("ISet Sts");
        break;
      case OPCODE_ADD_CMD:
        System.out.println("Add Cmd");
        break;
      case OPCODE_ADD_STS:
        System.out.println("Add Sts");
        break;
      case OPCODE_SUB_CMD:
        System.out.println("Sub Cmd");
        break;
      case OPCODE_SUB_STS:
        System.out.println("Sub Sts");
        break;
      case OPCODE_READ_CMD:
        System.out.println("Read Cmd");
        break;
      case OPCODE_READ_STS:
        System.out.println("Read Sts");
        break;
      case OPCODE_WRITE_CMD:
        System.out.println("Write Cmd");
        break;
      case OPCODE_WRITE_STS:
        System.out.println("Write Sts");
        break;
      default:
        System.out.println("Unknown");
        break;
      }
      System.out.printf("  Payload Length.......: %d\n", payloadLength);
      System.out.printf("  Payload..............:");
      for(i=0; i<payloadLength; i++){
        if (i%8 == 0 && i != 0){
          System.out.printf("\n                       :");
        }
        
        System.out.printf(" 0x%02X", payload[i]);
      }
      System.out.println("\n");
  }

  public int getPayloadLength() {
    return payloadLength;
  }

  public byte[] getPayload() {
    return payload;
  }

  public void setPayload(byte[] payload) {
    this.payload = payload;
  }
  
  public void setOffset(int offset) {
    this.dataIndex = offset;
  }
  
  public byte getOpcode() {
    return opcode;
  }
  
  public int getAvailableDecodeBytes(){
    return payloadLength - dataIndex;
  }

}
