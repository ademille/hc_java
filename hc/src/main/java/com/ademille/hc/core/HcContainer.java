package com.ademille.hc.core;

import java.util.LinkedList;

import com.ademille.hc.exception.SerializeException;
import com.ademille.hc.param.HcParam;

public class HcContainer {
  private String name = null;
  private LinkedList<HcContainer> contList = null;
  public LinkedList<HcContainer> getContList() {
    return contList;
  }

  public LinkedList<HcParam> getParamList() {
    return paramList;
  }

  private LinkedList<HcParam> paramList = null;
  private HcContainer parent = null;

  public HcContainer(String name) {
    this.name = name;
    contList = new LinkedList<HcContainer>();
    paramList = new LinkedList<HcParam>();
  }

  public void addContainer(HcContainer cont) {
    cont.parent = this;
    contList.add(cont);
  }

  public void remContainer(HcContainer cont) {
    if (contList.remove(cont))
      cont.parent = null;
  }

  public HcContainer getContainer(String name) {
    // Ignore leading "/"
    if (name.startsWith("/"))
      name = name.replaceFirst("/", "");

    // If the name is empty, we must be at the end container.
    if (name.equals(""))
      return this;

    // Split the name into containers. The first element will be
    // the current container to look for. The second element will be
    // the remainder of the path.
    String[] path = name.split("/", 2);

    // Look for the child container matching the first path element.
    for (HcContainer c : contList)
      if (c.getName().equals(path[0])) { // We found a match
        if (path.length == 1) { // No further path elements so return match.
          return c;
        } else { // There are still more path elements to process.
          // path[1] will contain the rest of the path as a single string.
          return getContainer(path[1]);
        }
      }

    // Didn't find any matches.
    return null;
  }

  public void addParam(HcParam param) {
    paramList.add(param);
    param.setParent(this);
  }

  public void remParam(HcParam param) {
    param.setParent(null);
    paramList.remove(param);
  }

  public HcParam getParam(String name) {
    // Ignore leading "/"
    if (name.startsWith("/"))
      name = name.replaceFirst("/", "");

    // Split the name into containers. The first element will be
    // the current container to look for. The second element will be
    // the remainder of the path.
    String[] path = name.split("/", 2);

    // Look for the child container matching the first path element.
    if (path.length == 1) {
      // No further path elements so search for the element in this container.
      for (HcParam param : paramList)
        if (param.getName().equals(path[0])) { // We found a match
          return param;
        }
    } else {
      // The current name references a container so search for it.
      for (HcContainer c : contList)
        if (c.getName().equals(path[0])) { // We found a match
          return getParam(path[1]);
        }
    }

    // Didn't find any param or container matches.
    return null;
  }

  public int countMatching(String name) {
    int count = 0;

    // Ignore leading "/"
    if (name.startsWith("/"))
      name = name.replaceFirst("/", "");

    // Split the name into containers. The first element will be
    // the current container to look for. The second element will be
    // the remainder of the path.
    String[] path = name.split("/", 2);

    // Look for the containers and params matching regular expression.
    if (path.length == 1) {
      // No further path elements so search for the element in this container.
      for (HcContainer c : contList) {
        if (c.getName().matches(path[0])) { // We found a match
          count++;
        }
      }

      for (HcParam param : paramList) {
        if (param.getName().matches(path[0])) { // We found a match
          count++;
        }
      }

    } else {
      // The current name references a container so search for it.
      for (HcContainer c : contList) {
        if (c.getName().matches(path[0])) { // We found a match
          return countMatching(path[1]);
        }
      }
    }

    // Didn't find any param or container matches.
    return count;
  }

  public void printMatching(String name) {

    // Ignore leading "/"
    if (name.startsWith("/"))
      name = name.replaceFirst("/", "");

    // Split the name into containers. The first element will be
    // the current container to look for. The second element will be
    // the remainder of the path.
    String[] path = name.split("/", 2);

    // Look for the containers and params matching regular expression.
    if (path.length == 1) {
      // No further path elements.
      for (HcContainer c : contList) {
        // Check for matching name.
        if (c.getName().matches(path[0])) {
          System.out.println(getPath());
        }

        // Print sub nodes
        printMatching(name);
      }

      for (HcParam param : paramList) {
        if (param.getName().matches(name)) { // We found a match
          System.out.println(param.getPath());
        }
      }

    } else {
      // The current name references a container so search for it.
      for (HcContainer c : contList) {
        if (c.getName().matches(path[0])) { // We found a match
          printMatching(path[1]);
        }
      }
    }

  }

  public void printListing() {
    // Print all container names.
    for (HcContainer c : contList) {
      System.out.println(c.getName());
    }

    // Print all parameter names, values, etc.
    for (HcParam param : paramList) {
      param.printValue();
    }
  }

  public int getNextCommonChar(String name) {
    return -1;
  }

  public static HcContainer makePath(HcContainer cont, String path) {
    HcContainer cp;
    // Ignore leading "/"
    if (path.startsWith("/"))
      path = path.replaceFirst("/", "");

    // If the name is empty, we must be at the end container.
    if (path.equals(""))
      return cont;

    // Split the name into containers. The first element will be
    // the current container to look for. The second element will be
    // the remainder of the path.
    String[] paths = path.split("/", 2);

    // Look for the child container matching the first path element.
    for (HcContainer c : cont.contList) {
      if (c.getName().equals(paths[0])) { // We found a match
        if (paths.length == 1) {
          // No further path elements so return match.
          return c;
        } else { // There are still more path elements to process.
          // path[1] will contain the rest of the path as a single string.
          return makePath(c, paths[1]);
        }
      }
    }

    // No containers with that name, so make one
    cp = new HcContainer(paths[0]);

    // Add container to this container
    cont.addContainer(cp);

    // If separator found, then recurse
    if (paths.length > 1)
      return makePath(cp, paths[1]);

    // Didn't need to recurse, so this must be the one
    return cp;
  }

  public String getPath() {
    StringBuffer str = new StringBuffer();
    if (parent != null) {
      str.append(parent.getPath());
    }

    str.append(getName() + "/");
    return str.toString();
  }

  public int serializePath(byte[] serbuf, int offset, int maxLen)
      throws SerializeException {

    // Initialize length to zero
    int len = 0;

    // Check for parent
    if (parent != null) {
      // Serialize parent path followed by this container name
      len = parent.serializePath(serbuf, offset, maxLen);

      // Check for error serializing parent path
      if (len < 0)
        throw new SerializeException();

      // Adjust serbuf and reduce maxlen
      offset += len;
      maxLen -= len;
    }

    // Check for overflow
    if (name.length() + 2 > maxLen) {
      return -1;
    }

    // Write container name to buffer
    System.arraycopy(name.getBytes(), 0, serbuf, offset, name.length());
    return len + name.length();
  }

  public String getName() {
    return name;
  }

}
