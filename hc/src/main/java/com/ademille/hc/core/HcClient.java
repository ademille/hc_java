package com.ademille.hc.core;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.ademille.hc.device.Device;
import com.ademille.hc.exception.EidException;
import com.ademille.hc.exception.ExceptionConvert;
import com.ademille.hc.exception.HcException;
import com.ademille.hc.exception.PidException;
import com.ademille.hc.exception.TimeoutException;
import com.ademille.hc.exception.TypeException;
import com.ademille.hc.exception.UnspecifiedException;
import com.ademille.hc.param.HcParam;

public class HcClient {

  private Device lowdev = null;
  private HcMessage imsg = null;
  private HcCell icell = null;
  private HcMessage omsg = null;
  private HcCell ocell = null;
  private boolean debug = false;

  private int senderrcnt = 0;
  private int recverrcnt = 0;
  private int transactionerrcnt = 0;
  private int cellerrcnt = 0;
  private int opcodeerrcnt = 0;
  private int timeouterrcnt = 0;
  private int piderrcnt = 0;
  private int typeerrcnt = 0;
  private int eiderrcnt = 0;
  private int offseterrcnt = 0;
  private int goodxactcnt = 0;
  private byte transaction = 0;

  private ReentrantLock xactMutex = null;
  private byte expTransaction = 0;
  private byte expOpCode = 0;
  private Semaphore replyEvent = null;
  private Thread readThread = null;
  private int timeoutMs = 1000;

  public HcClient(Device lowdev) {
    this(lowdev, 1000);
  }

  public HcClient(Device lowdev, int timeoutMs) {
    this.lowdev = lowdev;
    this.timeoutMs = timeoutMs;
    xactMutex = new ReentrantLock();

    imsg = new HcMessage();
    icell = new HcCell();
    omsg = new HcMessage();
    ocell = new HcCell();
    xactMutex = new ReentrantLock();
    replyEvent = new Semaphore(1);
    try {
      // Acquire so that the next acquire will block.
      replyEvent.acquire();
    } catch (InterruptedException e) {
    }

    readThread = new Thread(new Runnable() {

      @Override
      public void run() {
        readThread();
      }
    });
    readThread.start();
  }

  public void setDebug(boolean debug) {
    this.debug = debug;
  }

  public boolean isDebug() {
    return debug;
  }

  public int getSendErrCnt() {
    // Get the value
    return senderrcnt;
  }

  public int getRecvErrCnt() {
    // Get the value
    return recverrcnt;
  }

  public int getTransactionErrCnt() {
    // Get the value
    return transactionerrcnt;
  }

  public int getCellErrCnt() {
    // Get the value
    return cellerrcnt;
  }

  public int getOpCodeErrCnt() {
    // Get the value
    return opcodeerrcnt;
  }

  public int getTimeoutErrCnt() {
    // Get the value
    return timeouterrcnt;
  }

  public int getPIDErrCnt() {
    // Get the value
    return piderrcnt;
  }

  public int getTypeErrCnt() {
    // Get the value
    return typeerrcnt;
  }

  public int getEIDErrCnt() {
    // Get the value
    return eiderrcnt;
  }

  public int getOffsetErrCnt() {
    // Get the value
    return offseterrcnt;
  }

  public int getGoodXactCnt() {
    // Get the value
    return goodxactcnt;
  }

  public void call(int pid) throws HcException {
    // Begin mutual exclusion of transaction
    xactMutex.lock();

    // Format outbound message
    omsg.reset(transaction);
    ocell.reset(HcCell.OPCODE_CALL_CMD);
    ocell.writeU16(pid);
    omsg.write(ocell);

    try {
      // Perform call transaction
      callXact(pid);
    } finally {
      // End mutual exclusion of transaction
      xactMutex.unlock();
    }
  }

  public void iCall(int pid, long eid) throws HcException {
    // Begin mutual exclusion of transaction
    xactMutex.lock();

    // Format outbound message
    omsg.reset(transaction);
    ocell.reset(HcCell.OPCODE_ICALL_CMD);
    ocell.writeU16(pid);
    ocell.writeU32(eid);
    omsg.write(ocell);

    try {
      // Perform set transaction
      iCallXact(pid, eid);
    } finally {
      // End mutual exclusion of transaction
      xactMutex.unlock();
    }
  }

  private void callXact(int pid) throws HcException {
    short berr;

    /* Set expected reply parameters */
    expTransaction = transaction;
    expOpCode = HcCell.OPCODE_CALL_STS;

    /* Print outbound message if requested */
    if (debug) {
      omsg.print("Tx");
    }

    /* Send outbound message and check for error */
    try {
      omsg.send(lowdev);
    } catch (HcException hce) {
      /* Increment send error count */
      this.senderrcnt++;
      throw new UnspecifiedException();
    }

    try {

      /* Wait for response */
      if (!replyEvent.tryAcquire(timeoutMs, TimeUnit.MILLISECONDS)) {
        /* Increment timeout count */
        timeouterrcnt++;

        /* Throw timeout exception */
        throw new TimeoutException();
      }

      /* Read PID from inbound cell */
      if (icell.readU16() != pid) {
        /* Increment PID error count */
        piderrcnt++;

        /* Throw unspecified exception */
        throw new UnspecifiedException();
      }

      /* Read inbound error message */
      berr = icell.readS8();

      /* If an error was returned, convert it to an exception. */
      ExceptionConvert.convertToException(berr);

    } catch (InterruptedException e) {
      /* Throw timeout exception */
      throw new TimeoutException();
    }

    /* Increment good message count */
    goodxactcnt++;
  }

  private void iCallXact(int pid, long eid) throws HcException {
    short berr;

    /* Set expected reply parameters */
    expTransaction = transaction;
    expOpCode = HcCell.OPCODE_ICALL_STS;

    /* Print outbound message if requested */
    if (debug) {
      omsg.print("Tx");
    }

    /* Send outbound message and check for error */
    try {
      omsg.send(lowdev);
    } catch (HcException hce) {
      /* Increment send error count */
      this.senderrcnt++;
      throw new UnspecifiedException();
    }

    try {

      /* Wait for response */
      if (!replyEvent.tryAcquire(timeoutMs, TimeUnit.MILLISECONDS)) {
        /* Increment timeout count */
        timeouterrcnt++;

        /* Throw timeout exception */
        throw new TimeoutException();
      }

      /* Read PID from inbound cell */
      if (icell.readU16() != pid) {
        /* Increment PID error count */
        piderrcnt++;

        /* Throw unspecified exception */
        throw new UnspecifiedException();
      }

      /* Read EID from inbound cell */
      if (icell.readU32() != eid) {
        /* Increment PID error count */
        eiderrcnt++;

        /* Throw EID exception */
        throw new EidException();
      }

      /* Read inbound error message */
      berr = icell.readS8();

      /* If an error was returned, convert it to an exception. */
      ExceptionConvert.convertToException(berr);

    } catch (InterruptedException e) {
      /* Throw timeout exception */
      throw new TimeoutException();
    }

    /* Increment good message count */
    goodxactcnt++;
  }

  /////////////////////////////////////////////
  // This is for reading a file
  /////////////////////////////////////////////
  private int read(int pid, int offset, byte[] val) throws HcException {
    int merr;
    int len = 0;
    // Take the transaction mutex
    xactMutex.lock();

    try {

      // Perform get transaction
      readXact(pid, offset, val.length);

      // Read value and error from inbound cell (already skipped past PID, offset and
      // type)
      len = icell.readArray(0, val);
      merr = icell.readU8();

      /* If an error was returned, convert it to an exception. */
      ExceptionConvert.convertToException(merr);

    } finally {
      // Give the transaction mutex
      xactMutex.unlock();

    }

    return len;
  }

  /////////////////////////////////////////////////////
  // This is for writing a file.
  /////////////////////////////////////////////////////
 private void write(int pid, int offset, byte[] val, int len) throws HcException {

    // Begin mutual exclusion of transaction
    xactMutex.lock();

    // Format outbound message
    omsg.reset(this.transaction);
    ocell.reset(HcCell.OPCODE_WRITE_CMD);
    ocell.writeU16(pid);
    ocell.writeU32(offset);
    ocell.writeArray(0, val, len);
    omsg.write(ocell);

    // Perform write transaction
    try {
      writeXact(pid, offset);
    } catch (HcException hce) {
      throw hce;
    } finally {
      // End mutual exclusion of transaction
      xactMutex.unlock();
    }

  }

  public void downloadSif(int pid, String filename) throws HcException {
    LinkedList<Byte> byteList = new LinkedList<>();
    ByteArrayOutputStream byteStream = new ByteArrayOutputStream(0xFFFF);
    int filePos = 0;
    int retryCount = 0;
    final int MAX_RETRY = 3;
    boolean transferComplete = false;

    // Create file buffer to download the SIF
    byte[] fileBuffer = new byte[HcCell.PLD_LEN_MAX - 9];

    // Open local file and check for error
    Path p = Paths.get(filename);
    try (OutputStream out = new BufferedOutputStream(Files.newOutputStream(p, CREATE, APPEND))) {
      // Transfer file piece by piece
      while (!transferComplete) {
        try {
          // Read part of remote file
          int len = read(pid, filePos, fileBuffer);
          // If read didn't throw an exception, reset the retryCount
          retryCount = 0;
          // Update the file position to retrieve
          filePos += len;
          // Write to the local file
          out.write(fileBuffer, 0, len);
          
          // Write to byte Stream
          byteStream.write(fileBuffer, 0, len);

          // TODO: This isn't really a good check. The server could just return fewer
          // characters but still have more.
          // Check for done
          if (len < HcCell.PLD_LEN_MAX - 9)
            transferComplete = true;

        } catch (HcException e) {
          // The HC Read failed. Try again.
          retryCount++;
          if (retryCount >= MAX_RETRY) {
            throw new UnspecifiedException();
          }
        }
      } // end while !transferComplete
    } catch (IOException x) {
      throw new UnspecifiedException();
    }

    System.out.println("Finished retrieving XML file.");

    //Convert to a string
    byte[] byteArray = byteStream.toByteArray();
    String xml = new String(byteArray);
    parseXml(byteArray);

  }
  
  void parseXml(byte[] xmlBytes) {
    
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    try {
      DocumentBuilder builder = factory.newDocumentBuilder();
      ByteArrayInputStream input = new ByteArrayInputStream(
         xmlBytes);
      Document doc = builder.parse(input);
      Element root = doc.getDocumentElement();
      String name = root.getNodeName();
      System.out.println(name);
      NodeList children = root.getChildNodes();
      for (int i=0; i< children.getLength(); i++) {
        Node n = children.item(i);
        System.out.println(n.getNodeName());
      }

    } catch (ParserConfigurationException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (SAXException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    
  }

  // private void deserializeParam(int paramid, HcContainer top) throws
  // HcException {
  // byte type;
  // byte ctype; // Caridnal Type
  // String path;
  // String name;
  // HcContainer cont;
  // HcParam param;
  //
  // /* Deserialize the path */
  // path = imsg.readStr();
  //
  // /* Create path from top container */
  // if ((cont = HcContainer.makePath(top, path)) == null)
  // return;
  //
  // /* Deserialize the name */
  // name = imsg.readStr();
  //
  // /* Deserialize the type */
  // type = imsg.readS8();
  //
  // /* Determine cardinal type information */
  // ctype = (byte) (type & HcParam.HCPARAM_TYPE_CARD);
  //
  // /* Deserialize unique types */
  // switch (ctype) {
  // case HcParam.HCPARAM_TYPE_S8:
  // /* Check for cardinal or table type */
  // param = new HcS8Stub(name, this, paramid);
  // break;
  // case HcParam.HCPARAM_TYPE_S16:
  // param = new HcS16Stub(name, this, paramid);
  // break;
  // case HcParam.HCPARAM_TYPE_S32:
  // param = new HcS32Stub(name, this, paramid);
  // break;
  // case HcParam.HCPARAM_TYPE_S64:
  // param = new HcS64Stub(name, this, paramid);
  // break;
  // case HcParam.HCPARAM_TYPE_U8:
  // param = new HcU8Stub(name, this, paramid);
  // break;
  // case HcParam.HCPARAM_TYPE_U16:
  // param = new HcU16Stub(name, this, paramid);
  // break;
  // case HcParam.HCPARAM_TYPE_U32:
  // param = new HcU32Stub(name, this, paramid);
  // break;
  // case HcParam.HCPARAM_TYPE_U64:
  // param = new HcU64(name, 0, 0, 0, null);
  // break;
  // case HcParam.HCPARAM_TYPE_F32:
  // param = new HcF32Stub(name, this, paramid);
  // break;
  // case HcParam.HCPARAM_TYPE_F64:
  // param = new HcF64Stub(name, this, paramid);
  // break;
  // case HcParam.HCPARAM_TYPE_BOOL:
  // param = new HcBoolStub(name, this, paramid);
  // break;
  // case HcParam.HCPARAM_TYPE_STR:
  // param = new HcStringStub(name, this, paramid);
  // break;
  // case HcParam.HCPARAM_TYPE_ARRAY:
  // param = new HcArray(name, 0, 0, 0, null);
  // break;
  // case HcParam.HCPARAM_TYPE_CALL:
  // param = new HcCallStub(name, this, paramid);
  // break;
  // default:
  // System.err.println("[HcClient]Unrecognized type");
  //
  // return;
  // }
  //
  // try {
  // param.deserialize(imsg);
  // cont.addParam(param);
  // /*
  // * ASSERT(param != NULL); RCParamDeserialize(param, serbuf+offset,
  // * serlen-offset); AddParameter(param, cont, srv, paramid);
  // */
  // } catch (HcException rce) {
  // System.err.println("Error deserialize param: " + name);
  // }
  //
  // // System.out.println("\n**********************");
  // /*
  // * System.out.println("Deserialized Element "); System.out.println("paramid: "
  // +
  // * paramid); System.out.println("path: " + path); System.out.println("name: "
  // +
  // * name); System.out.println("type: " + typeName);
  // * System.out.println("PARAM INFO:");
  // */
  // // param.printInfo();
  // // System.out.println("CONT LISTING:");
  // // cont.printListing();
  //
  // }

  public byte getS8(int id) throws HcException {
    byte retVal = 0;
    byte merr = 0;

    /* Take the transaction mutex */
    xactMutex.lock();

    /* Perform get transaction */
    try {
      getXact(id, HcParam.HCPARAM_TYPE_S8);

      /* Set return value */
      retVal = icell.readS8();
      merr = icell.readS8();
      /* If an error was returned, convert it to an exception. */
      ExceptionConvert.convertToException(merr);
    } finally {
      /* Give the transaction mutex */
      xactMutex.unlock();
    }

    return retVal;
  }

  public void setS8(int id, byte val) throws HcException {

    /* Begin mutual exclusion of transaction */
    xactMutex.lock();

    /* Format outbound message */
    try {
      omsg.reset(transaction);
      ocell.reset(HcCell.OPCODE_SET_CMD);
      ocell.writeU16(id);
      ocell.writeU8(HcParam.HCPARAM_TYPE_S8);
      ocell.writeS8(val);
      omsg.write(ocell);

      /* Perform set transaction */
      setXact(id, HcParam.HCPARAM_TYPE_S8);
    } finally {
      /* End mutual exclusion of transaction */
      xactMutex.unlock();
    }
  }

  public byte iGetS8(int pid, long eid) throws HcException {
    byte retVal = 0;
    byte merr = 0;

    /* Take the transaction mutex */
    xactMutex.lock();

    /* Perform get transaction */
    try {
      iGetXact(pid, eid, HcParam.HCPARAM_TYPE_S8);

      /* Set return value */
      retVal = icell.readS8();
      merr = icell.readS8();
      /* If an error was returned, convert it to an exception. */
      ExceptionConvert.convertToException(merr);
    } finally {
      /* Give the transaction mutex */
      xactMutex.unlock();
    }

    return retVal;
  }

  public void iSetS8(int pid, long eid, byte val) throws HcException {

    /* Begin mutual exclusion of transaction */
    xactMutex.lock();

    /* Format outbound message */
    try {
      omsg.reset(transaction);
      ocell.reset(HcCell.OPCODE_ISET_CMD);
      ocell.writeU16(pid);
      ocell.writeU32(eid);
      ocell.writeU8(HcParam.HCPARAM_TYPE_S8);
      ocell.writeS8(val);
      omsg.write(ocell);

      /* Perform set transaction */
      iSetXact(pid, eid, HcParam.HCPARAM_TYPE_S8);
    } finally {
      /* End mutual exclusion of transaction */
      xactMutex.unlock();
    }
  }

  public short getS16(int id) throws HcException {
    short retVal = 0;
    byte merr = 0;

    /* Take the transaction mutex */
    xactMutex.lock();

    /* Perform get transaction */
    try {
      getXact(id, HcParam.HCPARAM_TYPE_S16);

      /* Set return value */
      retVal = icell.readS16();
      merr = icell.readS8();

      /* Convert to exception if necessary */
      ExceptionConvert.convertToException(merr);
    } finally {
      /* Give the transaction mutex */
      xactMutex.unlock();
    }

    return retVal;
  }

  public void setS16(int id, short val) throws HcException {
    /* Begin mutual exclusion of transaction */
    xactMutex.lock();

    /* Format outbound message */
    try {
      omsg.reset(transaction);
      ocell.reset(HcCell.OPCODE_SET_CMD);
      ocell.writeU16(id);
      ocell.writeU8(HcParam.HCPARAM_TYPE_S16);
      ocell.writeS16(val);
      omsg.write(ocell);

      /* Perform set transaction */
      setXact(id, HcParam.HCPARAM_TYPE_S16);
    } finally {
      /* End mutual exclusion of transaction */
      xactMutex.unlock();
    }
  }

  public short iGetS16(int pid, long eid) throws HcException {
    short retVal = 0;
    byte merr = 0;

    /* Take the transaction mutex */
    xactMutex.lock();

    /* Perform get transaction */
    try {
      iGetXact(pid, eid, HcParam.HCPARAM_TYPE_S16);

      /* Set return value */
      retVal = icell.readS16();
      merr = icell.readS8();

      /* Convert to exception if necessary */
      ExceptionConvert.convertToException(merr);
    } finally {
      /* Give the transaction mutex */
      xactMutex.unlock();
    }

    return retVal;
  }

  public void iSetS16(int pid, long eid, short val) throws HcException {
    /* Begin mutual exclusion of transaction */
    xactMutex.lock();

    /* Format outbound message */
    try {
      omsg.reset(transaction);
      ocell.reset(HcCell.OPCODE_ISET_CMD);
      ocell.writeU16(pid);
      ocell.writeU32(eid);
      ocell.writeU8(HcParam.HCPARAM_TYPE_S16);
      ocell.writeS16(val);
      omsg.write(ocell);

      /* Perform set transaction */
      iSetXact(pid, eid, HcParam.HCPARAM_TYPE_S16);
    } finally {
      /* End mutual exclusion of transaction */
      xactMutex.unlock();
    }
  }

  public int getS32(int id) throws HcException {
    int retVal = 0;
    byte merr = 0;

    /* Take the transaction mutex */
    xactMutex.lock();

    /* Perform get transaction */
    try {
      getXact(id, HcParam.HCPARAM_TYPE_S32);

      /* Set return value */
      retVal = icell.readS32();
      merr = icell.readS8();
      ExceptionConvert.convertToException(merr);
    } finally {
      /* Give the transaction mutex */
      xactMutex.unlock();
    }

    return retVal;
  }

  public void setS32(int id, int val) throws HcException {
    /* Begin mutual exclusion of transaction */
    xactMutex.lock();

    /* Format outbound message */
    try {
      omsg.reset(transaction);
      ocell.reset(HcCell.OPCODE_SET_CMD);
      ocell.writeU16(id);
      ocell.writeU8(HcParam.HCPARAM_TYPE_S32);
      ocell.writeS32(val);
      omsg.write(ocell);

      /* Perform set transaction */
      setXact(id, HcParam.HCPARAM_TYPE_S32);
    } finally {
      /* End mutual exclusion of transaction */
      xactMutex.unlock();
    }
  }

  public int iGetS32(int pid, long eid) throws HcException {
    int retVal = 0;
    byte merr = 0;

    /* Take the transaction mutex */
    xactMutex.lock();

    /* Perform get transaction */
    try {
      iGetXact(pid, eid, HcParam.HCPARAM_TYPE_S32);

      /* Set return value */
      retVal = icell.readS32();
      merr = icell.readS8();
      ExceptionConvert.convertToException(merr);
    } finally {
      /* Give the transaction mutex */
      xactMutex.unlock();
    }

    return retVal;
  }

  public void iSetS32(int pid, long eid, int val) throws HcException {
    /* Begin mutual exclusion of transaction */
    xactMutex.lock();

    /* Format outbound message */
    try {
      omsg.reset(transaction);
      ocell.reset(HcCell.OPCODE_ISET_CMD);
      ocell.writeU16(pid);
      ocell.writeU32(eid);
      ocell.writeU8(HcParam.HCPARAM_TYPE_S32);
      ocell.writeS32(val);
      omsg.write(ocell);

      /* Perform set transaction */
      iSetXact(pid, eid, HcParam.HCPARAM_TYPE_S32);
    } finally {
      /* End mutual exclusion of transaction */
      xactMutex.unlock();
    }
  }

  public long getS64(int id) throws HcException {
    long retVal = 0;
    byte merr = 0;

    /* Take the transaction mutex */
    xactMutex.lock();

    /* Perform get transaction */
    try {
      getXact(id, HcParam.HCPARAM_TYPE_S64);

      /* Set return value */
      retVal = icell.readS64();
      merr = icell.readS8();
      ExceptionConvert.convertToException(merr);
    } finally {
      /* Give the transaction mutex */
      xactMutex.unlock();
    }

    return retVal;
  }

  public void setS64(int id, long val) throws HcException {
    /* Begin mutual exclusion of transaction */
    xactMutex.lock();

    /* Format outbound message */
    try {
      omsg.reset(transaction);
      ocell.reset(HcCell.OPCODE_SET_CMD);
      ocell.writeU16(id);
      ocell.writeU8(HcParam.HCPARAM_TYPE_S64);
      ocell.writeS64(val);
      omsg.write(ocell);

      /* Perform set transaction */
      setXact(id, HcParam.HCPARAM_TYPE_S64);
    } finally {
      /* End mutual exclusion of transaction */
      xactMutex.unlock();
    }
  }

  public long iGetS64(int pid, long eid) throws HcException {
    long retVal = 0;
    byte merr = 0;

    /* Take the transaction mutex */
    xactMutex.lock();

    /* Perform get transaction */
    try {
      iGetXact(pid, eid, HcParam.HCPARAM_TYPE_S64);

      /* Set return value */
      retVal = icell.readS64();
      merr = icell.readS8();
      ExceptionConvert.convertToException(merr);
    } finally {
      /* Give the transaction mutex */
      xactMutex.unlock();
    }

    return retVal;
  }

  public void iSetS64(int pid, long eid, long val) throws HcException {
    /* Begin mutual exclusion of transaction */
    xactMutex.lock();

    /* Format outbound message */
    try {
      omsg.reset(transaction);
      ocell.reset(HcCell.OPCODE_ISET_CMD);
      ocell.writeU16(pid);
      ocell.writeU32(eid);
      ocell.writeU8(HcParam.HCPARAM_TYPE_S64);
      ocell.writeS64(val);
      omsg.write(ocell);

      /* Perform set transaction */
      iSetXact(pid, eid, HcParam.HCPARAM_TYPE_S64);
    } finally {
      /* End mutual exclusion of transaction */
      xactMutex.unlock();
    }
  }

  public short getU8(int id) throws HcException {
    short retVal = 0;
    byte merr = 0;

    /* Take the transaction mutex */
    xactMutex.lock();

    /* Perform get transaction */
    try {
      getXact(id, HcParam.HCPARAM_TYPE_U8);

      /* Set return value */
      retVal = icell.readU8();
      merr = icell.readS8();
      ExceptionConvert.convertToException(merr);
    } finally {
      /* Give the transaction mutex */
      xactMutex.unlock();
    }

    return retVal;
  }

  public void setU8(int id, short val) throws HcException {
    /* Begin mutual exclusion of transaction */
    xactMutex.lock();

    /* Format outbound message */
    try {
      omsg.reset(transaction);
      ocell.reset(HcCell.OPCODE_SET_CMD);
      ocell.writeU16(id);
      ocell.writeU8(HcParam.HCPARAM_TYPE_U8);
      ocell.writeU8(val);
      omsg.write(ocell);

      /* Perform set transaction */
      setXact(id, HcParam.HCPARAM_TYPE_U8);
    } finally {
      /* End mutual exclusion of transaction */
      xactMutex.unlock();
    }
  }

  public short iGetU8(int pid, long eid) throws HcException {
    short retVal = 0;
    byte merr = 0;

    /* Take the transaction mutex */
    xactMutex.lock();

    /* Perform get transaction */
    try {
      iGetXact(pid, eid, HcParam.HCPARAM_TYPE_U8);

      /* Set return value */
      retVal = icell.readU8();
      merr = icell.readS8();
      ExceptionConvert.convertToException(merr);
    } finally {
      /* Give the transaction mutex */
      xactMutex.unlock();
    }

    return retVal;
  }

  public void iSetU8(int pid, long eid, short val) throws HcException {
    /* Begin mutual exclusion of transaction */
    xactMutex.lock();

    /* Format outbound message */
    try {
      omsg.reset(transaction);
      ocell.reset(HcCell.OPCODE_ISET_CMD);
      ocell.writeU16(pid);
      ocell.writeU32(eid);
      ocell.writeU8(HcParam.HCPARAM_TYPE_U8);
      ocell.writeU8(val);
      omsg.write(ocell);

      /* Perform set transaction */
      iSetXact(pid, eid, HcParam.HCPARAM_TYPE_U8);
    } finally {
      /* End mutual exclusion of transaction */
      xactMutex.unlock();
    }
  }

  public int getU16(int id) throws HcException {
    int retVal = 0;
    byte merr = 0;

    /* Take the transaction mutex */
    xactMutex.lock();

    /* Perform get transaction */
    try {
      getXact(id, HcParam.HCPARAM_TYPE_U16);

      /* Set return value */
      retVal = icell.readU16();
      merr = icell.readS8();
      ExceptionConvert.convertToException(merr);
    } finally {
      /* Give the transaction mutex */
      xactMutex.unlock();
    }

    return retVal;
  }

  public void setU16(int id, int val) throws HcException {
    /* Begin mutual exclusion of transaction */
    xactMutex.lock();

    /* Format outbound message */
    try {
      omsg.reset(transaction);
      ocell.reset(HcCell.OPCODE_SET_CMD);
      ocell.writeU16(id);
      ocell.writeU8(HcParam.HCPARAM_TYPE_U16);
      ocell.writeU16(val);
      omsg.write(ocell);

      /* Perform set transaction */
      setXact(id, HcParam.HCPARAM_TYPE_U16);
    } finally {
      /* End mutual exclusion of transaction */
      xactMutex.unlock();
    }
  }

  public int iGetU16(int pid, long eid) throws HcException {
    int retVal = 0;
    byte merr = 0;

    /* Take the transaction mutex */
    xactMutex.lock();

    /* Perform get transaction */
    try {
      iGetXact(pid, eid, HcParam.HCPARAM_TYPE_U16);

      /* Set return value */
      retVal = icell.readU16();
      merr = icell.readS8();
      ExceptionConvert.convertToException(merr);
    } finally {
      /* Give the transaction mutex */
      xactMutex.unlock();
    }

    return retVal;
  }

  public void iSetU16(int pid, long eid, int val) throws HcException {
    /* Begin mutual exclusion of transaction */
    xactMutex.lock();

    /* Format outbound message */
    try {
      omsg.reset(transaction);
      ocell.reset(HcCell.OPCODE_ISET_CMD);
      ocell.writeU16(pid);
      ocell.writeU32(eid);
      ocell.writeU8(HcParam.HCPARAM_TYPE_U16);
      ocell.writeU16(val);
      omsg.write(ocell);

      /* Perform set transaction */
      iSetXact(pid, eid, HcParam.HCPARAM_TYPE_U16);
    } finally {
      /* End mutual exclusion of transaction */
      xactMutex.unlock();
    }
  }

  public long getU32(int id) throws HcException {
    long retVal = 0;
    byte merr = 0;

    /* Take the transaction mutex */
    xactMutex.lock();

    /* Perform get transaction */
    try {
      getXact(id, HcParam.HCPARAM_TYPE_U32);

      /* Set return value */
      retVal = icell.readU32();
      /* Convert message error to exception if necessary. */
      merr = icell.readS8();
      ExceptionConvert.convertToException(merr);
    } finally {
      /* Give the transaction mutex */
      xactMutex.unlock();
    }

    return retVal;
  }

  public void setU32(int id, long val) throws HcException {
    /* Begin mutual exclusion of transaction */
    xactMutex.lock();

    /* Format outbound message */
    try {
      omsg.reset(transaction);
      ocell.reset(HcCell.OPCODE_SET_CMD);
      ocell.writeU16(id);
      ocell.writeU8(HcParam.HCPARAM_TYPE_U32);
      ocell.writeU32(val);
      omsg.write(ocell);

      /* Perform set transaction */
      setXact(id, HcParam.HCPARAM_TYPE_U32);
    } finally {
      /* End mutual exclusion of transaction */
      xactMutex.unlock();
    }
  }

  public long iGetU32(int pid, long eid) throws HcException {
    long retVal = 0;
    byte merr = 0;

    /* Take the transaction mutex */
    xactMutex.lock();

    /* Perform get transaction */
    try {
      iGetXact(pid, eid, HcParam.HCPARAM_TYPE_U32);

      /* Set return value */
      retVal = icell.readU32();
      /* Convert message error to exception if necessary. */
      merr = icell.readS8();
      ExceptionConvert.convertToException(merr);
    } finally {
      /* Give the transaction mutex */
      xactMutex.unlock();
    }

    return retVal;
  }

  public void iSetU32(int pid, long eid, long val) throws HcException {
    /* Begin mutual exclusion of transaction */
    xactMutex.lock();

    /* Format outbound message */
    try {
      omsg.reset(transaction);
      ocell.reset(HcCell.OPCODE_ISET_CMD);
      ocell.writeU16(pid);
      ocell.writeU32(eid);
      ocell.writeU8(HcParam.HCPARAM_TYPE_U32);
      ocell.writeU32(val);
      omsg.write(ocell);

      /* Perform set transaction */
      iSetXact(pid, eid, HcParam.HCPARAM_TYPE_U32);
    } finally {
      /* End mutual exclusion of transaction */
      xactMutex.unlock();
    }
  }

  public float getF32(int id) throws HcException {
    float retVal = 0;
    byte merr = 0;

    /* Take the transaction mutex */
    xactMutex.lock();

    /* Perform get transaction */
    try {
      getXact(id, HcParam.HCPARAM_TYPE_F32);

      /* Set return value */
      retVal = icell.readF32();
      /* Convert message error to exception if necessary. */
      merr = icell.readS8();
      ExceptionConvert.convertToException(merr);
    } finally {
      /* Give the transaction mutex */
      xactMutex.unlock();
    }

    return retVal;
  }

  public void setF32(int id, float val) throws HcException {
    /* Begin mutual exclusion of transaction */
    xactMutex.lock();

    /* Format outbound message */
    try {
      omsg.reset(transaction);
      ocell.reset(HcCell.OPCODE_SET_CMD);
      ocell.writeU16(id);
      ocell.writeU8(HcParam.HCPARAM_TYPE_F32);
      ocell.writeF32(val);
      omsg.write(ocell);

      /* Perform set transaction */
      setXact(id, HcParam.HCPARAM_TYPE_F32);
    } finally {
      /* End mutual exclusion of transaction */
      xactMutex.unlock();
    }
  }

  public float iGetF32(int pid, long eid) throws HcException {
    float retVal = 0;
    byte merr = 0;

    /* Take the transaction mutex */
    xactMutex.lock();

    /* Perform get transaction */
    try {
      iGetXact(pid, eid, HcParam.HCPARAM_TYPE_F32);

      /* Set return value */
      retVal = icell.readF32();
      /* Convert message error to exception if necessary. */
      merr = icell.readS8();
      ExceptionConvert.convertToException(merr);
    } finally {
      /* Give the transaction mutex */
      xactMutex.unlock();
    }

    return retVal;
  }

  public void iSetF32(int pid, long eid, float val) throws HcException {
    /* Begin mutual exclusion of transaction */
    xactMutex.lock();

    /* Format outbound message */
    try {
      omsg.reset(transaction);
      ocell.reset(HcCell.OPCODE_ISET_CMD);
      ocell.writeU16(pid);
      ocell.writeU32(eid);
      ocell.writeU8(HcParam.HCPARAM_TYPE_F32);
      ocell.writeF32(val);
      omsg.write(ocell);

      /* Perform set transaction */
      iSetXact(pid, eid, HcParam.HCPARAM_TYPE_F32);
    } finally {
      /* End mutual exclusion of transaction */
      xactMutex.unlock();
    }
  }

  public double getF64(int id) throws HcException {
    double retVal = 0;
    byte merr = 0;

    /* Take the transaction mutex */
    xactMutex.lock();

    /* Perform get transaction */
    try {
      getXact(id, HcParam.HCPARAM_TYPE_F64);

      /* Set return value */
      retVal = icell.readF64();
      /* Convert message error to exception if necessary. */
      merr = icell.readS8();
      ExceptionConvert.convertToException(merr);
    } finally {
      /* Give the transaction mutex */
      xactMutex.unlock();
    }

    return retVal;
  }

  public void setF64(int id, double val) throws HcException {
    /* Begin mutual exclusion of transaction */
    xactMutex.lock();

    /* Format outbound message */
    try {
      omsg.reset(transaction);
      ocell.reset(HcCell.OPCODE_SET_CMD);
      ocell.writeU16(id);
      ocell.writeU8(HcParam.HCPARAM_TYPE_F64);
      ocell.writeF64(val);
      omsg.write(ocell);

      /* Perform set transaction */
      setXact(id, HcParam.HCPARAM_TYPE_F64);
    } finally {
      /* End mutual exclusion of transaction */
      xactMutex.unlock();
    }
  }

  public double iGetF64(int pid, long eid) throws HcException {
    double retVal = 0;
    byte merr = 0;

    /* Take the transaction mutex */
    xactMutex.lock();

    /* Perform get transaction */
    try {
      iGetXact(pid, eid, HcParam.HCPARAM_TYPE_F64);

      /* Set return value */
      retVal = icell.readF64();
      /* Convert message error to exception if necessary. */
      merr = icell.readS8();
      ExceptionConvert.convertToException(merr);
    } finally {
      /* Give the transaction mutex */
      xactMutex.unlock();
    }

    return retVal;
  }

  public void iSetF64(int pid, long eid, double val) throws HcException {
    /* Begin mutual exclusion of transaction */
    xactMutex.lock();

    /* Format outbound message */
    try {
      omsg.reset(transaction);
      ocell.reset(HcCell.OPCODE_ISET_CMD);
      ocell.writeU16(pid);
      ocell.writeU32(eid);
      ocell.writeU8(HcParam.HCPARAM_TYPE_F64);
      ocell.writeF64(val);
      omsg.write(ocell);

      /* Perform set transaction */
      iSetXact(pid, eid, HcParam.HCPARAM_TYPE_F64);
    } finally {
      /* End mutual exclusion of transaction */
      xactMutex.unlock();
    }
  }

  public boolean getBool(int id) throws HcException {
    boolean retVal = false;
    byte merr = 0;

    /* Take the transaction mutex */
    xactMutex.lock();

    /* Perform get transaction */
    try {
      getXact(id, HcParam.HCPARAM_TYPE_BOOL);

      /* Set return value */
      retVal = icell.readBool();
      /* Convert message error to exception if necessary. */
      merr = icell.readS8();
      ExceptionConvert.convertToException(merr);
    } finally {
      /* Give the transaction mutex */
      xactMutex.unlock();
    }

    return retVal;
  }

  public void setBool(int id, boolean val) throws HcException {
    /* Begin mutual exclusion of transaction */
    xactMutex.lock();

    /* Format outbound message */
    try {
      omsg.reset(transaction);
      ocell.reset(HcCell.OPCODE_SET_CMD);
      ocell.writeU16(id);
      ocell.writeU8(HcParam.HCPARAM_TYPE_BOOL);
      ocell.writeBool(val);
      omsg.write(ocell);

      /* Perform set transaction */
      setXact(id, HcParam.HCPARAM_TYPE_BOOL);
    } finally {
      /* End mutual exclusion of transaction */
      xactMutex.unlock();
    }
  }

  public boolean iGetBool(int pid, long eid) throws HcException {
    boolean retVal = false;
    byte merr = 0;

    /* Take the transaction mutex */
    xactMutex.lock();

    /* Perform get transaction */
    try {
      iGetXact(pid, eid, HcParam.HCPARAM_TYPE_BOOL);

      /* Set return value */
      retVal = icell.readBool();
      /* Convert message error to exception if necessary. */
      merr = icell.readS8();
      ExceptionConvert.convertToException(merr);
    } finally {
      /* Give the transaction mutex */
      xactMutex.unlock();
    }

    return retVal;
  }

  public void iSetBool(int pid, long eid, boolean val) throws HcException {
    /* Begin mutual exclusion of transaction */
    xactMutex.lock();

    /* Format outbound message */
    try {
      omsg.reset(transaction);
      ocell.reset(HcCell.OPCODE_ISET_CMD);
      ocell.writeU16(pid);
      ocell.writeU32(eid);
      ocell.writeU8(HcParam.HCPARAM_TYPE_BOOL);
      ocell.writeBool(val);
      omsg.write(ocell);

      /* Perform set transaction */
      iSetXact(pid, eid, HcParam.HCPARAM_TYPE_BOOL);
    } finally {
      /* End mutual exclusion of transaction */
      xactMutex.unlock();
    }
  }

  public String getString(int id) throws HcException {
    String retVal;
    byte merr = 0;

    /* Take the transaction mutex */
    xactMutex.lock();

    /* Perform get transaction */
    try {
      getXact(id, HcParam.HCPARAM_TYPE_STR);

      /* Set return value */
      retVal = icell.readStr();
      /* Convert message error to exception if necessary. */
      merr = icell.readS8();
      ExceptionConvert.convertToException(merr);
    } finally {
      /* Give the transaction mutex */
      xactMutex.unlock();
    }

    return retVal;
  }

  public void setString(int id, String val) throws HcException {
    /* Begin mutual exclusion of transaction */
    xactMutex.lock();

    /* Format outbound message */
    try {
      omsg.reset(transaction);
      ocell.reset(HcCell.OPCODE_SET_CMD);
      ocell.writeU16(id);
      ocell.writeU8(HcParam.HCPARAM_TYPE_STR);
      ocell.writeStr(val);
      omsg.write(ocell);

      /* Perform set transaction */
      setXact(id, HcParam.HCPARAM_TYPE_STR);
    } finally {
      /* End mutual exclusion of transaction */
      xactMutex.unlock();
    }
  }

  public String iGetString(int pid, long eid) throws HcException {
    String retVal;
    byte merr = 0;

    /* Take the transaction mutex */
    xactMutex.lock();

    /* Perform get transaction */
    try {
      iGetXact(pid, eid, HcParam.HCPARAM_TYPE_STR);

      /* Set return value */
      retVal = icell.readStr();
      /* Convert message error to exception if necessary. */
      merr = icell.readS8();
      ExceptionConvert.convertToException(merr);
    } finally {
      /* Give the transaction mutex */
      xactMutex.unlock();
    }

    return retVal;
  }

  public void iSetString(int pid, long eid, String val) throws HcException {
    /* Begin mutual exclusion of transaction */
    xactMutex.lock();

    /* Format outbound message */
    try {
      omsg.reset(transaction);
      ocell.reset(HcCell.OPCODE_ISET_CMD);
      ocell.writeU16(pid);
      ocell.writeU32(eid);
      ocell.writeU8(HcParam.HCPARAM_TYPE_STR);
      ocell.writeStr(val);
      omsg.write(ocell);

      /* Perform set transaction */
      iSetXact(pid, eid, HcParam.HCPARAM_TYPE_STR);
    } finally {
      /* End mutual exclusion of transaction */
      xactMutex.unlock();
    }
  }

  public void callTable(int pid, long eid) throws HcException {
    /* Begin mutual exclusion of transaction */
    xactMutex.lock();

    /* Format outbound message */
    try {
      omsg.reset(transaction);
      ocell.reset(HcCell.OPCODE_ISET_CMD);
      ocell.writeU16(pid);
      ocell.writeU32(eid);
      ocell.writeU8(HcParam.HCPARAM_TYPE_CALL);
      omsg.write(ocell);

      /* Perform set transaction */
      iSetXact(pid, eid, HcParam.HCPARAM_TYPE_CALL);
    } finally {
      /* End mutual exclusion of transaction */
      xactMutex.unlock();
    }
  }

  private void getXact(int pid, short type) throws HcException {

    // [transaction, opcode, len1, len2, pid1, pid2, type, data1, data..n]
    /* Set expected reply parameters */
    expTransaction = transaction;
    expOpCode = HcCell.OPCODE_GET_STS;

    /* Increment transaction number */
    omsg.reset(transaction++);

    ocell.reset(HcCell.OPCODE_GET_CMD);
    ocell.writeU16(pid);
    ocell.writeU8(type);
    omsg.write(ocell);

    /* Print outbound message if requested */
    if (debug) {
      omsg.print("Tx");
    }

    /* Send outbound message and check for error */
    try {
      omsg.send(lowdev);
    } catch (HcException hce) {
      /* Increment send error count */
      this.senderrcnt++;
      throw hce;
    }

    try {

      /* Wait for response */
      if (!replyEvent.tryAcquire(timeoutMs, TimeUnit.MILLISECONDS)) {
        /* Increment timeout count */
        timeouterrcnt++;

        /* Throw timeout exception */
        throw new TimeoutException();
      }

      /* Read PID from inbound cell */
      if (icell.readU16() != pid) {
        /* Increment PID error count */
        piderrcnt++;

        /* Throw unspecified exception */
        throw new PidException();
      }

      /* Read parameter type from inbound cell */
      if (icell.readU8() != type) {
        /* Increment PID error count */
        typeerrcnt++;

        /* Throw unspecified exception */
        throw new TypeException();
      }
    } catch (InterruptedException e) {
      /* Throw timeout exception */
      throw new TimeoutException();
    }

    /* Increment good message count */
    goodxactcnt++;

  }

  private void setXact(int pid, short type) throws HcException {
    short berr;

    /* Set expected reply parameters */
    expTransaction = transaction;
    expOpCode = HcCell.OPCODE_SET_STS;

    /* Print outbound message if requested */
    if (debug) {
      omsg.print("Tx");
    }

    /* Send outbound message and check for error */
    try {
      omsg.send(lowdev);
    } catch (HcException hce) {
      /* Increment send error count */
      this.senderrcnt++;
      throw new UnspecifiedException();
    }

    try {

      /* Wait for response */
      if (!replyEvent.tryAcquire(timeoutMs, TimeUnit.MILLISECONDS)) {
        /* Increment timeout count */
        timeouterrcnt++;

        /* Throw timeout exception */
        throw new TimeoutException();
      }

      /* Read PID from inbound cell */
      if (icell.readU16() != pid) {
        /* Increment PID error count */
        piderrcnt++;

        /* Throw PID exception */
        throw new PidException();
      }

      /* Read parameter type from inbound cell */
      if (icell.readU8() != type) {
        /* Increment PID error count */
        typeerrcnt++;

        /* Throw unspecified exception */
        throw new TypeException();
      }

      /* Read inbound error message */
      berr = icell.readS8();

      /* If an error was returned, convert it to an exception. */
      ExceptionConvert.convertToException(berr);

    } catch (InterruptedException e) {
      /* Throw timeout exception */
      throw new TimeoutException();
    }

    /* Increment good message count */
    goodxactcnt++;
  }

  private void iGetXact(int pid, long eid, short type) throws HcException {
    /* Set expected reply parameters */
    expTransaction = transaction;
    expOpCode = HcCell.OPCODE_IGET_STS;

    /* Format outbound message */
    omsg.reset(transaction++);

    ocell.reset(HcCell.OPCODE_IGET_CMD);
    ocell.writeU16(pid);
    ocell.writeU32(eid);
    ocell.writeU8(type);
    omsg.write(ocell);

    /* Print outbound message if requested */
    if (debug) {
      omsg.print("Tx");
    }

    /* Send outbound message and check for error */
    try {
      omsg.send(lowdev);
    } catch (HcException hce) {
      /* Increment send error count */
      this.senderrcnt++;
      throw hce;
    }

    try {

      /* Wait for response */
      if (!replyEvent.tryAcquire(timeoutMs, TimeUnit.MILLISECONDS)) {
        /* Increment timeout count */
        timeouterrcnt++;

        /* Throw timeout exception */
        throw new TimeoutException();
      }

      /* Read PID from inbound cell */
      if (icell.readU16() != pid) {
        /* Increment PID error count */
        piderrcnt++;

        /* Throw unspecified exception */
        throw new PidException();
      }

      /* Read EID from inbound cell */
      if (icell.readU32() != eid) {
        /* Increment PID error count */
        eiderrcnt++;

        /* Throw EID exception */
        throw new EidException();
      }
      /* Read parameter type from inbound cell */
      if (icell.readU8() != type) {
        /* Increment PID error count */
        typeerrcnt++;

        /* Throw unspecified exception */
        throw new TypeException();
      }
    } catch (InterruptedException e) {
      /* Throw timeout exception */
      throw new TimeoutException();
    }

    /* Increment good message count */
    goodxactcnt++;

  }

  private void iSetXact(int pid, long eid, short type) throws HcException {
    short berr;

    /* Set expected reply parameters */
    expTransaction = transaction;
    expOpCode = HcCell.OPCODE_ISET_STS;

    /* Print outbound message if requested */
    if (debug) {
      omsg.print("Tx");
    }

    /* Send outbound message and check for error */
    try {
      omsg.send(lowdev);
    } catch (HcException hce) {
      /* Increment send error count */
      this.senderrcnt++;
      throw new UnspecifiedException();
    }

    try {

      /* Wait for response */
      if (!replyEvent.tryAcquire(timeoutMs, TimeUnit.MILLISECONDS)) {
        /* Increment timeout count */
        timeouterrcnt++;

        /* Throw timeout exception */
        throw new TimeoutException();
      }

      /* Check for inbound PID doesn't match outbound PID */
      if (icell.readU16() != pid) {
        /* Increment PID error count */
        piderrcnt++;

        /* Throw unspecified exception */
        throw new PidException();
      }

      /* Check for inbound EID doesn't match outbound EID */
      if (icell.readU32() != eid) {
        /* Increment PID error count */
        piderrcnt++;

        /* Throw EID exception */
        throw new EidException();
      }

      /* Read parameter type from inbound cell */
      if (icell.readU8() != type) {
        /* Increment PID error count */
        typeerrcnt++;

        /* Throw unspecified exception */
        throw new TypeException();
      }

      /* Read inbound error message */
      berr = icell.readS8();

      /* If an error was returned, convert it to an exception. */
      ExceptionConvert.convertToException(berr);

    } catch (InterruptedException e) {
      /* Throw timeout exception */
      throw new TimeoutException();
    }

    /* Increment good message count */
    goodxactcnt++;
  }

  private void readXact(int pid, long offset, int maxlen) throws HcException {
    /* Set expected reply parameters */
    expTransaction = transaction;
    expOpCode = HcCell.OPCODE_READ_STS;

    /* Format outbound message */
    omsg.reset(transaction++);
    ocell.reset(HcCell.OPCODE_READ_CMD);
    ocell.writeU16(pid);
    ocell.writeU32(offset);
    ocell.writeU16(maxlen);
    omsg.write(ocell);

    /* Print outbound message if requested */
    if (debug) {
      omsg.print("Tx");
    }

    /* Send outbound message and check for error */
    try {
      omsg.send(lowdev);
    } catch (HcException hce) {
      /* Increment send error count */
      this.senderrcnt++;
      throw new UnspecifiedException();
    }

    try {
      /* Wait for response */
      if (!replyEvent.tryAcquire(timeoutMs, TimeUnit.MILLISECONDS)) {
        /* Increment timeout count */
        timeouterrcnt++;

        /* Throw timeout exception */
        throw new TimeoutException();
      }

      /* Check for inbound PID doesn't match outbound PID */
      if (icell.readU16() != pid) {
        /* Increment PID error count */
        piderrcnt++;

        /* Throw unspecified exception */
        throw new PidException();
      }

      /* Check for inbound offset doesn't match outbound offset */
      if (icell.readU32() != offset) {
        /* Increment offset error count */
        offseterrcnt++;

        /* Throw unspecified exception */
        throw new UnspecifiedException();
      }
    } catch (InterruptedException e) {
      /* Throw timeout exception */
      throw new TimeoutException();
    }

    /* Increment good message count */
    goodxactcnt++;

  }

  private void writeXact(int pid, long offset) throws HcException {
    short berr;

    /* Set expected reply parameters */
    expTransaction = transaction;
    expOpCode = HcCell.OPCODE_ISET_STS;

    /* Print outbound message if requested */
    if (debug) {
      omsg.print("Tx");
    }

    /* Send outbound message and check for error */
    try {
      omsg.send(lowdev);
    } catch (HcException hce) {
      /* Increment send error count */
      this.senderrcnt++;
      throw new UnspecifiedException();
    }

    try {

      /* Wait for response */
      if (!replyEvent.tryAcquire(timeoutMs, TimeUnit.MILLISECONDS)) {
        /* Increment timeout count */
        timeouterrcnt++;

        /* Throw timeout exception */
        throw new TimeoutException();
      }

      /* Check for inbound PID doesn't match outbound PID */
      if (icell.readU16() != pid) {
        /* Increment PID error count */
        piderrcnt++;

        /* Throw unspecified exception */
        throw new PidException();
      }

      /* Check for inbound offset doesn't match outbound offset */
      if (icell.readU32() != offset) {
        /* Increment offset error count */
        offseterrcnt++;

        /* Throw unspecified exception */
        throw new UnspecifiedException();
      }

      /* Read inbound error message */
      berr = icell.readS8();

      /* If an error was returned, convert it to an exception. */
      ExceptionConvert.convertToException(berr);

    } catch (InterruptedException e) {
      /* Throw timeout exception */
      throw new TimeoutException();
    }

    /* Increment good message count */
    goodxactcnt++;
  }

  public void readThread() {

    /* Go forever */
    while (true) {
      /* Receive inbound message and check for error */
      try {
        imsg.recv(lowdev);
      } catch (HcException hce) {
        recverrcnt++;

        /* Sleep a while to prevent us from starving other threads */
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
        }

        /* Ignore the rest of this loop */
        continue;
      }

      if (debug) {
        imsg.print("Rx");
      }

      /* Check for invalid transaction number */
      if (imsg.getTransaction() != expTransaction) {
        /* Increment transaction error count */
        transactionerrcnt++;

        /* Ignore the rest of this loop */
        continue;
      }

      /* Read inbound cell from message and check for error */
      if (!imsg.read(icell)) {
        /* Increment cell error count */
        cellerrcnt++;

        /* Ignore the rest of this loop */
        continue;
      }

      /* Check for invalid opcode */
      if (icell.getOpcode() != expOpCode) {
        /* Increment opcode error count */
        opcodeerrcnt++;

        /* Ignore the rest of this loop */
        continue;
      }

      /* Signal valid reply */
      replyEvent.release();
    }
  }

}
