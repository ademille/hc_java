package com.ademille.hc.core;

import com.ademille.hc.device.Device;
import com.ademille.hc.exception.UnspecifiedException;

public class HcMessage {
  
  //Message overhead
  public int OVERHEAD = 1;

  //Maximum payload size (see cell payload size)
  public int PAYLOAD_MAX = 1400;

  private byte buffer[];
  private int readIndex;
  private int payloadLength;
  private int transaction;
  
  public HcMessage() {
    // Allocate memory for message buffer
    buffer = new byte[OVERHEAD + PAYLOAD_MAX];
  }
    
    public void reset(int transaction) {
      //Initialize read index, payload length and transaction number
      this.readIndex = 0;
      this.payloadLength = 0;
      this.transaction = transaction;
    }
    
    int getTransaction() {
      return this.transaction;
    }
    
    int getPayloadLength() {
      return this.payloadLength;
    }
    
    //TODO AJD: HcMessage shouldn't know about Device and sending. That should be
    //done in the client.  This should just be serialize
    public void send(Device dev) throws UnspecifiedException
    {
      int i;

      //Zero index
      i=0;

      //Serialize transaction number
      buffer[i++] = (byte)transaction;

      //Payload is already serialized in buffer so skip
      i += payloadLength;

      //Write serialized message to device and check for error
      if(dev.write(buffer, i) != i)
        throw new UnspecifiedException();
    }

    //TODO AJD: HcMessage shouldn't know about Device and receiving. That should be
    //done in the client. This should just be deserialize
    public void recv(Device dev) throws UnspecifiedException
    {
      int rcnt;
      int i;

      //Read serialized message from device and check for error
      if((rcnt = dev.read(buffer)) == 0)
      {
        //Sleep a while to prevent starving other threads
        try {
        Thread.sleep(1000);
        } catch (Exception e) {}
        throw new UnspecifiedException();
      }

      //Check for overflow
      if((rcnt < OVERHEAD) || (rcnt > (OVERHEAD + PAYLOAD_MAX)))
        throw new UnspecifiedException();

      //Zero index
      i=0;

      //Deserialize transaction number
      transaction = buffer[i++];

      //Reset read index
      readIndex = 0;

      //Set payload length
      payloadLength = rcnt - OVERHEAD;
    }

    public boolean read(HcCell cell)
    {
      int len;

      //Deserialize cell from payload and check for error
      if((len = cell.deserialize(buffer, OVERHEAD + readIndex, payloadLength - readIndex)) == 0)
        return false;

      //Update read index
      readIndex += len;

      return true;
    }

    public boolean write(HcCell cell)
    {
      int len;

      //Serialize cell into payload and check for error
      if((len = cell.serialize(buffer, OVERHEAD + payloadLength)) == 0)
        return false;

      //Update payload length
      payloadLength += len;

      return true;
    }

    public void print(String extra)
    {
      //Print common info
      System.out.print(extra  + "Msg: Xact="  + transaction);

      //Print payload
      System.out.print(", Payload=");
      for(int i=0; i< payloadLength; i++) {
        System.out.println(String.format("0x%02X", (int)buffer[OVERHEAD + i]));
      }
      System.out.println();
    }
}
