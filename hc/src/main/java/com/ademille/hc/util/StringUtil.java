package com.ademille.hc.util;

public class StringUtil {
  public static String strcpy(byte[] buf, int offset) {
    return strncpy(buf, offset, buf.length - offset);
  }

  public static String strncpy(byte[] buf, int offset, int size) {
    // Find null terminating character
    for (int i = offset; (i < buf.length); i++) {
      // If null character found or we hit our max size, return a new string.
      if ((buf[i] == '\0') || (i >= offset + size)) {
        return new String(buf, offset, i - offset);
      }
    }
  
    // No null character was found, so return an empty string.
    return "";
  }

}
