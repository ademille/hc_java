package com.ademille.hc.stubs;

import com.ademille.hc.core.HcClient;
import com.ademille.hc.exception.HcException;
import com.ademille.hc.param.HcCall;

public class HcCallStub extends HcCall {
  
  private HcClient client;
  private int paramId;
  
  public HcCallStub(String name, HcClient client, int paramId){
    super(name);
    this.client = client;
    this.paramId = paramId;
  }
  
  @Override
  public void call() throws HcException {
    client.call(paramId);
  }

  @Override
  public void callTable(int tid) throws HcException {
    client.callTable(paramId, tid);
  }
}
