package com.ademille.hc.stubs;

import com.ademille.hc.core.HcClient;
import com.ademille.hc.exception.HcException;
import com.ademille.hc.param.HcS64;

public class HcS64Stub extends HcS64 {
  
  private HcClient client;
  private int paramId;
  
  public HcS64Stub(String name, HcClient client, int paramId){
    super(name, Mode.READ_WRITE);
    this.client = client;
    this.paramId = paramId;
  }
  
  @Override
  public long getValue() throws HcException {
    return client.getS64(paramId);
  }

  @Override
  public long iGetValue(int tid) throws HcException {
    return client.iGetS64(paramId, tid);
  }
  
  @Override
  public void setValue(long val) throws HcException {
    client.setS64(paramId, val);
  }

  @Override
  public void setTableValue(int tid, long val) throws HcException {
    client.iSetS64(paramId, tid, val);
  }

}
