package com.ademille.hc.stubs;

import com.ademille.hc.core.HcClient;
import com.ademille.hc.exception.HcException;
import com.ademille.hc.param.HcString;

public class HcStringStub extends HcString{
  
  private HcClient client;
  private int paramId;
  
  public HcStringStub(String name, HcClient client, int paramId){
    super(name, Mode.READ_WRITE);
    this.client = client;
    this.paramId = paramId;
  }
  
  @Override
  public String getValue() throws HcException {
    return client.getString(paramId);
  }

  @Override
  public String iGetValue(int tid) throws HcException {
    return client.iGetString(paramId, tid);
  }
  
  @Override
  public void setValue(String val) throws HcException {
    client.setString(paramId, val);
  }
  
  @Override
  public void setTableValue(int tid, String val) throws HcException {
    client.iSetString(paramId, tid, val);
   }

}
