package com.ademille.hc.stubs;

import com.ademille.hc.core.HcClient;
import com.ademille.hc.exception.HcException;
import com.ademille.hc.param.HcU8;

public class HcU8Stub extends HcU8 {
  
  private HcClient client;
  private int paramId;
  
  public HcU8Stub(String name, HcClient client, int paramId){
    super(name, Mode.READ_WRITE);
    this.client = client;
    this.paramId = paramId;
  }
  
  @Override
  public short getValue() throws HcException {
			return client.getU8(paramId);
  }
  
  @Override
  public void setValue(short val) throws HcException {
    client.setU8(paramId, val);
  }
  
  @Override
  public short iGetValue(int tid) throws HcException {
  		return client.iGetU8(paramId,  tid);
  }
  
  @Override
  public void setTableValue(int tid, short val) throws HcException {
  	client.iSetU8(paramId, tid, val);
  }

}
