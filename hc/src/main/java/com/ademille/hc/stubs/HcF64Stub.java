package com.ademille.hc.stubs;

import com.ademille.hc.core.HcClient;
import com.ademille.hc.exception.HcException;
import com.ademille.hc.param.HcF64;

public class HcF64Stub extends HcF64 {
  
  private HcClient client;
  private int paramId;
  
  public HcF64Stub(String name, HcClient client, int paramId){
    super(name, Mode.READ_WRITE);
    this.client = client;
    this.paramId = paramId;
  }
  
  @Override
  public double getValue() throws HcException {
    return client.getF64(paramId);
  }
  
  @Override
  public double iGetValue(int tid) throws HcException {
    return client.iGetF64(paramId, tid);
  }
  
  @Override
  public void setValue(double val) throws HcException {
    client.setF64(paramId, val);
  }

  @Override
  public void setTableValue(int tid, double val) throws HcException {
    client.iSetF64(paramId, tid, val);
  }

}
