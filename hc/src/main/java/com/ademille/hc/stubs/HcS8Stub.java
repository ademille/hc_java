package com.ademille.hc.stubs;

import com.ademille.hc.core.HcClient;
import com.ademille.hc.exception.HcException;
import com.ademille.hc.param.HcS8;

public class HcS8Stub extends HcS8 {
  
  private HcClient client;
  private int paramId;
  
  public HcS8Stub(String name, HcClient client, int paramId){
    super(name, Mode.READ_WRITE);
    this.client = client;
    this.paramId = paramId;
  }
  
  @Override
  public byte getValue() throws HcException {
    return client.getS8(paramId);
  }

  @Override
  public byte iGetValue(int tid) throws HcException {
    return client.iGetS8(paramId, tid);
  }
  
  @Override
  public void setValue(byte val) throws HcException {
    client.setS8(paramId, val);
  }
  
  @Override
  public void setTableValue(int tid, byte val) throws HcException {
    client.iSetS8(paramId, tid, val);
  }

}
