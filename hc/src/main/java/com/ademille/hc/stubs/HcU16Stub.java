package com.ademille.hc.stubs;

import com.ademille.hc.core.HcClient;
import com.ademille.hc.exception.HcException;
import com.ademille.hc.param.HcU16;

public class HcU16Stub extends HcU16 {
  
  private HcClient client;
  private int paramId;
  
  public HcU16Stub(String name, HcClient client, int paramId){
    super(name, Mode.READ_WRITE);
    this.client = client;
    this.paramId = paramId;
  }
  
  @Override
  public int getValue() throws HcException {
    return client.getU16(paramId);
  }

  @Override
  public int iGetValue(int tid) throws HcException {
    return client.iGetU16(paramId, tid);
  }
  
  @Override
  public void setValue(int val) throws HcException {
    client.setU16(paramId, val);
  }

  @Override
  public void setTableValue(int tid, int val) throws HcException {
    client.iSetU16(paramId, tid, val);
  }

}
