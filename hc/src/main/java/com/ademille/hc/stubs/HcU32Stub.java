package com.ademille.hc.stubs;

import com.ademille.hc.core.HcClient;
import com.ademille.hc.exception.HcException;
import com.ademille.hc.param.HcU32;

public class HcU32Stub extends HcU32 {
  
  private HcClient client;
  private int paramId;
  
  public HcU32Stub(String name, HcClient client, int paramId){
    super(name, Mode.READ_WRITE);
    this.client = client;
    this.paramId = paramId;
  }
  
  @Override
  public long getValue() throws HcException {
    return client.getU32(paramId);
  }

  @Override
  public long iGetValue(int tid) throws HcException {
    return client.iGetU32(paramId, tid);
  }
  
  @Override
  public void setValue(long val) throws HcException {
    client.setU32(paramId, val);
  }

  @Override
  public void setTableValue(int tid, long val) throws HcException {
    client.iSetU32(paramId, tid, val);
  }

}
