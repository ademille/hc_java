package com.ademille.hc.stubs;

import com.ademille.hc.core.HcClient;
import com.ademille.hc.exception.HcException;
import com.ademille.hc.param.HcBool;

public class HcBoolStub extends HcBool {
  
  private HcClient client;
  private int paramId;
  
  public HcBoolStub(String name, HcClient client, int paramId){
    super(name, Mode.READ_WRITE);
    this.client = client;
    this.paramId = paramId;
  }
  
  @Override
  public boolean getValue() throws HcException {
    return client.getBool(paramId);
  }

  @Override
  public boolean iGetValue(int eid) throws HcException {
    return client.iGetBool(paramId, eid);
  }
  
  @Override
  public void setValue(boolean val) throws HcException {
    client.setBool(paramId, val);
  }

  @Override
  public void setTableValue(int tid, boolean val) throws HcException {
    client.iSetBool(paramId, tid, val);
  }

}
