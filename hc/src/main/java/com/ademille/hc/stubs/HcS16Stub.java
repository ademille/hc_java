package com.ademille.hc.stubs;

import com.ademille.hc.core.HcClient;
import com.ademille.hc.exception.HcException;
import com.ademille.hc.param.HcS16;

public class HcS16Stub extends HcS16 {
  
  private HcClient client;
  private int paramId;
  
  public HcS16Stub(String name, HcClient client, int paramId){
    super(name, Mode.READ_WRITE);
    this.client = client;
    this.paramId = paramId;
  }
  
  @Override
  public short getValue() throws HcException {
    return client.getS16(paramId);
  }

  @Override
  public short iGetValue(int tid) throws HcException {
    return client.iGetS16(paramId, tid);
  }
  
  @Override
  public void setValue(short val) throws HcException {
    client.setS16(paramId, val);
  }

  @Override
  public void setTableValue(int tid, short val) throws HcException {
    client.iSetS16(paramId, tid, val);
  }

}
