package com.ademille.hc.stubs;

import com.ademille.hc.core.HcClient;
import com.ademille.hc.exception.HcException;
import com.ademille.hc.param.HcF32;

public class HcF32Stub extends HcF32 {
  
  private HcClient client;
  private int paramId;
  
  public HcF32Stub(String name, HcClient client, int paramId){
    super(name, Mode.READ_WRITE);
    this.client = client;
    this.paramId = paramId;
  }
  
  @Override
  public float getValue() throws HcException {
    return client.getF32(paramId);
  }

  @Override
  public float iGetValue(int tid) throws HcException {
    return client.iGetF32(paramId, tid);
  }
  
  @Override
  public void setValue(float val) throws HcException {
    client.setF32(paramId, val);
  }

  @Override
  public void setTableValue(int tid, float val) throws HcException {
    client.iSetF32(paramId, tid, val);
  }

}
