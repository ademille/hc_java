package com.ademille.hc.exception;

public class NotFoundException extends HcException {

  public final static byte ERR_ID = -18;
  private static String ERR_TYPE = "NOT_FOUND";
  private static final long serialVersionUID = 1L;
  
  public NotFoundException(){
    this("");
  }
  
  public NotFoundException(String msg){
    super (ERR_ID, ERR_TYPE, msg);
  }
  
  
  
}
