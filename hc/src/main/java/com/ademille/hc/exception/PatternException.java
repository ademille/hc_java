package com.ademille.hc.exception;

public class PatternException extends HcException {

  public final static byte ERR_ID = -8;
  private static String ERR_TYPE = "PATTERN";
  private static final long serialVersionUID = 1L;
  
  public PatternException(){
    this("");
  }
  
  public PatternException(String msg){
    super (ERR_ID, ERR_TYPE, msg);
  }
}
