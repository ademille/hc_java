package com.ademille.hc.exception;

public class TypeException extends HcException {

  public final static byte ERR_ID = -7;
  private static String ERR_TYPE = "TYPE";
  private static final long serialVersionUID = 1L;
  
  public TypeException(){
    this("");
  }
  
  public TypeException(String msg){
    super (ERR_ID, ERR_TYPE, msg);
  }
  
  
}
