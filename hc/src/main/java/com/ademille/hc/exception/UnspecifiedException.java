package com.ademille.hc.exception;

public class UnspecifiedException extends HcException {

  public final static byte ERR_ID = -1;
  private static String ERR_TYPE = "UNSPEC";
  private static final long serialVersionUID = 1L;
  
  public UnspecifiedException(){
    this("");
  }
  
  public UnspecifiedException(String msg){
    super (ERR_ID, ERR_TYPE, msg);
  }
  
  
}
