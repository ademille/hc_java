package com.ademille.hc.exception;

public class DestroyedException extends HcException {

  public final static byte ERR_ID = -5;
  private static String ERR_TYPE = "DESROYED";
  private static final long serialVersionUID = 1L;
  
  public DestroyedException(){
    this("");
  }
  
  public DestroyedException(String msg){
    super (ERR_ID, ERR_TYPE, msg);
  }
  
  
}
