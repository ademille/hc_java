package com.ademille.hc.exception;

public class AlignmentException extends HcException {

  public final static byte ERR_ID = -13;
  private static String ERR_TYPE = "ALIGNMENT";
  private static final long serialVersionUID = 1L;
  
  public AlignmentException(){
    this("");
  }
  
  public AlignmentException(String msg){
    super (ERR_ID, ERR_TYPE, msg);
  }
  
  
}
