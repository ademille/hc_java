package com.ademille.hc.exception;

public class ResetException extends HcException {

  public final static byte ERR_ID = -4;
  private static String ERR_TYPE = "RESET";
  private static final long serialVersionUID = 1L;
  
  public ResetException(){
    this("");
  }
  
  public ResetException(String msg){
    super (ERR_ID, ERR_TYPE, msg);
  }
  
  
  
}
