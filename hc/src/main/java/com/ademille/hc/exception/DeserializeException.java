package com.ademille.hc.exception;

public class DeserializeException extends HcException {

  public final static byte ERR_ID = -14;
  private static String ERR_TYPE = "DESER";
  private static final long serialVersionUID = 1L;
  
  public DeserializeException(){
    this("");
  }
  
  public DeserializeException(String msg){
    super (ERR_ID, ERR_TYPE, msg);
  }
  
  
}
