package com.ademille.hc.exception;

public class TimeoutException extends HcException {

  public final static byte ERR_ID = -2;
  private static String ERR_TYPE = "TIMEOUT";
  private static final long serialVersionUID = 1L;
  
  public TimeoutException(){
    this("");
  }
  
  public TimeoutException(String msg){
    super (ERR_ID, ERR_TYPE, msg);
  }
  
  
  
}
