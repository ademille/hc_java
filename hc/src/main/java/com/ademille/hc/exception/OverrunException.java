package com.ademille.hc.exception;

public class OverrunException extends HcException {

  public final static byte ERR_ID = -16;
  private static String ERR_TYPE = "OVERRUN";
  private static final long serialVersionUID = 1L;
  
  public OverrunException(){
    this("");
  }
  
  public OverrunException(String msg){
    super (ERR_ID, ERR_TYPE, msg);
  }
  
  
}
