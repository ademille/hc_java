package com.ademille.hc.exception;

public class ExceptionConvert {
  public static void convertToException(int err) throws HcException{
    switch (err){
    case 0: //No error
      return;
    case UnspecifiedException.ERR_ID:
      throw new UnspecifiedException();
    case TimeoutException.ERR_ID:
      throw new TimeoutException();
    case OwnerException.ERR_ID:
      throw new OwnerException();
    case ResetException.ERR_ID:
      throw new ResetException();
    case DestroyedException.ERR_ID:
      throw new DestroyedException();
    case OverflowException.ERR_ID:
      throw new OverflowException();
    case TypeException.ERR_ID:
      throw new TypeException();
    case PatternException.ERR_ID:
      throw new PatternException();
    case AccessException.ERR_ID:
      throw new AccessException();
    case RangeException.ERR_ID:
      throw new RangeException();
    case StepException.ERR_ID:
      throw new StepException();
    case InvalidException.ERR_ID:
      throw new InvalidException();
    case AlignmentException.ERR_ID:
      throw new AlignmentException();
    case DeserializeException.ERR_ID:
      throw new DeserializeException();
    case OpCodeException.ERR_ID:
      throw new OpCodeException();
    case PidException.ERR_ID:
      throw new PidException();
    case EidException.ERR_ID:
      throw new EidException();
    case NotFoundException.ERR_ID:
      throw new NotFoundException();
    default:
      throw new UnknownException();
    }
  }
}
