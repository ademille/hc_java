package com.ademille.hc.exception;

public class RangeException extends HcException {

  public final static byte ERR_ID = -10;
  private static String ERR_TYPE = "RANGE";
  private static final long serialVersionUID = 1L;
  
  public RangeException(){
    this("");
  }
  
  public RangeException(String msg){
    super (ERR_ID, ERR_TYPE, msg);
  }
  
  
}
