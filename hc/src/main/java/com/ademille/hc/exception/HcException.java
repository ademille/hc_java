package com.ademille.hc.exception;

public abstract class HcException extends Exception {
  
  private static final long serialVersionUID = 1L;
  
  private byte errId;
  private String type;
  
  public HcException(byte errId, String type, String msg){
    super (msg);
    this.errId = errId;
    this.type = type;
  }

  public byte getErrId(){
    return errId;
  }
  
  public String getType(){
    return type;
  }
  
  
}
