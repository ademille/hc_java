package com.ademille.hc.exception;

public class SerializeException extends HcException {

  public final static byte ERR_ID = -1;
  private static String ERR_TYPE = "SERIALIZE";
  private static final long serialVersionUID = 1L;
  
  public SerializeException(){
    this("");
  }
  
  public SerializeException(String msg){
    super (ERR_ID, ERR_TYPE, msg);
  }
  
  
  
}
