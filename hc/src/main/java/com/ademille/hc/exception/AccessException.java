package com.ademille.hc.exception;

public class AccessException extends HcException {

  public static final byte ERR_ID = -9;
  private static String ERR_TYPE = "ACCESS";
  private static final long serialVersionUID = 1L;
  
  public AccessException(){
    this("");
  }
  
  public AccessException(String msg){
    super (ERR_ID, ERR_TYPE, msg);
  }
  
}
