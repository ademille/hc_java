package com.ademille.hc.exception;

public class UnknownException extends HcException {

  public final static byte ERR_ID = -19;
  private static String ERR_TYPE = "UNKNOWN";
  private static final long serialVersionUID = 1L;
  
  public UnknownException(){
    this("");
  }
  
  public UnknownException(String msg){
    super (ERR_ID, ERR_TYPE, msg);
  }
  
  
}
