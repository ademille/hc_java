package com.ademille.hc.exception;

public class StepException extends HcException {

  public final static byte ERR_ID = -11;
  private static String ERR_TYPE = "STEP";
  private static final long serialVersionUID = 1L;
  
  public StepException(){
    this("");
  }
  
  public StepException(String msg){
    super (ERR_ID, ERR_TYPE, msg);
  }
  
}
