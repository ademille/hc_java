package com.ademille.hc.exception;

public class OpCodeException extends HcException {

  public final static byte ERR_ID = -15;
  private static String ERR_TYPE = "OPCODE";
  private static final long serialVersionUID = 1L;
  
  public OpCodeException(){
    this("");
  }
  
  public OpCodeException(String msg){
    super (ERR_ID, ERR_TYPE, msg);
  }
  
  
  
}
