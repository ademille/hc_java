package com.ademille.hc.exception;

public class OwnerException extends HcException {

  public final static byte ERR_ID = -3;
  private static String ERR_TYPE = "OWNER";
  private static final long serialVersionUID = 1L;
  
  public OwnerException(){
    this("");
  }
  
  public OwnerException(String msg){
    super (ERR_ID, ERR_TYPE, msg);
  }
  
  
  
}
