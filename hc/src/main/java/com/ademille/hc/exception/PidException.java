package com.ademille.hc.exception;

public class PidException extends HcException {

  public final static byte ERR_ID = -16;
  private static String ERR_TYPE = "PID";
  private static final long serialVersionUID = 1L;
  
  public PidException(){
    this("");
  }
  
  public PidException(String msg){
    super (ERR_ID, ERR_TYPE, msg);
  }
  
  
  
}
