package com.ademille.hc.exception;

public class OverflowException extends HcException {

  public final static byte ERR_ID = -6;
  private static String ERR_TYPE = "OVERFLOW";
  private static final long serialVersionUID = 1L;
  
  public OverflowException(){
    this("");
  }
  
  public OverflowException(String msg){
    super (ERR_ID, ERR_TYPE, msg);
  }
  
}
