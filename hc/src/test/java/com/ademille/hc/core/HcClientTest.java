package com.ademille.hc.core;

import static org.junit.Assert.*;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.ademille.hc.core.HcClient;
import com.ademille.hc.device.Device;
import com.ademille.hc.device.UdpDevice;
import com.ademille.hc.exception.EidException;
import com.ademille.hc.exception.HcException;
import com.ademille.hc.exception.PidException;
import com.ademille.hc.exception.TimeoutException;
import com.ademille.hc.exception.TypeException;


class SimDevice implements Device {
  private BlockingQueue<byte[]> responseQ = new ArrayBlockingQueue<>(4);
  private BlockingQueue<byte[]> readQ = new ArrayBlockingQueue<>(4);

  public void addResponse(byte[] buf, int len) {
	// Create an array that just has len elements
	try {
	  responseQ.put(Arrays.copyOf(buf, len));
	} catch (InterruptedException e) {
	  System.out.println("SimDevice response Q put interrupted.");
	  e.printStackTrace();
	}
  }


  @Override
  public int read(byte[] buf) {
	// Grab a packet from the queue
	int bytesToCopy = 0;
	try {
	  byte[] bytes = readQ.take();
	  bytesToCopy = (buf.length < bytes.length) ? buf.length : bytes.length;
	  System.arraycopy(bytes, 0, buf, 0, bytesToCopy);
	} catch (InterruptedException e) {
	  System.out.println("SimDevice Queue read interrupted.");
	  e.printStackTrace();
	}
	return bytesToCopy;
  }

  @Override
  public int write(byte[] buf, int len) {
	// Add the response packet when a message is written
	byte[] msg = responseQ.poll();
	if (msg != null) {
	  try {
		readQ.put(msg);
	  } catch (InterruptedException e) {
        System.out.println("SimDevice read Q put interrupted.");
		e.printStackTrace();
	  }
	}
	return len;
  }
}

@RunWith(JUnit4.class)
public class HcClientTest {
  private HcClient hcClient;
  private HcClient hcClientSimDev;
  private SimDevice simDev;
  final int PID_NAME = 0;
  final int PID_VERSION = 1;
  final int PID_INFOFILECRC = 2;
  final int PID_INFOFILE = 3;

  public HcClientTest() {
    UdpDevice udpDev = new UdpDevice();
    try {
      udpDev.setDestination(InetAddress.getLocalHost(), 1500);
    } catch (UnknownHostException e) {
      e.printStackTrace();
    }

    hcClient = new HcClient(udpDev, 100);
  }



  @Before
  public void setUp() throws Exception {
    simDev = new SimDevice();
    hcClientSimDev = new HcClient(simDev, 10);
  }


  @Test
  public void testDebug() {
    hcClient.setDebug(true);
    assertTrue(hcClient.isDebug());

    hcClient.setDebug(false);;
    assertFalse(hcClient.isDebug());
  }

  @Test
  public void testGetSendErrCnt() {
    //I don't really have a good way to force this to fail.
    assertEquals(0, hcClient.getSendErrCnt());
  }

  @Test
  public void testGetRecvErrCnt() {
    //TODO: Create fake server to force failure.
    assertEquals(0, hcClient.getRecvErrCnt());
  }

  @Test
  public void testGetTransactionErrCnt() throws HcException{
    //RxMsg: Xact=23, Payload=02 00 03 00 07 20 
	//TxMsg: Xact=23, Payload=03 00 05 00 07 20 80 00 
	boolean exception = false;
	byte[] response = 
	  {
		  0x1, //Transaction <-- Should be 0
		  0x03, //Opcode (get response)
		  0x00, 0x05, //Payload Length
		  0x00, 0x07,  // PID 
		  0x20, // Type (uint 8)
		  0x77, // Return value
		  0x00  // Error code
		  }; 
	simDev.addResponse(response, response.length);

	//There shouldn't be any error counts to start.
	assertEquals(0, hcClientSimDev.getTransactionErrCnt());

	try {
      hcClientSimDev.getU8(7);
	} catch (TimeoutException e) {
	  //This should happen 
	  exception = true;
	}
	assertTrue(exception);
	assertEquals(1, hcClientSimDev.getTransactionErrCnt());
  }

  @Test
  public void testGetCellErrCnt() throws HcException {
	//RxMsg: Xact=23, Payload=02 00 03 00 07 20 
	//TxMsg: Xact=23, Payload=03 00 05 00 07 20 80 00 
	boolean exception = false;
	byte[] response = 
	  {
		  0x0, //Transaction
		  0x03, //Opcode (get response)
		  0x00, 0x55, //Payload Length <-- Corrupt payload length. Should be 0x05
		  0x00, 0x07,  // PID
		  0x20, // Type (uint 8)
		  0x77, // Return value
		  0x00  // Error code
		  }; 
	simDev.addResponse(response, response.length);

	//There shouldn't be any error counts to start.
	assertEquals(0, hcClientSimDev.getCellErrCnt());

	try {
      hcClientSimDev.getU8(7);
	} catch (TimeoutException e) {
	  //This should happen 
	  exception = true;
	}
	assertTrue(exception);
	assertEquals(1, hcClientSimDev.getCellErrCnt());
  }

  @Test
  public void testGetOpCodeErrCnt() throws HcException{
    //RxMsg: Xact=23, Payload=02 00 03 00 07 20 
	//TxMsg: Xact=23, Payload=03 00 05 00 07 20 80 00 
	boolean exception = false;
	byte[] response = 
	  {
		  0x0, //Transaction
		  0x0A, //Opcode (get response) <-- Should be 0x03
		  0x00, 0x05, //Payload Length
		  0x00, 0x07,  // PID 
		  0x20, // Type (uint 8)
		  0x77, // Return value
		  0x00  // Error code
		  }; 
	simDev.addResponse(response, response.length);

	//There shouldn't be any error counts to start.
	assertEquals(0, hcClientSimDev.getOpCodeErrCnt());

	try {
      hcClientSimDev.getU8(7);
	} catch (TimeoutException e) {
	  //This should happen 
	  exception = true;
	}
	assertTrue(exception);
	assertEquals(1, hcClientSimDev.getOpCodeErrCnt());
  }

  public void testGetTimeoutErrCnt() throws HcException{
    // Don't put a response packet into the SimDev, forcing a timeout.
    // Catch the exception to verify the timeout error count was incremented.
    try {
      hcClientSimDev.getBool(0xFFF);
    } catch (HcException e) {
      // Ignore
    }
    assertEquals(1, hcClient.getTimeoutErrCnt());
  }

  @Test(expected = TimeoutException.class)
  public void testTimeoutException() throws HcException{
    // Don't put a response packet into the SimDev, forcing a timeout.
    hcClientSimDev.getBool(0xFFF);
  }

  @Test
  public void testGetPIDErrCnt() throws HcException{
	//RxMsg: Xact=23, Payload=02 00 03 00 07 20 
	//TxMsg: Xact=23, Payload=03 00 05 00 07 20 80 00 
	boolean exception = false;
	byte[] response = 
	  {
		  0x0, //Transaction
		  0x03, //Opcode (get response)
		  0x00, 0x05, //Payload Length
		  0x00, 0x7F,  // PID <-- Should be 0x07
		  0x20, // Type (uint 8)
		  0x77, // Return value
		  0x00  // Error code
		  }; 
	simDev.addResponse(response, response.length);

	//There shouldn't be any error counts to start.
	assertEquals(0, hcClientSimDev.getPIDErrCnt());

	try {
      hcClientSimDev.getU8(7);
	} catch (PidException e) {
	  //This should happen 
	  exception = true;
	}
	assertTrue(exception);
	assertEquals(1, hcClientSimDev.getPIDErrCnt());
  }

  @Test(expected = PidException.class)
  public void testPIDException() throws HcException {
    //Request invalid PID
    hcClient.getBool(0xFFF);
  }


  @Test
  public void testGetTypeErrCnt() throws HcException {
	//RxMsg: Xact=23, Payload=02 00 03 00 07 20 
	//TxMsg: Xact=23, Payload=03 00 05 00 07 20 80 00 
	boolean exception = false;
	byte[] response = 
	  {
		  0x0, //Transaction
		  0x03, //Opcode (get response)
		  0x00, 0x05, //Payload Length
		  0x00, 0x07,  // PID
		  0x21, // Type (uint 8) <-- Should be 0x20
		  0x77, // Return value
		  0x00 // Error code
		  }; 
	simDev.addResponse(response, response.length);

	//There shouldn't be any error counts to start.
	assertEquals(0, hcClientSimDev.getTypeErrCnt());

	try {
      hcClientSimDev.getU8(7);
	} catch (TypeException e) {
	  //This should happen 
	  exception = true;
	}
	assertTrue(exception);
	assertEquals(1, hcClientSimDev.getTypeErrCnt());
  }

  @Test
  public void testGetEIDErrCnt() throws HcException{
	// The following is a uint8 IGet call for index 2 of a table
	//RxMsg: Xact=56, Payload=08 00 07 00 0b 00 00 00 02 20 
	//TxMsg: Xact=56, Payload=09 00 09 00 0b 00 00 00 02 20 00 00
	boolean exception = false;
	byte[] response = 
	  {
		  0x00, //Transaction
		  0x09, //Opcode (iget response)
		  0x00, 0x09, //Payload Length
		  0x00, 0x07,  // PID
		  0x00, 0x00, 0x00, 0x03, // EID <-- Should be 0x02
		  0x20, // Type (uint 8)
		  0x77, // Return value
		  0x00 // Error code
		  }; 
	simDev.addResponse(response, response.length);

	//There shouldn't be any error counts to start.
	assertEquals(0, hcClientSimDev.getEIDErrCnt());

	try {
      hcClientSimDev.iGetU8(7, 2);
	} catch (EidException e) {
	  //This should happen 
	  exception = true;
	}
	assertTrue(exception);
	assertEquals(1, hcClientSimDev.getEIDErrCnt());
  }

  @Test
  public void testGetOffsetErrCnt() {
    //fail("Not yet implemented");
  }

  @Test
  public void testGetGoodXactCnt() throws HcException{
     final int PID = 7;
    final short VAL_MAX = 255;

    // Should start out with 0 good transactions
    assertEquals(0, hcClient.getGoodXactCnt());

    // A valid set should increment good count.
    hcClient.setU8(PID, VAL_MAX);
    assertEquals(1, hcClient.getGoodXactCnt());

    // A valid get should increment good count.
    hcClient.getU8(PID);
    assertEquals(2, hcClient.getGoodXactCnt());
  }

  @Test
  public void testCall() throws HcException{
    final int PID = 4;
    //Just use the u8 call
    // If it doesn't throw an exception, it took.
    hcClient.call(PID);
  }

  @Test
  public void testICall() throws HcException{
    //Just use the u8 call
    // If it doesn't throw an exception, it took.
    final int PID = 6;
    final long EID = 0;
    hcClient.iCall(PID, EID);
  }

  @Test
  public void testGetName() throws HcException{
    String name = hcClient.getString(PID_NAME);
    assertEquals("Scratch", name);
  }

  @Test
  public void testGetVersion() throws HcException {
    String version = hcClient.getString(PID_VERSION);
    assertTrue(version.length() > 5);
  }

  @Test
  public void testGetCRC() throws HcException{
    long infoFileCrc = hcClient.getU32(PID_INFOFILECRC);
    assertTrue(infoFileCrc > 0);
  }

  @Test
  public void testS8() throws HcException{
    final int PID = 71;
    final byte VAL_MAX = -128;
    final byte VAL_MIN = 127;
    final byte VAL_MID = 1;
    hcClient.setS8(PID, VAL_MAX);
    assertEquals(VAL_MAX, hcClient.getS8(PID));

    hcClient.setS8(PID, VAL_MIN);
    assertEquals(VAL_MIN, hcClient.getS8(PID));

    hcClient.setS8(PID, VAL_MID);
    assertEquals(VAL_MID, hcClient.getS8(PID));
  }

  @Test
  public void testiS8() throws HcException {
    final int PID = 75;
    final int EID = 0;
    final byte VAL_MAX = -128;
    final byte VAL_MIN = 127;
    final byte VAL_MID = 1;
    hcClient.iSetS8(PID, EID, VAL_MAX);
    assertEquals(VAL_MAX, hcClient.iGetS8(PID, EID));

    hcClient.iSetS8(PID, EID, VAL_MIN);
    assertEquals(VAL_MIN, hcClient.iGetS8(PID, EID));

    hcClient.iSetS8(PID, EID, VAL_MID);
    assertEquals(VAL_MID, hcClient.iGetS8(PID, EID));
  }

  @Test
  public void testS16() throws HcException {
    final int PID = 87;
    final short VAL_MAX = Short.MAX_VALUE;
    final short VAL_MIN = Short.MIN_VALUE;
    final short VAL_MID = 1;
    hcClient.setS16(PID, VAL_MAX);
    assertEquals(VAL_MAX, hcClient.getS16(PID));

    hcClient.setS16(PID, VAL_MIN);
    assertEquals(VAL_MIN, hcClient.getS16(PID));

    hcClient.setS16(PID, VAL_MID);
    assertEquals(VAL_MID, hcClient.getS16(PID));
  }

  @Test
  public void testiS16() throws HcException {
    final int PID = 91;
    final long EID = 0;
    final short VAL_MAX = Short.MAX_VALUE;
    final short VAL_MIN = Short.MIN_VALUE;
    final short VAL_MID = 1;
    hcClient.iSetS16(PID, EID, VAL_MAX);
    assertEquals(VAL_MAX, hcClient.iGetS16(PID, EID));

    hcClient.iSetS16(PID, EID, VAL_MIN);
    assertEquals(VAL_MIN, hcClient.iGetS16(PID, EID));

    hcClient.iSetS16(PID, EID, VAL_MID);
    assertEquals(VAL_MID, hcClient.iGetS16(PID, EID));
  }

  @Test
  public void testS32() throws HcException{
    final int PID = 103;
    final int VAL_MAX = Integer.MAX_VALUE;
    final int VAL_MIN = Integer.MIN_VALUE;
    final int VAL_MID = 1;
    hcClient.setS32(PID, VAL_MAX);
    assertEquals(VAL_MAX, hcClient.getS32(PID));

    hcClient.setS32(PID, VAL_MIN);
    assertEquals(VAL_MIN, hcClient.getS32(PID));

    hcClient.setS32(PID, VAL_MID);
    assertEquals(VAL_MID, hcClient.getS32(PID));
  }

  @Test
  public void testiS32() throws Exception {
    final int PID = 107;
    final long EID = 0;
    final int VAL_MAX = Integer.MAX_VALUE;
    final int VAL_MIN = Integer.MIN_VALUE;
    final int VAL_MID = 1;
    hcClient.iSetS32(PID, EID, VAL_MAX);
    assertEquals(VAL_MAX, hcClient.iGetS32(PID, EID));

    hcClient.iSetS32(PID, EID, VAL_MIN);
    assertEquals(VAL_MIN, hcClient.iGetS32(PID, EID));

    hcClient.iSetS32(PID, EID, VAL_MID);
    assertEquals(VAL_MID, hcClient.iGetS32(PID, EID));
  }

  @Test
  public void testS64() throws Exception {
    final int PID = 119;
    final long VAL_MAX = Long.MAX_VALUE;
    final long VAL_MIN = Long.MIN_VALUE;
    final long VAL_MID = 1;
    hcClient.setS64(PID, VAL_MAX);
    assertEquals(VAL_MAX, hcClient.getS64(PID));

    hcClient.setS64(PID, VAL_MIN);
    assertEquals(VAL_MIN, hcClient.getS64(PID));

    hcClient.setS64(PID, VAL_MID);
    assertEquals(VAL_MID, hcClient.getS64(PID));
  }

  @Test
  public void testiS64() throws Exception {
    final int PID = 123;
    final long EID = 0;
    final long VAL_MAX = Long.MAX_VALUE;
    final long VAL_MIN = Long.MIN_VALUE;
    final long VAL_MID = 1;
    hcClient.iSetS64(PID, EID, VAL_MAX);
    assertEquals(VAL_MAX, hcClient.iGetS64(PID, EID));

    hcClient.iSetS64(PID, EID, VAL_MIN);
    assertEquals(VAL_MIN, hcClient.iGetS64(PID, EID));

    hcClient.iSetS64(PID, EID, VAL_MID);
    assertEquals(VAL_MID, hcClient.iGetS64(PID, EID));
  }

  @Test
  public void testU8() throws HcException {
    final int PID = 7;
    final short VAL_MAX = 255;
    final short VAL_MIN = 0;
    final short VAL_MID = 128;
    hcClient.setU8(PID, VAL_MAX);
    assertEquals(VAL_MAX, hcClient.getU8(PID));

    hcClient.setU8(PID, VAL_MIN);
    assertEquals(VAL_MIN, hcClient.getU8(PID));

    hcClient.setU8(PID, VAL_MID);
    assertEquals(VAL_MID, hcClient.getU8(PID));
  }

  @Test
  public void testiU8() throws Exception {
    final int PID = 11;
    final int EID = 0;
    final short VAL_MAX = 0xFF;
    final short VAL_MIN = 0;
    final short VAL_MID = 0xEF;
    hcClient.iSetU8(PID, EID, VAL_MAX);
    assertEquals(VAL_MAX, hcClient.iGetU8(PID, EID));

    hcClient.iSetU8(PID, EID, VAL_MIN);
    assertEquals(VAL_MIN, hcClient.iGetU8(PID, EID));

    hcClient.iSetU8(PID, EID, VAL_MID);
    assertEquals(VAL_MID, hcClient.iGetU8(PID, EID));
  }

  @Test
  public void testU16() throws HcException{
    final int PID = 23;
    final int VAL_MAX = 0xFFFF;
    final int VAL_MIN = 0;
    final int VAL_MID = 0x7FFF;
    hcClient.setU16(PID, VAL_MAX);
    assertEquals(VAL_MAX, hcClient.getU16(PID));

    hcClient.setU16(PID, VAL_MIN);
    assertEquals(VAL_MIN, hcClient.getU16(PID));

    hcClient.setU16(PID, VAL_MID);
    assertEquals(VAL_MID, hcClient.getU16(PID));
  }

  @Test
  public void testiU16() throws Exception {
    final int PID = 27;
    final long EID = 0;
    final int VAL_MAX = 0xFFFF;
    final int VAL_MIN = 0;
    final int VAL_MID = 0xAABB;
    hcClient.iSetU16(PID, EID, VAL_MAX);
    assertEquals(VAL_MAX, hcClient.iGetU16(PID, EID));

    hcClient.iSetU16(PID, EID, VAL_MIN);
    assertEquals(VAL_MIN, hcClient.iGetU16(PID, EID));

    hcClient.iSetU16(PID, EID, VAL_MID);
    assertEquals(VAL_MID, hcClient.iGetU16(PID, EID));
  }

  @Test
  public void testU32() throws HcException{
    final int PID = 39;
    final long VAL_MAX = 0xFFFFFFFFL;
    final long VAL_MIN = 0;
    final long VAL_MID = 0x7FFFFFFFL;
    hcClient.setU32(PID, VAL_MAX);
    assertEquals(VAL_MAX, hcClient.getU32(PID));

    hcClient.setU32(PID, VAL_MIN);
    assertEquals(VAL_MIN, hcClient.getU32(PID));

    hcClient.setU32(PID, VAL_MID);
    assertEquals(VAL_MID, hcClient.getU32(PID));
  }

  @Test
  public void testiU32() throws Exception{
    final int PID = 43;
    final int EID = 0;
    final long VAL_MAX = 0xFFFFFFFFL;
    final long VAL_MIN = 0;
    final long VAL_MID = 0xAABBCCDDL;
    hcClient.iSetU32(PID, EID, VAL_MAX);
    assertEquals(VAL_MAX, hcClient.iGetU32(PID, EID));

    hcClient.iSetU32(PID, EID, VAL_MIN);
    assertEquals(VAL_MIN, hcClient.iGetU32(PID, EID));

    hcClient.iSetU32(PID, EID, VAL_MID);
    assertEquals(VAL_MID, hcClient.iGetU32(PID, EID));
  }

  @Test
  public void testF32() throws Exception {
    final int PID = 147;
    final float VAL_MAX = Float.MAX_VALUE;
    final float VAL_MIN = Float.MIN_VALUE;
    final float VAL_MID = 0.0001F;
    hcClient.setF32(PID, VAL_MAX);
    assertEquals(VAL_MAX, hcClient.getF32(PID), 0.00001);

    hcClient.setF32(PID, VAL_MIN);
    assertEquals(VAL_MIN, hcClient.getF32(PID), 0.00001);

    hcClient.setF32(PID, VAL_MID);
    assertEquals(VAL_MID, hcClient.getF32(PID), 0.00001);
  }

  @Test
  public void testiF32() throws Exception {
    final int PID = 150;
    final long EID = 0;
    final float VAL_MAX = Float.MAX_VALUE;
    final float VAL_MIN = Float.MIN_VALUE;
    final float VAL_MID = 0.0001F;
    hcClient.iSetF32(PID, EID, VAL_MAX);
    assertEquals(VAL_MAX, hcClient.iGetF32(PID, EID), 0.00001);

    hcClient.iSetF32(PID, EID, VAL_MIN);
    assertEquals(VAL_MIN, hcClient.iGetF32(PID, EID), 0.00001);

    hcClient.iSetF32(PID, EID, VAL_MID);
    assertEquals(VAL_MID, hcClient.iGetF32(PID, EID), 0.00001);
  }

  @Test
  public void testF64() throws Exception {
    final int PID = 157;
    final double VAL_MAX = Double.MAX_VALUE;
    final double VAL_MIN = Double.MIN_VALUE;
    final double VAL_MID = 0.0001F;
    hcClient.setF64(PID, VAL_MAX);
    assertEquals(VAL_MAX, hcClient.getF64(PID), 0.00001);

    hcClient.setF64(PID, VAL_MIN);
    assertEquals(VAL_MIN, hcClient.getF64(PID), 0.00001);

    hcClient.setF64(PID, VAL_MID);
    assertEquals(VAL_MID, hcClient.getF64(PID), 0.00001);
  }

  @Test
  public void testiF64() throws Exception {
    final int PID = 160;
    final long EID = 0;
    final double VAL_MAX = Double.MAX_VALUE;
    final double VAL_MIN = Double.MIN_VALUE;
    final double VAL_MID = 0.0001F;
    hcClient.iSetF64(PID, EID, VAL_MAX);
    assertEquals(VAL_MAX, hcClient.iGetF64(PID, EID), 0.00001);

    hcClient.iSetF64(PID, EID, VAL_MIN);
    assertEquals(VAL_MIN, hcClient.iGetF64(PID, EID), 0.00001);

    hcClient.iSetF64(PID, EID, VAL_MID);
    assertEquals(VAL_MID, hcClient.iGetF64(PID, EID), 0.00001);
  }

  @Test
  public void testBool() throws HcException{
    final int PID = 135;
    hcClient.setBool(PID, true);
    assertTrue(hcClient.getBool(PID));

    hcClient.setBool(PID, false);
    assertFalse(hcClient.getBool(PID));

    hcClient.setBool(PID, true);
    assertTrue(hcClient.getBool(PID));
  }

  @Test
  public void testiBool() throws Exception {
    final int PID = 139;
    final long EID = 0;
    hcClient.iSetBool(PID, EID, true);
    assertTrue(hcClient.iGetBool(PID, EID));

    hcClient.iSetBool(PID, EID, false);
    assertFalse(hcClient.iGetBool(PID, EID));

    hcClient.iSetBool(PID, EID, true);
    assertTrue(hcClient.iGetBool(PID, EID));
  }

  @Test
  public void testString() throws Exception {
    final int PID = 167;
    final String val1 = "My Test String";
    final String val2 = "";
    hcClient.setString(PID, val1);
    assertEquals(val1, hcClient.getString(PID));

    hcClient.setString(PID, val2);
    assertEquals(val2, hcClient.getString(PID));
  }

  @Test
  public void testiString() throws Exception {
    final int PID = 170;
    final long EID = 0;
    final String val1 = "My Test String";
    final String val2 = "";
    hcClient.iSetString(PID, EID, val1);
    assertEquals(val1, hcClient.iGetString(PID, EID));

    hcClient.iSetString(PID, EID, val2);
    assertEquals(val2, hcClient.iGetString(PID, EID));
  }
  
  @Test
  public void testDownloadSif() throws Exception {
    hcClient.downloadSif(3, "siffile.xml");
  }
}
