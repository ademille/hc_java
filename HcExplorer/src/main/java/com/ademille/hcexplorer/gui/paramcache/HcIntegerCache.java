package com.ademille.hcexplorer.gui.paramcache;

import java.util.List;

import com.ademille.hc.core.HcEnum;
import com.ademille.hc.exception.HcException;
import com.ademille.hc.param.HcIntegerParam;

public class HcIntegerCache extends HcParamCache {

	HcIntegerParam hcParam;

	public HcIntegerCache(HcIntegerParam param) {
		super(param);
		hcParam = param;
	}

	public HcIntegerCache(HcIntegerParam param, int tid) {
		super(param, tid);
		hcParam = param;
	}

	public Comparable<? extends Number> getMaximum() {
		return hcParam.getUlim();
	}

	public Comparable<? extends Number> getMinimum() {
		return hcParam.getLlim();
	}

	public Number getStep() {
		return hcParam.isStepValid() ? hcParam.getStep() : null;
	};

	public List<HcEnum> getEnumList() {
		return hcParam.getEnums();
	}

	public Object getValueAsObject() {
		try {
			return hcParam.getLongValue();
		} catch (HcException rce) {
		}
		return null;
	}
	
	public Object iGetValueAsObject() {
		try {
			return hcParam.iGetLongValue(getTid());
		} catch (HcException rce) {
		}
		return null;
	}
}
