package com.ademille.hcexplorer.gui.editors;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.AbstractCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

import org.apache.log4j.Logger;

import com.ademille.hc.core.HcEnum;
import com.ademille.hc.exception.HcException;
import com.ademille.hcexplorer.gui.paramcache.HcBoolCache;

public class HcBoolCellEditor extends AbstractCellEditor implements
    TableCellEditor, ActionListener {

	private static final Logger logger = Logger
	    .getLogger("com.ademille.hcexplorer");

	JComboBox<String> combo;
	JCheckBox checkBox;
	boolean checkBoxUsed;
	int selectedIndex;
	List<HcEnum> enums;
	Object editorValue;

	public HcBoolCellEditor() {
		combo = new JComboBox<String>();
		checkBox = new JCheckBox();
		combo.addActionListener(this);
	}

	@Override
	public Object getCellEditorValue() {
		if (checkBoxUsed)
			return String.valueOf(checkBox.isSelected());
		else
			return combo.getSelectedIndex()==0?"false":"true";
	}

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value,
	    boolean isSelected, int row, int column) {
		if (value instanceof HcBoolCache) {
			HcBoolCache param = ((HcBoolCache) value);
			if (param.getEnumList() != null) {
				// Enum
				checkBoxUsed = false;
				return getEnumComponent(param);
			} else {
				// Default
				// TODO: Need to use cached value.
				try {
					checkBoxUsed = true;
					if (param.hasQualTidEnums() || param.hasQualTidRange())
					  checkBox.setSelected(param.iGetValue());
					else
					checkBox.setSelected(param.getValue());
				} catch (HcException hce) {
					logger.error(hce);
				}
				return checkBox;
			}
		}
		return null;
	}

	public JComponent getEnumComponent(HcBoolCache param) {
		List<HcEnum> enums = param.getEnumList();
		if (enums != null) {
			// Get the enum of the selected item.
			String val = "";
			try {
				if (param.hasQualTidEnums() || param.hasQualTidRange()) {
					// If it is a table type, we need to request the value based on the
					// table index
					val = param.iGetEnumString(param.getTid());				
				} else {
				  val = param.getEnumString();
				}
				combo.removeAllItems();
				int i = 0;
				for (HcEnum e : enums) {
					combo.addItem(e.getStr());
					if (e.getStr().equals(val)) {
						combo.setSelectedIndex(i);
					}
					i++;
				}
			} catch (HcException rce) {
				logger.error("Error retrieving enum value." + rce);
				return null;
			}
			return combo;
		} else {
			logger.error("Invalid Enumeration list.");
			return null;
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// Make the renderer reappear.
		fireEditingStopped();
	}

}
