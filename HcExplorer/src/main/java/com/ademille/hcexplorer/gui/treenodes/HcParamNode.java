/*
 * Copyright (C) 2008  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hcexplorer.gui.treenodes;

import javax.swing.ImageIcon;

import com.ademille.hc.exception.HcException;
import com.ademille.hc.param.HcParam;
import com.ademille.hcexplorer.gui.util.ImageUtil;

public class HcParamNode extends HcHelperNode {

  protected HcParam param;

  public HcParamNode(HcParam param) {
    this.param = param;
  }

  private void createActions() {
    /*
    Action a = new HVTAction("Edit Teacher",
        ImageUtil.createIcon("user_edit.png"), "Edit Teacher", KeyEvent.VK_E,
        new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            EditTeacherDialog dlg = new EditTeacherDialog(getTopFrame(),
                teacher);
            dlg.setModal(true);
            dlg.setVisible(true);
            if (!dlg.isCancelled()) {
              // Remove the old teacher from the model.
              teacherList.remove(teacher);
              // Save the updated teacher.
              teacher = dlg.getTeacher();
              // Update the master teacher model.
              teacherList.add(teacher);
              // Refresh the node display.
              updateHVTHelperNode(HcParamNode.this);
            }
          }
        });
    actionList.add(a);
    */
    }

  @Override
  public void populate() {
    createActions();
  }

  @Override
  public String toString() {
    return param.getName();
  }

  @Override
  public String[][] iGetData() {
    String value;
    try {
    value = param.getValueString();
    } catch (HcException e){
      value = "Error: " + e.getType();
    }
    
    return new String[][] { { "Name", param.getName() },
                            { "Value", value } };
  }

  @Override
  public ImageIcon getIcon() {
    return ImageUtil.createIcon("user.png");
  }

}
