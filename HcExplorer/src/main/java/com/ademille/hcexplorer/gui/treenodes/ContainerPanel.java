package com.ademille.hcexplorer.gui.treenodes;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.ademille.hc.core.HcContainer;

public class ContainerPanel extends JPanel {
  private HcParamTable table;
  private JTextArea textArea;

  private class SelectionListener implements ListSelectionListener {

    @Override
    public void valueChanged(ListSelectionEvent e) {
      if (e.getValueIsAdjusting()) {
        return;
      } else {
        int row = table.getSelectedRow();
        if (row != -1) { // Make sure there is a selected row.
        	
        	HcContainerTableModel model = (HcContainerTableModel)table.getModel();
          textArea.setText(model.getRowInfo(row));
        }
      }
    }
  }

  void refresh() {
    table.refresh();
  }

  /**
   * Create the panel.
   */
  public ContainerPanel(HcContainer cont) {

    setLayout(new BorderLayout(0, 0));

    JSplitPane splitPane = new JSplitPane();
    splitPane.setOneTouchExpandable(true);
    splitPane.setResizeWeight(0.6);
    splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
    add(splitPane, BorderLayout.CENTER);

    JScrollPane scrollPaneTable = new JScrollPane();
    splitPane.setLeftComponent(scrollPaneTable);

    table = new HcParamTable(cont);
    table.getSelectionModel().addListSelectionListener(new SelectionListener());
    table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    table.setFillsViewportHeight(true);
    //table.setAutoCreateRowSorter(true);
    scrollPaneTable.setViewportView(table);

    JScrollPane scrollPaneInfo = new JScrollPane();
    splitPane.setRightComponent(scrollPaneInfo);

    textArea = new JTextArea();
    textArea.setRows(5);
    scrollPaneInfo.setViewportView(textArea);

    refresh(); // Populate cache and refresh table.
  }
}
