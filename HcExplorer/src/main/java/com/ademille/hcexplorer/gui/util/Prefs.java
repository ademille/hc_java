/*
 * Copyright (C) 2011  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hcexplorer.gui.util;

import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import org.apache.log4j.Logger;

/**
 * HTHPreferences. This class provides a way to save and restore user-defined
 * preferences.
 * 
 * @author Aaron DeMille
 * 
 */
public class Prefs {

  private static final Logger logger = Logger
      .getLogger("com.ademille.hcexplorer");

  private Preferences prefs = null;

  private final String LAST_IP_ADDR = "lastIpAddr";
  private final String LAST_PORT = "lastPort";

  public Prefs() {
    prefs = Preferences.userNodeForPackage(getClass());
  }

  public String getLastIpAddr() {
    return prefs.get(LAST_IP_ADDR, "");
  }

  public void setLastIpAddr(String ipAddr) {
    prefs.put(LAST_IP_ADDR, ipAddr);
    try {
      prefs.flush();
    } catch (BackingStoreException e) {
      logger.error("Unable to save preferences:" + e);
    }
  }

  public String getLastPort() {
    return prefs.get(LAST_PORT, "");
  }

  public void setLastPort(String lastPort) {
    prefs.put(LAST_PORT, lastPort);
    try {
      prefs.flush();
    } catch (BackingStoreException e) {
      logger.error("Unable to save preferences:" + e);
    }
  }

}
