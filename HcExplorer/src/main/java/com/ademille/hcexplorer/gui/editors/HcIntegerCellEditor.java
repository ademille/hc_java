package com.ademille.hcexplorer.gui.editors;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.AbstractCellEditor;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.table.TableCellEditor;

import org.apache.log4j.Logger;

import com.ademille.hc.core.HcEnum;
import com.ademille.hc.exception.HcException;
import com.ademille.hc.exception.RangeException;
import com.ademille.hcexplorer.gui.paramcache.HcIntegerCache;

public class HcIntegerCellEditor extends AbstractCellEditor implements
    TableCellEditor, ActionListener {

	private static final Logger logger = Logger
	    .getLogger("com.ademille.hcexplorer");

	private JComboBox<String> combo;
	private JTextField text;
	private JSpinner spinner;
	private SpinnerNumberModel spinnerModel;
	private List<HcEnum> enums;

	private enum Type {
		NONE, COMBO, TEXT, SPINNER
	};

	private Type type = Type.NONE;

	public HcIntegerCellEditor() {
		combo = new JComboBox<String>();
		text = new JTextField();
		spinnerModel = new SpinnerNumberModel();
		spinner = new JSpinner(spinnerModel);

		combo.addActionListener(this);
	}

	@Override
	public Object getCellEditorValue() {
		switch (type) {
		case COMBO:
			if (combo.getSelectedIndex() == -1)
				return null;
			else
				return String.valueOf(enums.get(combo.getSelectedIndex()).getVal());
		case TEXT:
			return text.getText();
		case SPINNER:
			return spinner.getValue().toString();
		default:
			return null;
		}
	}

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value,
	    boolean isSelected, int row, int column) {
		type = Type.NONE;
		if (value instanceof HcIntegerCache) {
			HcIntegerCache param = ((HcIntegerCache) value);
			if (param.getEnumList() != null) {
				type = Type.COMBO;
				enums = param.getEnumList();
				return getEnumComponent(param);
			} else if (param.getStep() != null) {
				type = Type.SPINNER;
				spinnerModel = new SpinnerNumberModel();
				spinnerModel.setMaximum(param.getMaximum());
				spinnerModel.setMinimum(param.getMinimum());
				spinnerModel.setStepSize(param.getStep());
				// TODO: Need to use cached value.
				if (param.hasQualTidEnums() || param.hasQualTidRange())
				  spinnerModel.setValue(param.iGetValueAsObject());
				else
				  spinnerModel.setValue(param.getValueAsObject());
				spinner.setModel(spinnerModel);
				return spinner;
			} else {
				// Default
				type = Type.TEXT;
				if (param.hasQualTidEnums() || param.hasQualTidRange())
  				text.setText(String.valueOf(param.iGetValueAsObject()));
				else
  				text.setText(String.valueOf(param.getValueAsObject()));
				return text;
			}
		}
		return null;
	}

	public JComponent getEnumComponent(HcIntegerCache param) {
		List<HcEnum> enums = param.getEnumList();
		if (enums != null) {
			// Get the enum of the selected item.
			String val = "";
			try {
				if (param.hasQualTidEnums() || param.hasQualTidRange()) {
					// If it is a table type, we need to request the value based on the
					// table index
					val = param.iGetEnumString(param.getTid());
				} else {
					val = param.getEnumString();
				}
			} catch (RangeException rce) {
				logger.error("Error retrieving enum value." + rce);
				// We couldn't retrieve the enum, but that might just be because the value
				// returned didn't match one of the enum values. In that case a
				// RangeException will be thrown. So, still allow the drop-down list to
				// appear.
			} catch (HcException hce) {
				logger.error("Error retrieving enum value." + hce);
				return null;
			}
			combo.removeAllItems();
			int i = 0;
			for (HcEnum e : enums) {
				combo.addItem(e.getStr());
				if (e.getStr().equals(val)) {
					combo.setSelectedIndex(i);
				}
				i++;
			}
			return combo;
		} else {
			logger.error("Invalid Enumeration list.");
			return null;
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// Make the renderer reappear.
		fireEditingStopped();
	}
}
