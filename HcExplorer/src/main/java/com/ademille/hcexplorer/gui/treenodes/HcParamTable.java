package com.ademille.hcexplorer.gui.treenodes;

import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import com.ademille.hc.core.HcContainer;


public class HcParamTable extends JTable {
private HcContainerTableModel model;

	public HcParamTable(HcContainer cont){
		model = new HcContainerTableModel(cont);
		setModel(model);
		//Bump up the row height, so the editors look good.
		setRowHeight(getRowHeight() + 5);
	}
	
	@Override
	public TableCellEditor getCellEditor(int row, int column) {
		//See if the model can provide an editor. If it can't use the default one.
		TableCellEditor editor = model.getCellEditorAt(row, column);
		if (editor != null){
			return editor;
		} else {
			return super.getCellEditor(row, column);
		}
	}
	
	@Override
	public TableCellRenderer getCellRenderer(int row, int column) {
		TableCellRenderer renderer = model.getCellRenderer(row, column);
		if (renderer != null){
			return renderer;
		} else {
			return super.getCellRenderer(row, column);
		}
  }
	
	public void refresh(){
		model.refresh();
	}

}
