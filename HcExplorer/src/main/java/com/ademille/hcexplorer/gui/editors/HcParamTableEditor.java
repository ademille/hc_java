package com.ademille.hcexplorer.gui.editors;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

import com.ademille.hcexplorer.gui.paramcache.HcParamCache;
import com.ademille.hcexplorer.gui.treenodes.HcTableParamDialog;

public class HcParamTableEditor extends AbstractCellEditor implements
    TableCellEditor, ActionListener {
	HcParamCache hcParam;
	JButton button;
	HcTableParamDialog dialog;
	protected static final String TABLE = "table";

	public HcParamTableEditor() {
		button = new JButton();
		button.setActionCommand(TABLE);
		button.addActionListener(this);
		button.setBorderPainted(false);

		// Set up the dialog that the button brings up.
		dialog = new HcTableParamDialog();
		dialog.setModal(true);
	}

	public void actionPerformed(ActionEvent e) {
		if (TABLE.equals(e.getActionCommand())) {
			// The user has clicked the cell, so
			// bring up the dialog.
			dialog.setVisible(true);

			fireEditingStopped(); // Make the renderer reappear.
		} 
	}

	// Implement the one CellEditor method that AbstractCellEditor doesn't.
	public Object getCellEditorValue() {
		return hcParam;
	}

	// Implement the one method defined by TableCellEditor.
	public Component getTableCellEditorComponent(JTable table, Object value,
	    boolean isSelected, int row, int column) {
		hcParam = (HcParamCache) value;
		dialog.setData(hcParam);
		return button;
	}

}
