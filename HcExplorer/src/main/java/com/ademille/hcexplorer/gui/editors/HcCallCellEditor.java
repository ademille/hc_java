package com.ademille.hcexplorer.gui.editors;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

import com.ademille.hcexplorer.gui.paramcache.HcParamCache;

public class HcCallCellEditor extends AbstractCellEditor implements
    TableCellEditor, ActionListener {
	HcParamCache hcParam;
	JButton button;
	protected static final String TABLE = "table";

	public HcCallCellEditor() {
		button = new JButton();
		button.addActionListener(this);
		button.setBorderPainted(false);
	}

	public void actionPerformed(ActionEvent e) {
		fireEditingStopped(); // Make the renderer reappear.
	}

	// Implement the one CellEditor method that AbstractCellEditor doesn't.
	public Object getCellEditorValue() {
		return hcParam;
	}

	// Implement the one method defined by TableCellEditor.
	public Component getTableCellEditorComponent(JTable table, Object value,
	    boolean isSelected, int row, int column) {
		return button;
	}
}
