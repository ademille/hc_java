package com.ademille.hcexplorer.gui.paramcache;

import java.util.List;

import com.ademille.hc.core.HcEnum;
import com.ademille.hc.exception.HcException;
import com.ademille.hc.param.HcBool;

public class HcBoolCache extends HcParamCache {
	
	private HcBool boolParam;

	public HcBoolCache(HcBool param) {
	  super(param);
	  this.boolParam = param;
  }

	public HcBoolCache(HcBool param, int tid) {
		super(param, tid);
	  this.boolParam = param;
  }
	
	public List<HcEnum> getEnumList(){
		return boolParam.getEnumList();
	}
	
	public boolean getValue() throws HcException{
		 return boolParam.getValue();
	}
	
	public boolean iGetValue() throws HcException{
	  return boolParam.iGetValue(getTid());
	}
}
