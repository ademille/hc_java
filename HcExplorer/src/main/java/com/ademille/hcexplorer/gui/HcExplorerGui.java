/*
 * Copyright (C) 2008  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hcexplorer.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;

import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import com.ademille.hc.core.HcClient;
import com.ademille.hc.core.HcContainer;
import com.ademille.hc.device.UdpDevice;
import com.ademille.hcexplorer.Version;
import com.ademille.hcexplorer.gui.logging.LogTable;
import com.ademille.hcexplorer.gui.treenodes.ContainerNode;
import com.ademille.hcexplorer.gui.treenodes.HcHelperNode;
import com.ademille.hcexplorer.gui.util.GuiAction;
import com.ademille.hcexplorer.gui.util.ImageUtil;
import com.ademille.hcexplorer.gui.util.Prefs;

public class HcExplorerGui extends JFrame implements TreeSelectionListener {

  private static final long serialVersionUID = 1L;

  private JPanel guiContentPane = null;

  private JMenuBar menuBar = null;

  private JMenu mnuFile = null;

  private JScrollPane scrollPaneTree = null;

  private JTree treeAssignments = null;

  private JPanel panelMain = null;

  private JSplitPane splitPaneTree = null;

  private JSplitPane splitPaneInfo = null;

  private JScrollPane scrollPaneLog = null;

  private JMenuItem mnuExit = null;

  private static final Logger logger = Logger
      .getLogger("com.ademille.hcexplorer"); // @jve:decl-index=0:

  private JToolBar toolBarTop = null;

  private JButton btnConnect = null;

  private Action connectAction;

  private JPanel panelTree = null;

  private JPanel panelTreeSearch = null;

  private JTextField txtTreeSearch = null;

  private JButton btnDown = null;

  private JButton btnUp = null;

  private JMenu mnuHelp = null;

  private JMenuItem mnuAbout = null;

  private String aboutMessage; // @jve:decl-index=0:

  private JButton btnSearch = null;

  private List<HcHelperNode> searchResults = null;

  private int searchPos = 0;

  private JLabel lblSearchStatus = null;

  private JPanel panelSearchStatus = null;

  private JPanel panelActions = null;

  private JPopupMenu treePopupMenu;

  private enum SearchResult {
    NEXT, PREVIOUS
  };

  private HcContainer topCont; // @jve:decl-index=0:

  private static String title = "HC Explorer " + Version.major + "."
      + Version.minor + "." + Version.bugfix; // @jve:decl-index=0:


  private LogTable logTable = null;

  private Prefs prefs;
  private JScrollPane scrollPane;
  private JTable tableElements;
  private JLabel lblIpAddress;
  private JTextField txtIpAddr;
  private JLabel lblPort;
  private JTextField txtPort;
  private JPanel panelConnection;
  private JPanel panelConnect;

  private class NodeRenderer extends DefaultTreeCellRenderer {
    private static final long serialVersionUID = 1L;

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value,
        boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
      // Get the default behavior.
      super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row,
          hasFocus);

      // Just override the icon.
      if (value instanceof HcHelperNode) {
        HcHelperNode node = (HcHelperNode) value;
        ImageIcon icon = node.getIcon();
        if (icon != null) {
          setIcon(icon);
        }
      }
      return this;
    }
  }
  /**
   * This is the default constructor
   */
  public HcExplorerGui() {
    super();

    prefs = new Prefs();

    createActions();
    initialize();

    //Restore Last connection info.
    txtIpAddr.setText(prefs.getLastIpAddr());
    txtPort.setText(prefs.getLastPort());

    addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        exitApp();
      }
    });

  }


  /**
   * This method initializes this
   *
   * @return void
   */
  private void initialize() {
    this.setSize(814, 546);
    this.setIconImage(Toolkit.getDefaultToolkit().getImage(
        getClass().getResource("/icons/opentype.gif")));
    this.setJMenuBar(getAppMenuBar());
    this.setContentPane(getGuiContentPane());
    this.setTitle(title);
    this.setLocationRelativeTo(null);
    logger.addAppender(logTable.getAppender());
    this.aboutMessage = "<html><center><b>RC Explorer</b><br>"
        + Version.major
        + "."
        + Version.minor
        + "."
        + Version.bugfix
        + "<br>"
        + "Copyright 2011<br> Aaron DeMille<br>"
        + "Build Date: "
        + Version.buildTime
        + "<br>"
        + "ajdemille@gmail.com</center><br><br>"
        + "Some icons provided by:<br>"
        + "http://www.famfamfam.com/lab/icons/silk/" + "</html>";
  }

  /*
  private void setTitleWithIdent(String ident) {
    String title = HcExplorerGui.title;
    if (ident != null && !"".equals(ident)) {
      title += " - " + ident;
    }
    setTitle(title);
  }
  */

  private void createActions() {
    // Setup Import Action
    connectAction = new GuiAction("Connect",
        ImageUtil.createIcon("import.gif"), "Connect to RC Server",
        KeyEvent.VK_C, new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
                connectToServer();
          }
        });

  }

  private List<TreePath> getTreePathsForDepth(TreeNode root, int depth) {

    List<TreePath> paths = new LinkedList<TreePath>();
    if (root.getChildCount() == 0) {
      TreePath path = HcHelperNode.getPath(root.getParent());
      paths.add(path);
    } else if (depth == 0) {
      TreePath path = HcHelperNode.getPath(root);
      paths.add(path);
    } else {
      // recurse on each child.
      for (int i = 0; i < root.getChildCount(); i++) {
        List<TreePath> childPaths = getTreePathsForDepth(root.getChildAt(i),
            depth - 1);
        paths.addAll(childPaths);
      }
    }
    return paths;
  }

  private void expandTree() {
    List<TreePath> paths = getTreePathsForDepth((TreeNode) treeAssignments
        .getModel().getRoot(), 1);
    for (TreePath t : paths) {
      treeAssignments.expandPath(t);
    }
    // Select the top node.
    treeAssignments.setSelectionRow(0);
  }

  /**
   * This method initializes guiContentPane
   *
   * @return javax.swing.JPanel
   */
  private JPanel getGuiContentPane() {
    if (guiContentPane == null) {
      guiContentPane = new JPanel();
      guiContentPane.setLayout(new BorderLayout());
      guiContentPane.add(getSplitPaneTree(), BorderLayout.CENTER);
      guiContentPane.add(getPanelConnection(), BorderLayout.NORTH);
    }
    return guiContentPane;
  }

  /**
   * This method initializes menuBar
   *
   * @return javax.swing.JMenuBar
   */
  private JMenuBar getAppMenuBar() {
    if (menuBar == null) {
      menuBar = new JMenuBar();
      menuBar.add(getMnuFile());
      menuBar.add(getMnuHelp());
    }
    return menuBar;
  }

  /**
   * This method initializes mnuFile
   *
   * @return javax.swing.JMenu
   */
  private JMenu getMnuFile() {
    if (mnuFile == null) {
      mnuFile = new JMenu();
      mnuFile.setText("File");
      mnuFile.setMnemonic(KeyEvent.VK_F);
      mnuFile.add(getMnuExit());
    }
    return mnuFile;
  }

  /**
   * Read a directory that contains MLS Data and import it.
   */
  private boolean connectToServer() {
    waitCursor();
    UdpDevice udpDev = new UdpDevice();
    try {
      //Get the IP Address and Port for the connection.
      InetAddress addr = InetAddress.getByName(txtIpAddr.getText());
      int port = Integer.parseInt(txtPort.getText());

      udpDev.setDestination(addr, port);
      /* Create top container */
      logger.info("Creating top container");
      topCont = new HcContainer("");
      HcClient cli = new HcClient(udpDev);
      //cli.registerDebugParams(topCont, "rccli");
      // cli.setDebug(true);
      try {
        logger.info("Pinging IP: " + txtIpAddr.getText().trim() + " Port: " + txtPort.getText().trim());
        //cli.ping();
        logger.info("Ping was successful!");

        //Since the ping was successful, save the connection
        prefs.setLastIpAddr(txtIpAddr.getText().trim());
        prefs.setLastPort(txtPort.getText().trim());

        /* Get a list of elements on the server */
        /* Get parameter info from server */
        logger.info("Getting parameter info from server\n");

        //HcContainer cont = cli.discover();
        //populateTree(cont);
        normalCursor();
        return true;

      }
      catch (Exception e) {
      //catch (HcException e) {
        logger.error("Exception pinging: " + e.getMessage());
        normalCursor();
        return false;
      }

    } catch (UnknownHostException e) {
        logger.error("Unknown Host: " + e.getMessage());
        normalCursor();
        return false;
    }
  }

  private void populateTree(HcContainer cont) {
    HcHelperNode top = new ContainerNode(cont);
    DefaultTreeModel model = new DefaultTreeModel(top);
    // Save the current model in the HcHelperNode so that it
    // can update the JTree when the model changes.
    HcHelperNode.setModel(model);
    HcHelperNode.setTree(treeAssignments);
    // Pass in the JFrame for use with dialog windows.
    HcHelperNode.setTopFrame(this);
    top.populate();
    treeAssignments.setModel(model);

    // Expand the tree out a bit.
    expandTree();
  }

  /**
   * This method initializes scrollPaneTree
   *
   * @return javax.swing.JScrollPane
   */
  private JScrollPane getScrollPaneTree() {
    if (scrollPaneTree == null) {
      scrollPaneTree = new JScrollPane();
      scrollPaneTree.setViewportView(getTreeAssignments());
    }
    return scrollPaneTree;
  }

  /**
   * This method initializes treeAssignments
   *
   * @return javax.swing.JTree
   */
  private JTree getTreeAssignments() {
    if (treeAssignments == null) {
      treeAssignments = new JTree();
      //treeAssignments.setModel(new DefaultTreeModel(new ContainerNode(null)));
      treeAssignments.setModel(new DefaultTreeModel(null));
      treeAssignments.setShowsRootHandles(true);
      treeAssignments.getSelectionModel().setSelectionMode(
          TreeSelectionModel.SINGLE_TREE_SELECTION);
      treeAssignments.addTreeSelectionListener(this);
      treeAssignments.setCellRenderer(new NodeRenderer());

      treeAssignments.addMouseListener(new java.awt.event.MouseAdapter() {
        public void mousePressed(java.awt.event.MouseEvent e) {
          showContextMenu(e);
        }

        public void mouseReleased(java.awt.event.MouseEvent e) {
          showContextMenu(e);
        }
      });
    }
    return treeAssignments;
  }

  private void showContextMenu(MouseEvent e) {
    if (e.isPopupTrigger()) {
      // Select the node that was clicked on.
      TreePath selPath = treeAssignments.getPathForLocation(e.getX(), e.getY());
      treeAssignments.setSelectionPath(selPath);
      if (null != treePopupMenu) {
        treePopupMenu.show((Component) e.getSource(), e.getX(), e.getY());
      }
    }

  }

  /**
   * This method initializes panelMain
   *
   * @return javax.swing.JPanel
   */
  private JPanel getPanelMain() {
    if (panelMain == null) {
      panelMain = new JPanel();
      panelMain.setLayout(new BorderLayout());
      panelMain.add(getScrollPane(), BorderLayout.CENTER);
    }
    return panelMain;
  }

  /**
   * This method initializes splitPaneTree
   *
   * @return javax.swing.JSplitPane
   */
  private JSplitPane getSplitPaneTree() {
    if (splitPaneTree == null) {
      splitPaneTree = new JSplitPane();
      splitPaneTree.setDividerLocation(250);
      splitPaneTree.setLeftComponent(getPanelTree());
      splitPaneTree.setRightComponent(getSplitPaneInfo());
    }
    return splitPaneTree;
  }

  /**
   * This method initializes splitPaneInfo
   *
   * @return javax.swing.JSplitPane
   */
  private JSplitPane getSplitPaneInfo() {
    if (splitPaneInfo == null) {
      splitPaneInfo = new JSplitPane();
      splitPaneInfo.setOrientation(JSplitPane.VERTICAL_SPLIT);
      splitPaneInfo.setDividerLocation(350);
      splitPaneInfo.setResizeWeight(0.85D);
      splitPaneInfo.setPreferredSize(new Dimension(555, 480));
      splitPaneInfo.setBottomComponent(getScrollPaneLog());
      splitPaneInfo.setTopComponent(getPanelMain());
    }
    return splitPaneInfo;
  }

  /**
   * This method initializes scrollPaneLog
   *
   * @return javax.swing.JScrollPane
   */
  private JScrollPane getScrollPaneLog() {
    if (scrollPaneLog == null) {
      scrollPaneLog = new JScrollPane();
      scrollPaneLog.setViewportView(getLogTable());
    }
    return scrollPaneLog;
  }

  /**
   * This method initializes mnuExit
   *
   * @return javax.swing.JMenuItem
   */
  private JMenuItem getMnuExit() {
    if (mnuExit == null) {
      mnuExit = new JMenuItem();
      mnuExit.setText("Exit");
      mnuExit.setMnemonic(KeyEvent.VK_X);
      mnuExit.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          exitApp();
        }
      });
    }
    return mnuExit;
  }

  /**
   * This method initializes toolBarTop
   *
   * @return javax.swing.JToolBar
   */
  private JToolBar getToolBarTop() {
    if (toolBarTop == null) {
      toolBarTop = new JToolBar();
      toolBarTop.add(getPanelActions());
    }
    return toolBarTop;
  }

  /**
   * This method initializes btnImport
   *
   * @return javax.swing.JButton
   */
  private JButton getBtnConnect() {
    if (btnConnect == null) {
      btnConnect = new JButton();
      btnConnect.setAction(connectAction);
    }
    return btnConnect;
  }

  /**
   * This method initializes panelTree
   *
   * @return javax.swing.JPanel
   */
  private JPanel getPanelTree() {
    if (panelTree == null) {
      panelTree = new JPanel();
      panelTree.setLayout(new BorderLayout());
      panelTree.add(getScrollPaneTree(), BorderLayout.CENTER);
      panelTree.add(getPanelTreeSearch(), BorderLayout.SOUTH);
    }
    return panelTree;
  }

  /**
   * This method initializes panelTreeSearch
   *
   * @return javax.swing.JPanel
   */
  private JPanel getPanelTreeSearch() {
    if (panelTreeSearch == null) {
      GridBagConstraints gridBagConstraints31 = new GridBagConstraints();
      gridBagConstraints31.anchor = GridBagConstraints.WEST;
      gridBagConstraints31.gridy = 1;
      gridBagConstraints31.gridwidth = 4;
      gridBagConstraints31.gridx = 0;
      lblSearchStatus = new JLabel();
      lblSearchStatus.setText("");
      GridBagConstraints gridBagConstraints4 = new GridBagConstraints();
      gridBagConstraints4.gridy = 0;
      gridBagConstraints4.gridx = 2;
      GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
      gridBagConstraints3.gridy = 0;
      gridBagConstraints3.gridx = 3;
      GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
      gridBagConstraints2.gridy = 0;
      gridBagConstraints2.gridx = 1;
      GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
      gridBagConstraints1.fill = GridBagConstraints.HORIZONTAL;
      gridBagConstraints1.gridx = 0;
      gridBagConstraints1.gridy = 0;
      gridBagConstraints1.weightx = 1.0;
      gridBagConstraints1.insets = new Insets(5, 5, 5, 5);
      GridBagConstraints gridBagConstraints = new GridBagConstraints();
      gridBagConstraints.fill = GridBagConstraints.VERTICAL;
      gridBagConstraints.weightx = 1.0;
      panelTreeSearch = new JPanel();
      panelTreeSearch.setLayout(new GridBagLayout());
      panelTreeSearch.setVisible(true);
      panelTreeSearch.add(getTxtTreeSearch(), gridBagConstraints1);
      panelTreeSearch.add(getBtnSearch(), gridBagConstraints2);
      panelTreeSearch.add(getBtnUp(), gridBagConstraints3);
      panelTreeSearch.add(getBtnDown(), gridBagConstraints4);
      panelTreeSearch.add(getPanelSearchStatus(), gridBagConstraints31);
    }
    return panelTreeSearch;
  }

  /**
   * This method initializes txtTreeSearch
   *
   * @return javax.swing.JTextField
   */
  private JTextField getTxtTreeSearch() {
    if (txtTreeSearch == null) {
      txtTreeSearch = new JTextField();
      txtTreeSearch.setPreferredSize(new Dimension(150, 25));
      txtTreeSearch.setText("");
    }
    return txtTreeSearch;
  }

  /**
   * This method initializes btnDown
   *
   * @return javax.swing.JButton
   */
  private JButton getBtnDown() {
    if (btnDown == null) {
      btnDown = new JButton();
      btnDown.setText("");
      btnDown.setPreferredSize(new Dimension(25, 26));
      btnDown.setEnabled(false);
      btnDown.setIcon(new ImageIcon(getClass().getResource(
          "/icons/down.png")));
      btnDown.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          showSearchNode(SearchResult.NEXT);
        }
      });
    }
    return btnDown;
  }

  /**
   * This method initializes btnUp
   *
   * @return javax.swing.JButton
   */
  private JButton getBtnUp() {
    if (btnUp == null) {
      btnUp = new JButton();
      btnUp.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          showSearchNode(SearchResult.PREVIOUS);
        }
      });
      btnUp.setText("");
      btnUp.setPreferredSize(new Dimension(25, 26));
      btnUp.setEnabled(false);
      btnUp.setIcon(new ImageIcon(getClass().getResource(
          "/icons/up.png")));
    }
    return btnUp;
  }

  /**
   * This method initializes mnuHelp
   *
   * @return javax.swing.JMenu
   */
  private JMenu getMnuHelp() {
    if (mnuHelp == null) {
      mnuHelp = new JMenu();
      mnuHelp.setText("Help");
      mnuHelp.setMnemonic(KeyEvent.VK_H);
      mnuHelp.add(getMnuAbout());
    }
    return mnuHelp;
  }

  /**
   * This method initializes mnuAbout
   *
   * @return javax.swing.JMenuItem
   */
  private JMenuItem getMnuAbout() {
    if (mnuAbout == null) {
      mnuAbout = new JMenuItem();
      mnuAbout.setText("About");
      mnuAbout.setMnemonic(KeyEvent.VK_A);
      mnuAbout.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          JOptionPane.showMessageDialog(HcExplorerGui.this, aboutMessage);
        }
      });
    }
    return mnuAbout;
  }

  /**
   * This method initializes btnSearch
   *
   * @return javax.swing.JButton
   */
  private JButton getBtnSearch() {
    if (btnSearch == null) {
      btnSearch = new JButton();
      btnSearch.setIcon(new ImageIcon(getClass().getResource(
          "/icons/magnifier.png")));
      btnSearch.setPreferredSize(new Dimension(25, 26));
      btnSearch.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent e) {
          // Make sure some text has been entered in search box.
          if (txtTreeSearch.getText().trim().equals("")) {
            lblSearchStatus
                .setText("<html><font color=red>Please enter text to search for first.</font></html>");
          } else {
            searchResults = searchNode(txtTreeSearch.getText());
            // Set the searchPos to the entry before the first so it can
            // be advanced to 0 in showSearchNode.
            searchPos = -1;
            // This will either jump to the first match and
            // disable the next, previous buttons as necessary.
            showSearchNode(SearchResult.NEXT);
            btnDown.grabFocus();
          }
        }
      });
    }
    return btnSearch;
  }

  private void showSearchNode(SearchResult type) {
    // make the node visible by scroll to it
    DefaultTreeModel model = (DefaultTreeModel) treeAssignments.getModel();
    HcHelperNode node = null;
    if (null != searchResults && searchResults.size() > 0) {
      if (SearchResult.NEXT == type) {
        // Bounds check.
        if (searchPos + 1 < searchResults.size()) {
          node = searchResults.get(++searchPos);
        }
      } else if (SearchResult.PREVIOUS == type) {
        // Bounds check.
        if (searchPos - 1 >= 0) {
          node = searchResults.get(--searchPos);
        }
      }

      // Disable buttons based on list.
      btnDown.setEnabled(searchPos + 1 < searchResults.size());
      btnUp.setEnabled(searchPos - 1 >= 0);

      if (null != node) {
        TreeNode[] nodes = model.getPathToRoot(node);
        TreePath path = new TreePath(nodes);
        treeAssignments.scrollPathToVisible(path);
        treeAssignments.setSelectionPath(path);
      }
      lblSearchStatus.setText("Search Result: " + (searchPos + 1) + " of "
          + searchResults.size());
    } else {
      lblSearchStatus
          .setText("<html><font color=red>Search term not found.</font></html>");
    }
  }

  private List<HcHelperNode> searchNode(String str) {
    HcHelperNode node = null;
    HcHelperNode root = (HcHelperNode) treeAssignments.getModel().getRoot();
    List<HcHelperNode> nodeList = new LinkedList<HcHelperNode>();

    // Get the enumeration
    Enumeration<TreeNode> en = root.depthFirstEnumeration();

    // iterate through the enumeration
    str = str.toLowerCase(); // make search case insensitive.
    while (en.hasMoreElements()) {
      // get the node
      node = (HcHelperNode)en.nextElement();

      // match the string with the user-object of the node
      String nodeName = node.getUserObject().toString().toLowerCase();
      if (nodeName.contains(str)) {
        // tree node with string found
        nodeList.add(node);
      }
    }

    // tree node with string node found return null
    return nodeList;
  }

  /**
   * This method initializes panelSearchStatus
   *
   * @return javax.swing.JPanel
   */
  private JPanel getPanelSearchStatus() {
    if (panelSearchStatus == null) {
      GridBagConstraints gridBagConstraints6 = new GridBagConstraints();
      gridBagConstraints6.anchor = GridBagConstraints.WEST;
      gridBagConstraints6.gridx = -1;
      gridBagConstraints6.gridy = -1;
      gridBagConstraints6.weightx = 1.0D;
      gridBagConstraints6.gridwidth = 3;
      GridBagConstraints gridBagConstraints5 = new GridBagConstraints();
      gridBagConstraints5.anchor = GridBagConstraints.WEST;
      gridBagConstraints5.gridwidth = 1;
      gridBagConstraints5.gridx = -1;
      gridBagConstraints5.gridy = -1;
      gridBagConstraints5.insets = new Insets(5, 5, 5, 5);
      panelSearchStatus = new JPanel();
      panelSearchStatus.setLayout(new FlowLayout());
      panelSearchStatus.add(lblSearchStatus, null);
    }
    return panelSearchStatus;
  }

  /**
   * This method initializes panelActions
   *
   * @return javax.swing.JPanel
   */
  private JPanel getPanelActions() {
    if (panelActions == null) {
      panelActions = new JPanel();
      GridBagLayout gbl_panelActions = new GridBagLayout();
      gbl_panelActions.columnWidths = new int[]{80, 280, 35, 280, 112, 0};
      gbl_panelActions.rowHeights = new int[]{25, 0};
      gbl_panelActions.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
      gbl_panelActions.rowWeights = new double[]{0.0, Double.MIN_VALUE};
      panelActions.setLayout(gbl_panelActions);
    }
    return panelActions;
  }

  /**
   * This method initializes logTable
   *
   * @return com.ademille.hcexplorer.gui.logging.LogTable
   */
  private LogTable getLogTable() {
    if (logTable == null) {
      logTable = new LogTable();
    }
    return logTable;
  }


  /**
   * @param args
   */
  public static void main(String[] args) {
    DOMConfigurator.configure("log4jconfig.xml");
    try {
      // Set System L&F
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    } catch (Exception e) {
      logger.error(e.getMessage());
    }

    SwingUtilities.invokeLater(new Runnable() {
      public void run() {
        HcExplorerGui thisClass = new HcExplorerGui();
        thisClass.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        thisClass.pack();
        thisClass.setVisible(true);
      }
    });
  }

  @Override
  public void valueChanged(TreeSelectionEvent e) {
    final HcHelperNode node = (HcHelperNode) treeAssignments
        .getLastSelectedPathComponent();

    if (node != null) {
      // Add the actions to the action panel.
      panelActions.removeAll();
      // treePopupMenu = null;
      List<Action> actions = node.getActionList();
      if (null != actions) {
        // Add all the actions to the Button Bar.
        for (Action a : actions) {
          panelActions.add(new JButton(a));
        }

        // Add all the actions to the Context menu.
        treePopupMenu = new JPopupMenu();
        for (Action a : actions) {
          JMenuItem m = new JMenuItem(a);
          // m.setAccelerator(KeyStroke.getKeyStroke('D', Toolkit
          // .getDefaultToolkit().getMenuShortcutKeyMask()));
          treePopupMenu.add(m);
        }

      }
      panelActions.revalidate();
      //Try to get a table model from the node.
      JComponent comp = node.getComponent();
      if (comp == null) {
        // If the node doesn't provide a component, default to simpler data.
        TableModel tm = new DefaultTableModel(node.iGetData(),
            node.getColumnNames()) {
          private static final long serialVersionUID = 1L;

          @Override
          public void setValueAt(Object aValue, int row, int column) {
            // Do nothing. Read-Only.
          }
        };
        tableElements.setModel(tm);
        comp = panelMain;
      }

      splitPaneInfo.setTopComponent(comp);
      splitPaneInfo.validate();
      //scrollPane.setViewportView(comp);
    }
  }

  public void normalCursor() {
    Cursor normalCursor = new Cursor(Cursor.DEFAULT_CURSOR);
    setCursor(normalCursor);
  }

  public void waitCursor() {
    Cursor hourglassCursor = new Cursor(Cursor.WAIT_CURSOR);
    setCursor(hourglassCursor);
  }


  private void exitApp() {
      System.exit(0);
  }
  private JScrollPane getScrollPane() {
    if (scrollPane == null) {
    	scrollPane = new JScrollPane();
    	scrollPane.setViewportView(iGetElements());
    }
    return scrollPane;
  }
  private JTable iGetElements() {
    if (tableElements == null) {
    	tableElements = new JTable();
    }
    return tableElements;
  }
  private JLabel getLblIpAddress() {
    if (lblIpAddress == null) {
    	lblIpAddress = new JLabel("IP Address:");
    }
    return lblIpAddress;
  }
  private JTextField getTxtIpAddr() {
    if (txtIpAddr == null) {
    	txtIpAddr = new JTextField();
    	txtIpAddr.setColumns(10);
    }
    return txtIpAddr;
  }
  private JLabel getLblPort() {
    if (lblPort == null) {
    	lblPort = new JLabel("Port:");
    }
    return lblPort;
  }
  private JTextField getTxtPort() {
    if (txtPort == null) {
    	txtPort = new JTextField();
    	txtPort.setColumns(10);
    }
    return txtPort;
  }
  private JPanel getPanelConnection() {
    if (panelConnection == null) {
    	panelConnection = new JPanel();
    	panelConnection.setLayout(new BorderLayout(0, 0));
    	panelConnection.add(getPanelConnect(), BorderLayout.NORTH);
    	panelConnection.add(getToolBarTop(), BorderLayout.SOUTH);
    }
    return panelConnection;
  }
  private JPanel getPanelConnect() {
    if (panelConnect == null) {
    	panelConnect = new JPanel();
    	FlowLayout flowLayout = (FlowLayout) panelConnect.getLayout();
    	flowLayout.setAlignment(FlowLayout.LEFT);
    	panelConnect.add(getLblIpAddress());
    	panelConnect.add(getTxtIpAddr());
    	panelConnect.add(getLblPort());
    	panelConnect.add(getTxtPort());
    	panelConnect.add(getBtnConnect());
    }
    return panelConnect;
  }
} // @jve:decl-index=0:visual-constraint="10,10"
