package com.ademille.hcexplorer.gui.paramcache;

import java.util.List;

import com.ademille.hc.core.HcEnum;
import com.ademille.hc.core.HcCell;
import com.ademille.hc.exception.HcException;
import com.ademille.hc.param.HcParam;

// Cache the HcParam contents so that screen refreshes don't always hit the
// server. Delegate all other methods to the HcParam that is being wrapped.
public class HcParamCache extends HcParam {
	protected HcParam param;
	protected boolean staleData;
	protected String name;

	private String type;
	private String access;
	private String value;
	private String info;
	private int tid=0; //Table identifier


	public HcParamCache(HcParam param) {
		super(param.getName(), param.getMode(), param.getType());
		this.param = param;
	}

	public HcParamCache(HcParam param, int tid) {
		this(param);
		this.tid = tid;
	}

	public HcParam getParam() {
  	return param;
  }
	
	public int getTid(){
		return tid;
	}

	public String getName() {
		if (staleData)
			try {
				updateCache();
			} catch (HcException e) {
				return "Error Accessing";
			}
		return name;
	}
	
	@Override
	 public List<HcEnum> iGetEnums() {
		return param.iGetEnums();
	}

	public String getTypeAsString() {
		if (staleData)
			try {
				updateCache();
			} catch (HcException e) {
				return "Error Accessing";
			}
		return type;
	}

	public String getAccess() {
		if (staleData)
			try {
				updateCache();
			} catch (HcException e) {
				return "Error Accessing";
			}
		return access;
	}

	public String getValueAsString() {
		if (staleData)
			try {
				updateCache();
			} catch (HcException e) {
				return "Error Accessing";
			}
		return value;
	}

	public String getInfo() {
		if (staleData)
			try {
				updateCache();
			} catch (HcException e) {
				return "Error Accessing";
			}
		return info;
	}

	public void updateCache() throws HcException {
		name = param.getName();
		type = param.getTypeString();
		access = param.getModeString();
		//If the param is a table type, then grab the table value
		if (param.hasQualTidEnums() || param.hasQualTidRange()) {
			value = param.iGetValueString(tid);
		}
		else {
			value = param.getValueString();
		}
		info = param.getInfo();
		staleData = false;
	}
	
	

	@Override
	public String toString() {
		return value;
	}

	///////////////////////////////////////////////////
	// Delegated methods
	///////////////////////////////////////////////////
	@Override
  public void serialize(HcCell msg) throws HcException {
		param.serialize(msg);
  }

	@Override
  public void deserialize(HcCell msg) throws HcException {
		param.deserialize(msg);
  }

	@Override
  public void printValue() {
		param.printValue();
  }

	@Override
  public String getValueString() throws HcException {
		return param.getValueString();
  }

	@Override
  public String getEnumString() throws HcException {
		return param.getEnumString();
  }

	@Override
  public String iGetEnumString(int tid) throws HcException {
		return param.iGetEnumString(tid);
  }

	@Override
  public void setTableEnumString(int tid, String val) throws HcException {
		param.setTableEnumString(tid, val);
  }

	@Override
  public void setEnumString(String val) throws HcException {
		param.setEnumString(val);
  }

	@Override
  public void setNatString(String val) throws HcException {
		param.setNatString(val);
  }

	@Override
  public void setTableNatString(int tid, String val) throws HcException {
		param.setTableNatString(tid, val);
  }


	@Override
  public String iGetValueString(int tid) throws HcException {
		return param.iGetValueString(tid);
  }

	@Override
  public void getMsg(HcCell omsg) throws HcException {
		param.getMsg(omsg);
  }

	@Override
  public void setMsg(HcCell imsg, HcCell omsg) throws HcException {
		param.setMsg(imsg, omsg);
  }
	
	@Override
	public boolean hasQualValRange() {
		return param.hasQualValRange();
  }

	@Override
	public boolean hasQualValEnums() {
  	return param.hasQualValEnums();
  }

	@Override
	public boolean hasQualValXlate() {
  	return param.hasQualValXlate();
  }

	@Override
	public boolean isQualStub() {
  	return param.isQualStub();
  }

	@Override
	public boolean hasQualTidRange() {
  	return param.hasQualTidRange();
  }

	@Override
	public boolean hasQualTidEnums() {
  	return param.hasQualTidEnums();
  }

	@Override
	public int getTidCnt() {
  	return param.getTidCnt();
  }

}
