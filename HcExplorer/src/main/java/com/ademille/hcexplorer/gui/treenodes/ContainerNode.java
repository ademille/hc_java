/*
 * Copyright (C) 2008  Aaron DeMille
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.ademille.hcexplorer.gui.treenodes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JComponent;

import com.ademille.hc.core.HcContainer;
import com.ademille.hcexplorer.gui.util.GuiAction;
import com.ademille.hcexplorer.gui.util.ImageUtil;

public class ContainerNode extends HcHelperNode {
  private HcContainer cont;
  private ContainerPanel contPanel;

  public ContainerNode(HcContainer cont) {
    this.cont = cont;
    contPanel = new ContainerPanel(cont);
  }

  @Override
  public String toString() {
    if (cont != null)
      return cont.getName();
    else
      return "";
  }
  
  @Override
  public JComponent getComponent(){
    return contPanel;
  }
  
  private void createActions() {
    Action a = new GuiAction("Refresh",
        ImageUtil.createIcon("refresh.gif"), "Refresh Table", KeyEvent.VK_F5,
        new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            contPanel.refresh();
          }
        });
    actionList.add(a);
    }
  
  @Override
  public void populate() {
    //Create container actions list
    createActions();
    
    //Add other containers
    for (HcContainer c :cont.getContList()){
      addHVTHelperNode(new ContainerNode(c));  
    }
  }
  
  @Override
  public ImageIcon getIcon() {
    return ImageUtil.createIcon("opentype.gif");
  }

}
