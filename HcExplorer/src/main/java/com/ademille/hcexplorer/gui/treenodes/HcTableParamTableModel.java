package com.ademille.hcexplorer.gui.treenodes;

import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultCellEditor;
import javax.swing.JTextField;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import org.apache.log4j.Logger;

import com.ademille.hc.core.HcEnum;
import com.ademille.hc.exception.HcException;
import com.ademille.hc.param.HcBool;
import com.ademille.hc.param.HcCall;
import com.ademille.hc.param.HcIntegerParam;
import com.ademille.hc.param.HcParam;
import com.ademille.hcexplorer.gui.editors.HcBoolCellEditor;
import com.ademille.hcexplorer.gui.editors.HcCallCellEditor;
import com.ademille.hcexplorer.gui.editors.HcIntegerCellEditor;
import com.ademille.hcexplorer.gui.paramcache.HcBoolCache;
import com.ademille.hcexplorer.gui.paramcache.HcIntegerCache;
import com.ademille.hcexplorer.gui.paramcache.HcParamCache;
import com.ademille.hcexplorer.gui.renderers.ButtonRenderer;

class HcTableParamTableModel extends AbstractTableModel {
	private String[] columnNames = { "Index", "Value" };
	public final int COL_NAME = 0;
	private final int COL_VALUE = 1;

	private List<HcParamCache> paramCacheList;
	
	
	//Editors
	private HcBoolCellEditor hcBoolEditor;
	private HcIntegerCellEditor hcIntEditor;
	private HcCallCellEditor hcCallEditor;
	private DefaultCellEditor defaultEditor;
	
	private TableCellEditor typeEditor; //The editor for this table type.
	
	private JTextField text;
	private static final Logger logger = Logger
	    .getLogger("com.ademille.hcexplorer");

	public HcTableParamTableModel() {
		// Cache the values from the server so that table redraws don't grab data
		// from the server.
		// logger.info("Refreshing.");
		paramCacheList = new ArrayList<HcParamCache>();
		text = new JTextField();
		
		//Instantiate editors
		hcBoolEditor = new HcBoolCellEditor();
		hcIntEditor = new HcIntegerCellEditor();
		hcCallEditor = new HcCallCellEditor();
		defaultEditor = new DefaultCellEditor(text);
		
	}
	
	public void setData(HcParamCache cachedParam) {
		
		//Empty existing param list
		paramCacheList.clear();
		
		// Populate table based on HcParam
		// Check for table type
		
		//Pull out the real HcParam from the Cache wrapper
		HcParam param = cachedParam.getParam();

		// Check if the table has a range and add to param cache. The cache will
		// be the table aware version.
		if (param.hasQualTidRange()) {
			for (int i = 0; i < param.getTidCnt(); i++)
				if (param instanceof HcBool) {
					paramCacheList.add(new HcBoolCache((HcBool) param, i));
				} else if (param instanceof HcIntegerParam) {
					paramCacheList.add(new HcIntegerCache((HcIntegerParam) param, i));
				} else {
					paramCacheList.add(new HcParamCache(param, i));
				}
		}
		//For Table Enums, the value of the enum is the table index, so pull that.
		else if (param.hasQualTidEnums()) {
			for (HcEnum hce:param.iGetEnums())
				if (param instanceof HcBool) {
					paramCacheList.add(new HcBoolCache((HcBool) param, (int)hce.getVal()));
				} else if (param instanceof HcIntegerParam) {
					paramCacheList.add(new HcIntegerCache((HcIntegerParam) param, (int)hce.getVal()));
				} else {
					paramCacheList.add(new HcParamCache(param, (int)hce.getVal()));
				}
		}
		//Decide which editor to use.  Since param is really an HcParamCache,
		//it is necessary to use use the getType method instead of instanceof.
		if (param instanceof HcBool) {
			typeEditor = hcBoolEditor;
		} else if (param instanceof HcIntegerParam) {
			typeEditor = hcIntEditor;
		} else if (param instanceof HcCall) {
			typeEditor = hcCallEditor;
		} else {
			typeEditor = defaultEditor;
		}
	}

	public void refresh() {
		// Cache the values from the server so that table redraws don't grab data
		// from the server.
		//logger.info("Refreshing.");
		for (HcParamCache param : paramCacheList) {
			try {
				param.updateCache();
			} catch (HcException rc) {
				logger.error("Error getting value for: " + param.getName() + ". "
				    + rc.getMessage());
			}
		}
		fireTableDataChanged();
	}

	public String getRowInfo(int row) {
		return paramCacheList.get(row).getInfo();
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		if (columnIndex == COL_VALUE && rowIndex < paramCacheList.size())
			return true;
		else
			return false;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		if (rowIndex < paramCacheList.size()) {
			HcParamCache param = paramCacheList.get(rowIndex);
			switch (columnIndex) {
			case COL_NAME:
				//If it's a table range, it's just a zero- table count value.
				//So, just return the row index.
				if (param.hasQualTidRange()){
					return rowIndex;
				}
				//If the table is an enumeration, return the enumeration name as the name.
				else if (param.hasQualTidEnums()){
					return param.iGetEnums().get(rowIndex).getStr();
				}
			case COL_VALUE:
				return param;
			}
		}
		// Not valid selection.
		return null;
	}

	@Override
	public void setValueAt(Object val, int rowIndex, int columnIndex) {
		if (columnIndex == COL_VALUE && rowIndex < paramCacheList.size()) {
			HcParamCache param = paramCacheList.get(rowIndex);
			try {
				//Check for table type
				if (param.hasQualTidEnums() || param.hasQualTidRange()){
					param.setTableNatString(param.getTid(),(String) val);
				}
				else {
					//This shouldn't ever happen.
					param.setNatString((String) val);
				}
				// Setting the value may have had side-effects so refresh the entire
				// table.
				refresh();
			} catch (HcException e) {
				logger.error("Error setting " + param.getName() + " to '" + val + "' :"
				    + e.getType() + ":" + e.getMessage());
			}
		}
	}

	@Override
	public int getRowCount() {
		return paramCacheList.size();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public String getColumnName(int column) {
		if (column < columnNames.length && column >= 0)
			return columnNames[column];
		else
			return "";
	}

	public TableCellEditor getCellEditorAt(int row, int col) {
		//Since the entire table type table uses the same editor
		return typeEditor;
	}

	public TableCellRenderer getCellRenderer(int row, int col) {
			//If it's a table return a button otherwise use default
			//renderer
		if (col == COL_VALUE && row < paramCacheList.size()) {
			HcParamCache param = paramCacheList.get(row);
			if (param.getParam() instanceof HcCall) //Call should be displayed as a button.
				return new ButtonRenderer("Call");
			else
				return null;
		}
		else
			return null;
	}
}