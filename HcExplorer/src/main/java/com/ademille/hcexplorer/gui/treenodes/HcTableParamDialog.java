package com.ademille.hcexplorer.gui.treenodes;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

import com.ademille.hcexplorer.gui.paramcache.HcParamCache;

import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;

public class HcTableParamDialog extends JDialog {
	private final HcTableParamTable hcTableParamTable;
	private JTextArea textArea;


	/**
	 * Create the dialog.
	 */
	public HcTableParamDialog() {
		setBounds(100, 100, 511, 473);
		getContentPane().setLayout(new BorderLayout());
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Close");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						setVisible(false);
					}
				});
				okButton.setActionCommand("Close");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
		{
			JSplitPane splitPane = new JSplitPane();
			splitPane.setResizeWeight(0.7);
			splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
			getContentPane().add(splitPane, BorderLayout.CENTER);
			{
				JPanel tablePanel = new JPanel();
				splitPane.setLeftComponent(tablePanel);
				tablePanel.setLayout(new BorderLayout(0, 0));
				{
					JScrollPane scrollPane = new JScrollPane();
					tablePanel.add(scrollPane);
					{
						hcTableParamTable = new HcTableParamTable();
						scrollPane.setViewportView(hcTableParamTable);
					}
				}
			}
			{
				JScrollPane scrollPane = new JScrollPane();
				splitPane.setRightComponent(scrollPane);
				{
					textArea = new JTextArea();
					scrollPane.setViewportView(textArea);
				}
			}
		}
	}
	
	public void setData(HcParamCache param){
		String mode = "";
		switch (param.getMode()){
		case READ_ONLY:
			mode = "Read Only";
			break;
		case WRITE_ONLY:
			mode = "Write Only";
			break;
		case READ_WRITE:
			mode = "Read Write";
			break;
		}
		setTitle(param.getName() + " : " + mode);
		hcTableParamTable.setData(param);
		textArea.setText(param.getInfo());
		
	}

}
