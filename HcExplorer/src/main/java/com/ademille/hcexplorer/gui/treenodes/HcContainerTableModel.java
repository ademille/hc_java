package com.ademille.hcexplorer.gui.treenodes;

import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultCellEditor;
import javax.swing.JTextField;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import org.apache.log4j.Logger;

import com.ademille.hc.core.HcContainer;
import com.ademille.hc.exception.HcException;
import com.ademille.hc.param.HcBool;
import com.ademille.hc.param.HcCall;
import com.ademille.hc.param.HcIntegerParam;
import com.ademille.hc.param.HcParam;
import com.ademille.hcexplorer.gui.editors.HcBoolCellEditor;
import com.ademille.hcexplorer.gui.editors.HcCallCellEditor;
import com.ademille.hcexplorer.gui.editors.HcIntegerCellEditor;
import com.ademille.hcexplorer.gui.editors.HcParamTableEditor;
import com.ademille.hcexplorer.gui.paramcache.HcBoolCache;
import com.ademille.hcexplorer.gui.paramcache.HcIntegerCache;
import com.ademille.hcexplorer.gui.paramcache.HcParamCache;
import com.ademille.hcexplorer.gui.renderers.ButtonRenderer;

class HcContainerTableModel extends AbstractTableModel {
	private String[] columnNames = { "Name", "Type", "Access", "Value" };
	public final int COL_NAME = 0;
	private final int COL_TYPE = 1;
	private final int COL_ACCESS = 2;
	private final int COL_VALUE = 3;

	private HcContainer cont;
	private List<HcParamCache> paramCacheList;
	private List<TableCellEditor> editorList;
	
	
	//Editors
	private HcBoolCellEditor hcBoolEditor;
	private HcIntegerCellEditor hcIntEditor;
	private HcCallCellEditor hcCallEditor;
	private DefaultCellEditor defaultEditor;
	private HcParamTableEditor hcTableEditor;
	
	private JTextField text;
	private static final Logger logger = Logger
	    .getLogger("com.ademille.hcexplorer");

	public HcContainerTableModel(HcContainer cont) {
		this.cont = cont;
		// Cache the values from the server so that table redraws don't grab data
		// from the server.
		// logger.info("Refreshing.");
		paramCacheList = new ArrayList<HcParamCache>();
		editorList = new ArrayList<TableCellEditor>();
		text = new JTextField();
		
		//Instantiate editors
		hcBoolEditor = new HcBoolCellEditor();
		hcIntEditor = new HcIntegerCellEditor();
		hcCallEditor = new HcCallCellEditor();
		defaultEditor = new DefaultCellEditor(text);
		hcTableEditor = new HcParamTableEditor();
		
		for (HcParam param : cont.getParamList()) {
			// Check for table type
  		if (param.hasQualTidEnums() || param.hasQualTidRange()) {
  			//The editor is just a button and the param doesn't need to be cached.
  			//It will be cached in the HcTableParamTable Model
				editorList.add(hcTableEditor);
				paramCacheList.add(new HcParamCache(param));
  		}
  		else if (param instanceof HcBool) {
				paramCacheList.add(new HcBoolCache((HcBool) param));
				editorList.add(hcBoolEditor);
			}
			else if (param instanceof HcIntegerParam){
			  paramCacheList.add(new HcIntegerCache((HcIntegerParam)param));
				editorList.add(hcIntEditor);
			}
			else if (param instanceof HcCall) {
			  paramCacheList.add(new HcParamCache(param));
				editorList.add(hcCallEditor);
			}
			else {
				paramCacheList.add(new HcParamCache(param));
  			editorList.add(defaultEditor);
			}
		}
	}

	public void refresh() {
		// Cache the values from the server so that table redraws don't grab data
		// from the server.
		//logger.info("Refreshing.");
		for (HcParamCache param : paramCacheList) {
			try {
				param.updateCache();
			} catch (HcException rc) {
				logger.error("Error getting value for: " + param.getName() + ". "
				    + rc.getMessage());
			}
		}
		fireTableDataChanged();
	}

	public String getRowInfo(int row) {
		return paramCacheList.get(row).getInfo();
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		if (columnIndex == COL_VALUE && rowIndex < cont.getParamList().size())
			return true;
		else
			return false;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Object retVal = null;
		if (rowIndex < paramCacheList.size()) {
			switch (columnIndex) {
			case COL_NAME:
				retVal = paramCacheList.get(rowIndex).getName();
				break;
			case COL_TYPE:
				retVal = paramCacheList.get(rowIndex).getTypeString();
				break;
			case COL_ACCESS:
				retVal = paramCacheList.get(rowIndex).getAccess();
				break;
			case COL_VALUE:
				retVal = paramCacheList.get(rowIndex);
				break;
			}
		}
		// Not valid selection.
		return retVal;
	}

	@Override
	public void setValueAt(Object val, int rowIndex, int columnIndex) {
		List<HcParam> params = cont.getParamList();
		if (columnIndex == COL_VALUE && rowIndex < params.size()) {
			HcParam param = params.get(rowIndex);
			try {
				//Check for table type
				if (param.hasQualTidEnums() || param.hasQualTidRange()){
					//Don't do anything. The custom editor will have already
					//set the values
				}
				else {
					param.setNatString((String) val);
				}
				// Setting the value may have had side-effects so refresh the entire
				// table.
				refresh();
			} catch (HcException e) {
				logger.error("Error setting " + param.getName() + " to '" + val + "' :"
				    + e.getType() + ":" + e.getMessage());
			}
		}
	}

	@Override
	public int getRowCount() {
		return paramCacheList.size();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public String getColumnName(int column) {
		if (column < columnNames.length && column >= 0)
			return columnNames[column];
		else
			return "";
	}

	public TableCellEditor getCellEditorAt(int row, int col) {
		if (row < editorList.size()) {
			return editorList.get(row);
		} else {
			return null;
		}
	}

	public TableCellRenderer getCellRenderer(int row, int col) {
		List<HcParam> params = cont.getParamList();
		if (col == COL_VALUE && row < params.size()) {
			HcParam param = params.get(row);
			//If it's a table return a button otherwise use default
			//renderer
			if (param.hasQualTidEnums() || param.hasQualTidRange())
				return new ButtonRenderer("Table");
			else if (param instanceof HcCall) //Call should be displayed as a button.
				return new ButtonRenderer("Call");
			else
				return null;
		}
		else
			return null;
	}
}