RcExploer by Aaron DeMille ajdemille@gmail.com (Copyright 2011-2013).

******* LICENSES ***********

RcExplorer comes with no warranty of any kind, but will hopefully prove
useful. It is released under the GPLv3 license.

For details see LICENSE.txt

See 3RD_PARTY_LICENSES.txt for additional licenses by reused components.


**** To run RcExplorer ****

Java 1.5+ is required. You can get it from http://java.sun.com
On linux/unix/(Mac OS?) boxes just type:
$> ./RcExplorerGUI.sh 

on Windows type or double click the .bat file in Windows Explorer:
C:\>RcExplorerGUI.bat

**** Sample Data ****
Sample MLS Export data is in the sample_mls_data directory.  
You can use this directory for the import source.


**** To build RcExplorer ****
Java 1.5+ is required. You can get it from http://java.sun.com
Ant 1.6+ is also required. You can get it from http://ant.apache.org

$> ant 

The program with necessary libraries and scripts will be copied to the "dist"
directory.

